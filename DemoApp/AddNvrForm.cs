/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

namespace DemoApp
{
	public class AddNvrForm
		: System.Windows.Forms.Form
	{
		public AddNvrForm(int defaultPortVal)
		{
			// Initialize the form
			m_addressLabel = new System.Windows.Forms.Label();
			m_addressLabel.Text = "IP Address:";
			m_addressLabel.AutoSize = true;
			Controls.Add(m_addressLabel);

			m_addressTextBox = new System.Windows.Forms.TextBox();
			m_addressTextBox.Width = 150;
			Controls.Add(m_addressTextBox);

			m_portLabel = new System.Windows.Forms.Label();
			m_portLabel.Text = "Port:";
			m_portLabel.AutoSize = true;
			Controls.Add(m_portLabel);

			m_portUpDown = new System.Windows.Forms.NumericUpDown();
			m_portUpDown.Width = 150;
			m_portUpDown.Maximum = System.Int32.MaxValue - 1;
			m_portUpDown.Minimum = 1000;
			m_portUpDown.Value = defaultPortVal;
			Controls.Add(m_portUpDown);

			m_okButton = new System.Windows.Forms.Button();
			m_okButton.Text = "OK";
			m_okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			Controls.Add(m_okButton);

			m_cancelButton = new System.Windows.Forms.Button();
			m_cancelButton.Text = "Cancel";
			m_cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			Controls.Add(m_cancelButton);

			ClientSize = new System.Drawing.Size(250, 90);

			m_addressLabel.Location = new System.Drawing.Point(5, 7);
			m_addressTextBox.Location = new System.Drawing.Point(80, 5);
			m_portLabel.Location = new System.Drawing.Point(5, 30);
			m_portUpDown.Location = new System.Drawing.Point(80, 28);

			m_cancelButton.Location = new System.Drawing.Point(168, 60);
			m_okButton.Location = new System.Drawing.Point(90, 60);

			Text = "Add Site";
			FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			AcceptButton = m_okButton;
			CancelButton = m_cancelButton;
		}

		public string Address
		{
			get { return m_addressTextBox.Text; }
		}

		public int Port
		{
			get { return (int) m_portUpDown.Value; }
		}

		private System.Windows.Forms.Label m_addressLabel;
		private System.Windows.Forms.TextBox m_addressTextBox;
		private System.Windows.Forms.Label m_portLabel;
		private System.Windows.Forms.NumericUpDown m_portUpDown;
		private System.Windows.Forms.Button m_okButton;
		private System.Windows.Forms.Button m_cancelButton;

	}
}
