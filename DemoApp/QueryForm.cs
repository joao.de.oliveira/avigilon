/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AvigilonDotNet;

namespace DemoApp
{
	delegate void AddToListViewDelegate(System.Collections.Generic.List<ListViewItem> lviList);

	/// <summary>
	/// Defines the form that allows a user to query events from an dataStore
	/// </summary>
	public class QueryForm
		: System.Windows.Forms.Form
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="dataStore">dataStore to query</param>
		public QueryForm(AvigilonDotNet.IDataStore dataStore)
		{
			m_dataStore = dataStore;
			m_continuingQuery = false;

			InitializeComponents_();

			this.Text = "Querying";
			this.Layout += OnLayout_;
		}

		public QueryForm(AvigilonDotNet.IQueryContinuable continueQuery)
		{
			m_activeQuery = continueQuery;
			m_continuingQuery = true;

			InitializeComponents_();

			m_activeQuery.ResultReceived += QueryResultHandlerDelegate_;
			m_activeQuery.ErrorOccurred += QueryErrorHandlerDelegate_;

			SetResultColumns_(m_activeQuery.Search);

			this.Text = "Continuing Query: " + m_activeQuery.Search.ToString();
			this.Layout += OnLayout_;
		}

		/// <summary>
		/// Form layout event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLayout_(
			System.Object sender,
			System.Windows.Forms.LayoutEventArgs e)
		{
			LayoutControls_();
		}

		protected override void OnFormClosing(FormClosingEventArgs e)
		{
			if (m_activeQuery != null)
			{
				m_activeQuery.Cancel();
				m_activeQuery = null;
			}
		}

		/// <summary>
		/// Execute button click event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void OnExecute_(
			System.Object sender,
			System.EventArgs e)
		{
			if (m_continuingQuery)
			{
				System.Windows.Forms.MessageBox.Show("Resuming a query, no execution allowed");
				return;
			}

			if (m_activeQuery != null)
			{
				// Query already executing, cancel it and dispose.
				m_activeQuery.Cancel();
				m_activeQuery = null;
			}

			// Clear the current results from the display
			m_queryResultsList.Items.Clear();

			if (m_activeQuery == null)
			{
				if (m_queryParametersTab.SelectedTab.Text == QueryEventAllTab.GetText())
				{
					QueryEventAllTab qTab = (QueryEventAllTab)m_queryParametersTab.SelectedTab;
					m_activeQuery = m_dataStore.QueryEventsAll(
						EventLinkMode.Closed | EventLinkMode.Opened | EventLinkMode.Single | EventLinkMode.Undefined,
						qTab.InclLinkedEvents);
				}
				else if (m_queryParametersTab.SelectedTab.Text == QueryEventTimeTab.GetText())
				{
					QueryEventTimeTab qTab = (QueryEventTimeTab)m_queryParametersTab.SelectedTab;
					m_activeQuery = m_dataStore.QueryEventsTimeStamp(
						qTab.StartDateTime,
						qTab.EndDateTime,
						EventLinkMode.Closed | EventLinkMode.Opened | EventLinkMode.Single | EventLinkMode.Undefined,
						qTab.InclLinkedEvents);
				}
				else if (m_queryParametersTab.SelectedTab.Text == QueryEventTypesTab.GetText())
				{
					QueryEventTypesTab qTab = (QueryEventTypesTab)m_queryParametersTab.SelectedTab;

					m_activeQuery = m_dataStore.QueryEventsType(
						qTab.StartDateTime,
						qTab.EndDateTime,
						EventLinkMode.Closed | EventLinkMode.Opened | EventLinkMode.Single | EventLinkMode.Undefined,
						qTab.InclLinkedEvents,
						qTab.EventTypes);
				}
				else if (m_queryParametersTab.SelectedTab.Text == QueryEventCategoriesTab.GetText())
				{
					QueryEventCategoriesTab qTab = (QueryEventCategoriesTab)m_queryParametersTab.SelectedTab;

					m_activeQuery = m_dataStore.QueryEventsCategory(
						qTab.StartDateTime,
						qTab.EndDateTime,
						EventLinkMode.Closed | EventLinkMode.Opened | EventLinkMode.Single | EventLinkMode.Undefined,
						qTab.InclLinkedEvents,
						qTab.EventCategories,
						qTab.FilteredEventTypes);
				}
				else if (m_queryParametersTab.SelectedTab.Text == QueryEventDeviceIdTab.GetText())
				{
					QueryEventDeviceIdTab qTab = (QueryEventDeviceIdTab)m_queryParametersTab.SelectedTab;

					m_activeQuery = m_dataStore.QueryEventsDeviceId(
						qTab.StartDateTime,
						qTab.EndDateTime,
						EventLinkMode.Closed | EventLinkMode.Opened | EventLinkMode.Single | EventLinkMode.Undefined,
						qTab.InclLinkedEvents,
						qTab.DeviceIds);
				}
				else if (m_queryParametersTab.SelectedTab.Text == QueryEventActiveTab.GetText())
				{
					QueryEventActiveTab qTab = (QueryEventActiveTab)m_queryParametersTab.SelectedTab;

					m_activeQuery = m_dataStore.QueryEventsActive();
				}
				else if (m_queryParametersTab.SelectedTab.Text == QueryTextTransactionsTab.GetText())
				{
					QueryTextTransactionsTab qTab = (QueryTextTransactionsTab)m_queryParametersTab.SelectedTab;

					m_activeQuery = m_dataStore.QueryTransactions(
						qTab.StartDateTime,
						qTab.EndDateTime,
						qTab.TextSourceIds);
				}
				else if (m_queryParametersTab.SelectedTab.Text == QueryRecordingsTab.GetText())
				{
					QueryRecordingsTab qTab = (QueryRecordingsTab)m_queryParametersTab.SelectedTab;

					m_activeQuery = m_dataStore.QueryRecordingEvents(
						qTab.StartDateTime,
						qTab.EndDateTime,
						qTab.DeviceIds);
				}
				else if (m_queryParametersTab.SelectedTab.Text == QueryTimelineTab.GetText())
				{
					QueryTimelineTab qTab = (QueryTimelineTab) m_queryParametersTab.SelectedTab;

					m_activeQuery = m_dataStore.QueryTimelineEvents(
						qTab.StartDateTime,
						qTab.EndDateTime,
						qTab.DeviceIds,
						qTab.Precision);
				}
			}

			// Hookup callback
			if (m_activeQuery != null)
			{
				m_activeQuery.ResultReceived += QueryResultHandlerDelegate_;
				m_activeQuery.ErrorOccurred += QueryErrorHandlerDelegate_;
				SetResultColumns_(m_activeQuery.Search);

				// Start execution or resume
				DoQuery_(m_activeQuery);
			}
		}

		/// <summary>
		/// Continue button click event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void OnContinue_(
			System.Object sender,
			System.EventArgs e)
		{
			// Clear the current results from the display and perform the query again
			m_queryResultsList.Items.Clear();
			if (m_activeQuery != null)
			{
				DoQuery_(m_activeQuery);
			}
			else
			{
				System.Windows.Forms.MessageBox.Show("No active query defined, unable to continue");
			}
		}

		/// <summary>
		/// Cancel button click event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void OnCancel_(
			System.Object sender,
			System.EventArgs e)
		{
			if (m_activeQuery != null)
			{
				// Query already executing, cancel it and dispose.
				AvgError err = m_activeQuery.Cancel();
				MessageBox.Show("Query cancel: " + err.ToString());
				m_activeQuery = null;
			}
			else
			{
				System.Windows.Forms.MessageBox.Show("No active query defined, unable to cancel");
			}
		}

		/// <summary>
		/// Serialize button click event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void OnSerialize_(
			System.Object sender,
			System.EventArgs e)
		{
			if (m_activeQuery == null)
			{
				System.Windows.Forms.MessageBox.Show("No active query defined, unable to serialize");
				return;
			}

			if (m_activeQuery is AvigilonDotNet.IQueryContinuable)
			{
				IQueryContinuable queryContinuable = m_activeQuery as AvigilonDotNet.IQueryContinuable;
				String serializedval;
				AvigilonDotNet.AvgError error = queryContinuable.Serialize(out serializedval);
				if (AvigilonDotNet.ErrorHelper.IsError(error))
				{
					System.Windows.Forms.MessageBox.Show("Failed to serialize query: " + error.ToString());
				}
				else
				{
					System.Windows.Forms.Clipboard.SetText(serializedval);
					System.Windows.Forms.MessageBox.Show("Query has been serialized to the clipboard");
				}
			}
			else
			{
				System.Windows.Forms.MessageBox.Show("Active query is not continuable");
			}
		}

		void DoQuery_(AvigilonDotNet.IQuery query)
		{
			// Start execution or resume
			AvgError error = query.Execute();
			if (ErrorHelper.IsError(error))
			{
				MessageBox.Show("Query Failed " + error.ToString());
			}
		}

		void SetResultColumns_(AvigilonDotNet.QuerySearch querySearch)
		{
			m_queryResultsList.Columns.Clear();
			switch (querySearch)
			{
				case AvigilonDotNet.QuerySearch.Transactions:
					{
						m_queryResultsList.Columns.Add("TextSouceId", 100, HorizontalAlignment.Left);
						m_queryResultsList.Columns.Add("TextSouce", 100, HorizontalAlignment.Left);
						m_queryResultsList.Columns.Add("Txn Start", 100, HorizontalAlignment.Left);
						m_queryResultsList.Columns.Add("Txn End", 100, HorizontalAlignment.Left);
						m_queryResultsList.Columns.Add("Line time", 100, HorizontalAlignment.Left);
						m_queryResultsList.Columns.Add("Text", 100, HorizontalAlignment.Left);
					}
					break;

				default:
					{
						m_queryResultsList.Columns.Add("Time", 100, HorizontalAlignment.Left);
						m_queryResultsList.Columns.Add("Type", 100, HorizontalAlignment.Left);
						m_queryResultsList.Columns.Add("Category", 100, HorizontalAlignment.Left);
						m_queryResultsList.Columns.Add("LinkMode", 100, HorizontalAlignment.Left);
						m_queryResultsList.Columns.Add("Name", 100, HorizontalAlignment.Left);
						m_queryResultsList.Columns.Add("Message", 100, HorizontalAlignment.Left);

					}
					break;
			}
		}

		/// <summary>
		/// Query result event handler
		/// </summary>
		/// <param name="events"></param>
		/// <param name="bComplete"></param>
		void QueryResultHandlerDelegate_(
			IQueryResult result,
			QueryState queryState)
		{
			// Send conversion to thread pool so we don't block up the main thread
			System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(ConvertResult_), result);

			if (queryState != QueryState.Executing)
			{
				MessageBox.Show("Query is complete");
			}
		}

		void ConvertResult_(object obj)
		{
			IQueryResult result = obj as IQueryResult;
			System.Collections.Generic.List<ListViewItem> lviList = new System.Collections.Generic.List<ListViewItem>();
			if (result != null)
			{
				switch (result.Type)
				{
					case QueryResultType.Events:
						{
							IQueryResultEvents queryResultsEvents = (IQueryResultEvents)result;
							System.Collections.Generic.List<IQueryResultEvents.Result> eventResults = queryResultsEvents.Results;
							foreach (IQueryResultEvents.Result eventResult in eventResults)
							{
								// Columns:
								// Time, Type, Category, linkMode, Name, Message

								if (eventResult.originalEvent != null)
								{
									ListViewItem newItem = new ListViewItem(eventResult.originalEvent.TimeStamp.ToString());
									newItem.SubItems.Add(eventResult.originalEvent.Type.ToString());
									newItem.SubItems.Add(eventResult.originalEvent.Category.ToString());
									newItem.SubItems.Add(eventResult.originalEvent.LinkMode.ToString());
									newItem.SubItems.Add(eventResult.originalEvent.Name);
									newItem.SubItems.Add(eventResult.originalEvent.Message);
									newItem.Tag = eventResult;
									lviList.Add(newItem);
								}

								if (eventResult.linkedEvent != null)
								{
									ListViewItem newItem = new ListViewItem(eventResult.linkedEvent.TimeStamp.ToString());
									newItem.SubItems.Add(eventResult.linkedEvent.Type.ToString());
									newItem.SubItems.Add(eventResult.linkedEvent.Category.ToString());
									newItem.SubItems.Add(eventResult.linkedEvent.LinkMode.ToString());
									newItem.SubItems.Add(eventResult.linkedEvent.Name);
									newItem.SubItems.Add(eventResult.linkedEvent.Message);
									newItem.Tag = eventResult;
									lviList.Add(newItem);
								}
							}
						}
						break;

					case QueryResultType.Transactions:
						{
							// Columns:
							// TextSouceId, TextSouce, Txn Start, Txn End, Line time, Text

							IQueryResultTransactions queryResultsTT = (IQueryResultTransactions)result;
							System.Collections.Generic.List<ITransaction> tranasctions = queryResultsTT.Results;

							foreach (ITransaction txn in tranasctions)
							{
								foreach (ITransactionLine txnLine in txn.Lines)
								{
									ListViewItem newItem = new ListViewItem(txn.TextSourceId.ToString());
									newItem.SubItems.Add(txn.TextSourceName);
									newItem.SubItems.Add(txn.TransactionStart.ToString());
									newItem.SubItems.Add(txn.TransactionEnd.ToString());

									newItem.SubItems.Add(txnLine.Timestamp.ToString());
									newItem.SubItems.Add(txnLine.Text);

									lviList.Add(newItem);
								}
							}
						}
						break;

					default:
						break;
				}
			}

			m_queryResultsList.BeginInvoke(new AddToListViewDelegate(AddToLiveView_), lviList);
		}

		void AddToLiveView_(System.Collections.Generic.List<ListViewItem> lviList)
		{
			foreach (ListViewItem lvi in lviList)
				m_queryResultsList.Items.Add(lvi);
		}

		/// <summary>
		/// Query error event handler delegate
		/// </summary>
		void QueryErrorHandlerDelegate_()
		{
			MessageBox.Show("Query error");
		}

		/// <summary>
		/// Called to initialize the controls on the form
		/// </summary>
		private void InitializeComponents_()
		{
			this.Size = new System.Drawing.Size(675, 500);

			if (!m_continuingQuery)
			{
				m_queryParametersTab = new System.Windows.Forms.TabControl();

				// Allow creation of a new query
				m_queryParametersTab.TabPages.Add(new QueryEventAllTab());
				m_queryParametersTab.TabPages.Add(new QueryEventTimeTab());
				m_queryParametersTab.TabPages.Add(new QueryEventTypesTab());
				m_queryParametersTab.TabPages.Add(new QueryEventCategoriesTab());
				m_queryParametersTab.TabPages.Add(new QueryEventDeviceIdTab());
				m_queryParametersTab.TabPages.Add(new QueryEventActiveTab());
				m_queryParametersTab.TabPages.Add(new QueryTextTransactionsTab());
				m_queryParametersTab.TabPages.Add(new QueryRecordingsTab());
				m_queryParametersTab.TabPages.Add(new QueryTimelineTab());
				Controls.Add(m_queryParametersTab);
			}

			m_executeQueryButton = new System.Windows.Forms.Button();
			m_executeQueryButton.Text = "Execute";
			m_executeQueryButton.Click += OnExecute_;
			Controls.Add(m_executeQueryButton);

			m_continueQueryButton = new System.Windows.Forms.Button();
			m_continueQueryButton.Text = "Continue";
			m_continueQueryButton.Click += OnContinue_;
			Controls.Add(m_continueQueryButton);

			m_cancelQueryButton = new System.Windows.Forms.Button();
			m_cancelQueryButton.Text = "Cancel";
			m_cancelQueryButton.Click += OnCancel_;
			Controls.Add(m_cancelQueryButton);

			m_serializeQueryButton = new System.Windows.Forms.Button();
			m_serializeQueryButton.Text = "Serialize";
			m_serializeQueryButton.Click += OnSerialize_;
			Controls.Add(m_serializeQueryButton);

			m_queryResultsList = new System.Windows.Forms.ListView();
			m_queryResultsList.View = View.Details;
			m_queryResultsList.GridLines = true;
			m_queryResultsList.MultiSelect = false;
			Controls.Add(m_queryResultsList);

			LayoutControls_();
		}

		/// <summary>
		/// Positions the controls on the form
		/// </summary>
		private void LayoutControls_()
		{
			System.Drawing.Rectangle clientRect = this.ClientRectangle;

			if (m_queryParametersTab != null)
			{
				m_queryParametersTab.Location = new System.Drawing.Point(10, 10);
				m_queryParametersTab.Size = new System.Drawing.Size(clientRect.Width - 20, 230);

				m_executeQueryButton.Location = new System.Drawing.Point(10, m_queryParametersTab.Bottom + 10);
				m_continueQueryButton.Location = new System.Drawing.Point(100, m_queryParametersTab.Bottom + 10);
				m_cancelQueryButton.Location = new System.Drawing.Point(190, m_queryParametersTab.Bottom + 10);
				m_serializeQueryButton.Location = new System.Drawing.Point(280, m_queryParametersTab.Bottom + 10);
			}
			else
			{
				m_executeQueryButton.Location = new System.Drawing.Point(10, 10);
				m_continueQueryButton.Location = new System.Drawing.Point(100, 10);
				m_cancelQueryButton.Location = new System.Drawing.Point(190, 10);
				m_serializeQueryButton.Location = new System.Drawing.Point(280, 10);
			}

			m_queryResultsList.Location = new System.Drawing.Point(10, m_executeQueryButton.Bottom + 10);
			m_queryResultsList.Size = new System.Drawing.Size(clientRect.Width - 20, clientRect.Bottom - 10 - (m_executeQueryButton.Bottom + 10));
		}

		bool m_continuingQuery;
		private AvigilonDotNet.IDataStore m_dataStore;
		private AvigilonDotNet.IQuery m_activeQuery;

		private System.Windows.Forms.TabControl m_queryParametersTab;
		private System.Windows.Forms.Button m_executeQueryButton;
		private System.Windows.Forms.Button m_continueQueryButton;
		private System.Windows.Forms.Button m_cancelQueryButton;
		private System.Windows.Forms.Button m_serializeQueryButton;

		private System.Windows.Forms.ListView m_queryResultsList;
	}
}