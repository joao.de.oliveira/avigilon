/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Text;

namespace DemoApp
{
	/// <summary>
	/// Displays the current progress of an backup operation
	/// </summary>
	class BackupForm
		: System.Windows.Forms.Form
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="backup">The backup operation to monitor</param>
		public BackupForm(AvigilonDotNet.IBackup backup)
		{
			m_backup = backup;
			m_backup.ProgressChanged += new AvigilonDotNet.BackupProgressChange(backupChange_);

			Size = new System.Drawing.Size(460, 240);
			Text = "Backup";
			StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;

			System.Windows.Forms.Label nvrLabel = new System.Windows.Forms.Label();
			nvrLabel.Location = new System.Drawing.Point(10, 10);
			nvrLabel.Text = "Site: " + m_backup.NvrUuid.ToString();
			nvrLabel.AutoSize = true;
			Controls.Add(nvrLabel);

			System.Windows.Forms.Label devicesLabel = new System.Windows.Forms.Label();
			devicesLabel.Location = new System.Drawing.Point(10, 40);
			System.String devicesString = "Devices: ";
			foreach (System.String deviceId in m_backup.DeviceIds)
			{
				devicesString += deviceId + " ";
			}
			devicesLabel.Text = devicesString;
			devicesLabel.AutoSize = true;
			Controls.Add(devicesLabel);

			System.Windows.Forms.Label timeLabel = new System.Windows.Forms.Label();
			timeLabel.Location = new System.Drawing.Point(10, 70);
			timeLabel.Text = m_backup.StartTime.ToString() + " to " + m_backup.EndTime.ToString();
			timeLabel.AutoSize = true;
			Controls.Add(timeLabel);


			m_progress = new System.Windows.Forms.ProgressBar();
			m_progress.Location = new System.Drawing.Point(10, 100);
			m_progress.Width = 430;
			m_progress.Minimum = 0;
			m_progress.Maximum = 100;
			Controls.Add(m_progress);

			System.Windows.Forms.Label stateLabel = new System.Windows.Forms.Label();
			stateLabel.Location = new System.Drawing.Point(10, 130);
			stateLabel.Text = "State:";
			stateLabel.AutoSize = true;
			Controls.Add(stateLabel);

			m_state = new System.Windows.Forms.Label();
			m_state.Location = new System.Drawing.Point(50, 130);
			m_state.AutoSize = true;
			Controls.Add(m_state);

			System.Windows.Forms.Label countLabel = new System.Windows.Forms.Label();
			countLabel.Location = new System.Drawing.Point(120, 130);
			countLabel.Text = "Count:";
			countLabel.AutoSize = true;
			Controls.Add(countLabel);

			m_count = new System.Windows.Forms.Label();
			m_count.Location = new System.Drawing.Point(160, 130);
			m_count.AutoSize = true;
			Controls.Add(m_count);

			m_startButton = new System.Windows.Forms.Button();
			m_startButton.Location = new System.Drawing.Point(10, 170);
			m_startButton.Text = "Start";
			m_startButton.AutoSize = true;
			m_startButton.Click += new EventHandler(StartButtonClick_);
			Controls.Add(m_startButton);

			m_abortButton = new System.Windows.Forms.Button();
			m_abortButton.Location = new System.Drawing.Point(100, 170);
			m_abortButton.Text = "Abort";
			m_abortButton.AutoSize = true;
			m_abortButton.Click += new EventHandler(AbortButtonClick_);
			Controls.Add(m_abortButton);

			m_exitButton = new System.Windows.Forms.Button();
			m_exitButton.Location = new System.Drawing.Point(370, 170);
			m_exitButton.Text = "Exit";
			m_exitButton.AutoSize = true;
			m_exitButton.Click += new EventHandler(ExitButtonClick_);
			Controls.Add(m_exitButton);
		}

		/// <summary>
		/// backup change event handler. This is called whenever the state of the backup changes.
		/// </summary>
		void backupChange_()
		{

			m_state.Text = m_backup.State.ToString();
			m_count.Text = (System.Int32)(100.0 * m_backup.PercentComplete) + "%";

			m_progress.Value = (System.Int32)(100.0 * m_backup.PercentComplete);
		}

		/// <summary>
		/// Start button clicked event handler
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="e"></param>
		void StartButtonClick_(
			System.Object obj,
			System.EventArgs e)
		{
			if(AvigilonDotNet.ErrorHelper.IsError(m_backup.Start()))
			{
				System.Windows.Forms.MessageBox.Show("Illegal transition");
			}
		}

		/// <summary>
		/// Abort button clicked event handler
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="e"></param>
		void AbortButtonClick_(
			System.Object obj,
			System.EventArgs e)
		{
			if(AvigilonDotNet.ErrorHelper.IsError(m_backup.Abort()))
			{
				System.Windows.Forms.MessageBox.Show("Illegal transition");
			}
		}

		/// <summary>
		/// Exit button clicked event handler
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="e"></param>
		void ExitButtonClick_(
			System.Object obj,
			System.EventArgs e)
		{
			Close();
		}

		/// <summary>
		/// Called when the dialog is to be closed
		/// </summary>
		/// <param name="e"></param>
		protected override void OnFormClosed(System.Windows.Forms.FormClosedEventArgs e)
		{
			m_backup.Abort();
			base.OnFormClosed(e);
		}


		private AvigilonDotNet.IBackup m_backup;

		private System.Windows.Forms.ProgressBar m_progress;
		private System.Windows.Forms.Label m_state;
		private System.Windows.Forms.Label m_count;

		private System.Windows.Forms.Button m_startButton;
		private System.Windows.Forms.Button m_abortButton;

		private System.Windows.Forms.Button m_exitButton;
	}
}
