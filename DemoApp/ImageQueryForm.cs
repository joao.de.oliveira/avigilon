/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DemoApp
{
	public class DateOps
	{
		/// <summary>
		/// Clamps a date time to values allowed by the System.Windows.Forms.DateTimePicker control
		/// </summary>
		/// <param name="inDt">input date time</param>
		/// <returns>date time clamped to a value allowed by System.Windows.Forms.DateTimePicker</returns>
		public static System.DateTime ClampToPicker(System.DateTime inDt)
		{
			if (inDt < System.Windows.Forms.DateTimePicker.MinimumDateTime)
				return System.Windows.Forms.DateTimePicker.MinimumDateTime;

			if (inDt > System.Windows.Forms.DateTimePicker.MaximumDateTime)
				return System.Windows.Forms.DateTimePicker.MaximumDateTime;

			return inDt;
		}
	}

	public class ImageQueryForm 
		: System.Windows.Forms.Form
	{
		public ImageQueryForm(AvigilonDotNet.RequestParams queryParams,
			AvigilonDotNet.IRequestor requestor)
		{
			m_params = queryParams;
			m_requestor = requestor;

			InitializeComponents_();
			UpdateValues_(true);

			Text = "Image Query";
			StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;

			//m_requestor.ResultReceived += ImageRequestResultHandler_;
			m_requestor.ResultFrameReceived += ImageRequestResultFrameHandler_;
		}

		private void OnExecute_(
			System.Object sender,
			System.EventArgs e)
		{
			try
			{
				UpdateValues_(false);
				m_requestor.Query(
					System.Convert.ToUInt32(m_requestIdEdit.Text),
					m_params);
			}
			catch
			{
				MessageBox.Show("Invalid argument");
			}
		}

		//private void ImageRequestResultHandler_(
		//    uint requestId, 
		//    AvigilonDotNet.AvgError error,
		//    AvigilonDotNet.IImage image)
		//{
		//    if (image != null)
		//    {
		//        ImageDisplayForm imgDisplay = new ImageDisplayForm(image);
		//        imgDisplay.Show();
		//    }
		//    else
		//    {
		//        String errMsg = "Request " + requestId.ToString() + " has failed, err = " + error.ToString();
		//        MessageBox.Show(errMsg);
		//    }
		//}

		private void ImageRequestResultFrameHandler_(
			uint requestId,
			AvigilonDotNet.AvgError error,
			AvigilonDotNet.IFrame frame)
		{
			if (frame != null)
			{
				ImageDisplayForm imgDisplay = new ImageDisplayForm(frame);
				imgDisplay.Show();
			}
			else
			{
				String errMsg = "Request " + requestId.ToString() + " has failed, err = " + error.ToString();
				MessageBox.Show(errMsg);
			}
		}


		private void InitializeComponents_()
		{
			this.Size = new System.Drawing.Size(450, 600);

			System.Windows.Forms.Label requestId = new System.Windows.Forms.Label();
			requestId.Text = "Request Id:";
			requestId.Location = new System.Drawing.Point(10, 10);
			requestId.AutoSize = true;
			Controls.Add(requestId);

			m_requestIdEdit = new System.Windows.Forms.TextBox();
			m_requestIdEdit.Text = "1";
			m_requestIdEdit.Location = new System.Drawing.Point(150, 10);
			Controls.Add(m_requestIdEdit);

			m_queryLive = new System.Windows.Forms.CheckBox();
			m_queryLive.Text = "Query for Live Image";
			m_queryLive.Location = new System.Drawing.Point(150, m_requestIdEdit.Bottom + 5);
			m_queryLive.AutoSize = true;
			m_queryLive.CheckedChanged += QueryLiveChanged_;
			Controls.Add(m_queryLive);

			System.Windows.Forms.Label timestampId = new System.Windows.Forms.Label();
			timestampId.Text = "Timestamp:";
			timestampId.Location = new System.Drawing.Point(10, m_queryLive.Bottom + 5);
			timestampId.AutoSize = true;
			Controls.Add(timestampId);

			m_timestamp = new System.Windows.Forms.DateTimePicker();
			m_timestamp.Format = DateTimePickerFormat.Custom;
			m_timestamp.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_timestamp.Location = new System.Drawing.Point(150, m_queryLive.Bottom + 5);
			Controls.Add(m_timestamp);

			System.Windows.Forms.Label timestampRoundId = new System.Windows.Forms.Label();
			timestampRoundId.Text = "Timestamp Rounding:";
			timestampRoundId.Location = new System.Drawing.Point(10, m_timestamp.Bottom + 5);
			timestampRoundId.AutoSize = true;
			Controls.Add(timestampRoundId);

			m_tsRounding = new EnumRadioSelector<AvigilonDotNet.QueryMatch>(2,100);
			m_tsRounding.Location = new System.Drawing.Point(150, m_timestamp.Bottom + 5);
			Controls.Add(m_tsRounding);

			System.Windows.Forms.Label timestampMinId = new System.Windows.Forms.Label();
			timestampMinId.Text = "Timestamp Min:";
			timestampMinId.Location = new System.Drawing.Point(10, m_tsRounding.Bottom + 5);
			timestampMinId.AutoSize = true;
			Controls.Add(timestampMinId);

			m_timestampMin = new System.Windows.Forms.DateTimePicker();
			m_timestampMin.Format = DateTimePickerFormat.Custom;
			m_timestampMin.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_timestampMin.Location = new System.Drawing.Point(150, m_tsRounding.Bottom + 5);
			Controls.Add(m_timestampMin);

			System.Windows.Forms.Label timestampMaxId = new System.Windows.Forms.Label();
			timestampMaxId.Text = "Timestamp Max:";
			timestampMaxId.Location = new System.Drawing.Point(10, m_timestampMin.Bottom + 5);
			timestampMaxId.AutoSize = true;
			Controls.Add(timestampMaxId);

			m_timestampMax = new System.Windows.Forms.DateTimePicker();
			m_timestampMax.Format = DateTimePickerFormat.Custom;
			m_timestampMax.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_timestampMax.Location = new System.Drawing.Point(150, m_timestampMin.Bottom + 5);
			Controls.Add(m_timestampMax);

			System.Windows.Forms.Label resolutionXId = new System.Windows.Forms.Label();
			resolutionXId.Text = "Width:";
			resolutionXId.AutoSize = true;
			resolutionXId.Location = new System.Drawing.Point(10, m_timestampMax.Bottom + 5);
			Controls.Add(resolutionXId);

			m_resolutionX = new System.Windows.Forms.TextBox();
			m_resolutionX.Location = new System.Drawing.Point(150, m_timestampMax.Bottom + 5);
			Controls.Add(m_resolutionX);

			System.Windows.Forms.Label resolutionYId = new System.Windows.Forms.Label();
			resolutionYId.Text = "Height:";
			resolutionYId.Location = new System.Drawing.Point(10, m_resolutionX.Bottom + 5);
			resolutionYId.AutoSize = true;
			Controls.Add(resolutionYId);

			m_resolutionY = new System.Windows.Forms.TextBox();
			m_resolutionY.Location = new System.Drawing.Point(150, m_resolutionX.Bottom + 5);
			Controls.Add(m_resolutionY);

			m_executeQueryButton = new System.Windows.Forms.Button();
			m_executeQueryButton.Text = "Execute";
			m_executeQueryButton.Location = new System.Drawing.Point(10, m_resolutionY.Bottom + 5);
			m_executeQueryButton.Click += OnExecute_;
			Controls.Add(m_executeQueryButton);

			SetLiveQuery_();
		}

		private void QueryLiveChanged_(
			object obj,
			EventArgs e)
		{
			SetLiveQuery_();
		}

		private void SetLiveQuery_()
		{
			if (m_queryLive.Checked)
			{
				m_timestamp.Enabled = false;
				m_tsRounding.Enabled = false;
				m_timestampMin.Enabled = false;
				m_timestampMax.Enabled = false;
			}
			else
			{
				m_timestamp.Enabled = true;
				m_tsRounding.Enabled = true;
				m_timestampMin.Enabled = true;
				m_timestampMax.Enabled = true;
			}
		}

		private void UpdateValues_(bool toUi)
		{
			if (toUi)
			{
				m_queryLive.Checked = m_params.Live;
				m_timestamp.Value = DateOps.ClampToPicker(m_params.Timestamp);
				m_tsRounding.Value = m_params.TimestampRoundMode;
				m_timestampMin.Value = DateOps.ClampToPicker(m_params.TimestampMin);
				m_timestampMax.Value = DateOps.ClampToPicker(m_params.TimestampMax);

				m_resolutionX.Text = m_params.Resolution.Width.ToString();
				m_resolutionY.Text = m_params.Resolution.Height.ToString();

				SetLiveQuery_();
			}
			else
			{
				m_params.Live = m_queryLive.Checked;

				if (!m_queryLive.Checked)
				{
					// only set ts values if we're not doing live
					m_params.Timestamp = m_timestamp.Value;
					m_params.TimestampRoundMode = m_tsRounding.Value;
					m_params.TimestampMin = m_timestampMin.Value;
					m_params.TimestampMax = m_timestampMax.Value;
				}

				m_params.Resolution = new System.Drawing.Size(
					System.Convert.ToUInt16(m_resolutionX.Text),
					System.Convert.ToUInt16(m_resolutionY.Text));
			}
		}

		private System.Windows.Forms.TextBox m_requestIdEdit;
		private System.Windows.Forms.CheckBox m_queryLive;
		private System.Windows.Forms.DateTimePicker m_timestamp;
		private EnumRadioSelector<AvigilonDotNet.QueryMatch> m_tsRounding;
		private System.Windows.Forms.DateTimePicker m_timestampMin;
		private System.Windows.Forms.DateTimePicker m_timestampMax;

		private System.Windows.Forms.TextBox m_resolutionX;
		private System.Windows.Forms.TextBox m_resolutionY;

		private System.Windows.Forms.Button m_executeQueryButton;

		private AvigilonDotNet.RequestParams m_params;
		private AvigilonDotNet.IRequestor m_requestor;
	}
}
