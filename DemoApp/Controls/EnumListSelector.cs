/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AvigilonDotNet;


namespace DemoApp
{
	public class EnumListSelector<EnumT>
		: System.Windows.Forms.CheckedListBox
	{
		public EnumListSelector(List<EnumT> ignoredValues)
		{
			CheckOnClick = true;
			Location = new System.Drawing.Point(0, 0);
			Size = Size;

			SelectedIndexChanged += OnSelectedIndexChanged_;

			// populate check box with event types
			foreach (EnumT theEvent in Enum.GetValues(typeof(EnumT)))
			{
				if(!ignoredValues.Contains(theEvent))
					Items.Add(theEvent);
			}
		}

		public event System.EventHandler ValueChanged;

		public System.Collections.Generic.List<EnumT> Selection
		{
			get
			{
				System.Collections.Generic.List<EnumT> retVal = new System.Collections.Generic.List<EnumT>();
				for (int i = 0; i < CheckedItems.Count; i++)
				{
					retVal.Add((EnumT)(CheckedItems[i]));
				}

				return retVal;
			}

			set
			{
				for (int i = 0; i < Items.Count; i++)
				{
					if (value.Contains((EnumT)(Items[i])))
					{
						SetItemChecked(i, true);
					}
					else
					{
						SetItemChecked(i, false);
					}
				}
			}
		}

		public void CheckAll()
		{
			for (int i = 0; i < Items.Count; i++)
			{
				SetItemChecked(i, true);
			}
		}

		public void CheckNone()
		{
			for (int i = 0; i < Items.Count; i++)
			{
				SetItemChecked(i, false);
			}
		}

		private void OnSelectedIndexChanged_(
			System.Object sender,
			System.EventArgs args)
		{
			if (ValueChanged != null)
				ValueChanged(sender, args);
		}
	}
}