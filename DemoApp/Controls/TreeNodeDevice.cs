/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

namespace DemoApp 
{
	/// <summary>
	/// Defines a delegate for handling device events
	/// </summary>
	/// <param name="source"></param>
	/// <param name="device">The device applicable to the event</param>
	public delegate void DeviceHandler(object source, AvigilonDotNet.IDevice device);

	/// <summary>
	/// Defines a tree node that holds device information
	/// </summary>
    public class TreeNodeDevice
        : System.Windows.Forms.TreeNode
    {
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="device">Device to display</param>
        public TreeNodeDevice(AvigilonDotNet.IDevice device)
        {
			// Set members
			m_device = device;
			m_device.InfoChanged += DeviceInfoChangeHandler_;
			m_device.EntityListAdd += DeviceEntityListAddHandler_;

			// Context menu
			m_deviceContextMenu = new System.Windows.Forms.ContextMenuStrip();
			m_deviceContextMenu.Opening += new System.ComponentModel.CancelEventHandler(UpdatePopupMenu_);
			this.ContextMenuStrip = m_deviceContextMenu;

			m_infoItem = new System.Windows.Forms.ToolStripMenuItem("Info");
			m_infoItem.Click += new System.EventHandler(OnInfoItemClick_);
			m_deviceContextMenu.Items.Add(m_infoItem);

			UpdateInfo();
			AddAllEntities();		
        }

		/// <summary>
		/// Raised with the user selects to display a device
		/// </summary>
		public event EntityHandler DisplayEntity;

		/// <summary>
		/// Raised with the user selects to export data from a device
		/// </summary>
		public event EntityHandler ExportEntity;

		/// <summary>
		/// Raised with the user selects to perform an image query on a device
		/// </summary>
		public event EntityHandler ImageQueryEntity;

		/// <summary>
		/// Extracts the name applied to the node
		/// </summary>
		/// <param name="device"></param>
		/// <returns></returns>
		public static System.String ExtractNodeName(AvigilonDotNet.IDevice device)
		{
			return "Device" + device.DeviceId.ToString();
		}

		/// <summary>
		/// Device info changed event handler
		/// </summary>
		private void DeviceInfoChangeHandler_()
        {
			UpdateInfo();
            if (TreeView != null)
                TreeView.Refresh();
        }

		private void DeviceEntityListAddHandler_(System.String entityId)
		{
			AvigilonDotNet.IEntity entity = m_device.GetEntityById(entityId);
			if (entity != null &&
				Nodes.Find(TreeNodeEntity.ExtractNodeName(entity), false).Length == 0)
			{
				// Does not already exist in the tree
				TreeNodeEntity newEntityNode = new TreeNodeEntity(entity);
				newEntityNode.DisplayEntity += OnDisplayEntity_;
				newEntityNode.ExportEntity += OnExportEntity_;
				newEntityNode.ImageQueryEntity += OnImageQueryEntity_;

				Nodes.Add(newEntityNode);
                if (TreeView != null)
                    TreeView.Refresh();
			}
		}

		/// <summary>
		/// Updates the information displayed based on the state of the device.
		/// </summary>
        private void UpdateInfo()
        {
            Name = ExtractNodeName(m_device);

            // Set text
            if (m_device.Valid)
            {
                Text = m_device.Name;
                if (m_device.Connected)
                {
                    Text += " Connected";
                }
            }
            else
            {
                Text = "Invalid";
            }
        }

		private void AddAllEntities()
		{
			System.Collections.Generic.List<AvigilonDotNet.IEntity> entities = m_device.Entities;
			foreach (AvigilonDotNet.IEntity entity in entities)
			{
				TreeNodeEntity newEntityNode = new TreeNodeEntity(entity);
				newEntityNode.DisplayEntity += OnDisplayEntity_;
				newEntityNode.ExportEntity += OnExportEntity_;
				newEntityNode.ImageQueryEntity += OnImageQueryEntity_;

				Nodes.Add(newEntityNode);
			}
		}


		/// <summary>
		/// Display device menu selection event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="device"></param>
		private void OnDisplayEntity_(
			System.Object sender,
			AvigilonDotNet.IEntity entity)
		{
			DisplayEntity(sender, entity);
		}

		/// <summary>
		/// Export device menu selection event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="device"></param>
		private void OnExportEntity_(
			System.Object sender,
			AvigilonDotNet.IEntity entity)
		{
			ExportEntity(sender, entity);
		}

		/// <summary>
		/// Image Query device menu selection event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="device"></param>
		private void OnImageQueryEntity_(
			System.Object sender,
			AvigilonDotNet.IEntity entity)
		{
			ImageQueryEntity(sender, entity);
		}

        /// <summary>
        /// Defines items for popup menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdatePopupMenu_(
            object sender,
            System.ComponentModel.CancelEventArgs e)
        {}

        /// <summary>
        /// Event handler for the info menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnInfoItemClick_(System.Object sender, System.EventArgs e)
        {
            DeviceInfoForm itemInfo = new DeviceInfoForm(m_device);
            itemInfo.Show();
        }

        private AvigilonDotNet.IDevice m_device;
        private System.Windows.Forms.ContextMenuStrip m_deviceContextMenu;
        private System.Windows.Forms.ToolStripMenuItem m_infoItem;
    };
} /* namespace DemoApp */