/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

using System.Windows.Forms;

namespace DemoApp
{
	/// <summary>
	/// Defines a delegate for handling nvr events
	/// </summary>
	/// <param name="source"></param>
	/// <param name="entity">The entity applicable to the event</param>
	public delegate void NvrHandler(object source, AvigilonDotNet.INvr nvr);

	/// <summary>
	/// Defines a tree node that holds nvr information
	/// </summary>
	public class TreeNodeNvr
		: TreeNodeDataStore
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="nvr">Nvr to display</param>
		public TreeNodeNvr(AvigilonDotNet.INvr nvr)
			: base(nvr)
		{
			m_nvr = nvr;
			m_nvr.InfoChanged += NvrInfoChangeHandler_;
			m_nvr.NvrReady += NvrReadyChangeHandler_;
			m_nvr.AlarmAdd += NvrAlarmAddHandler_;
			m_nvr.AlarmRemove += NvrAlarmListRemoveHandler_;

			m_loginItem = new System.Windows.Forms.ToolStripMenuItem("Login");
			m_loginItem.Click += new System.EventHandler(OnLoginItemClick_);
			m_dataStoreContextMenu.Items.Add(m_loginItem);

			m_logoutItem = new System.Windows.Forms.ToolStripMenuItem("Logout");
			m_logoutItem.Click += new System.EventHandler(OnLogoutItemClick_);
			m_dataStoreContextMenu.Items.Add(m_logoutItem);

			m_backupItem = new System.Windows.Forms.ToolStripMenuItem("Backup");
			m_backupItem.Click += new System.EventHandler(OnBackupItemClick_);
			m_dataStoreContextMenu.Items.Add(m_backupItem);

			m_removeItem = new System.Windows.Forms.ToolStripMenuItem("Remove");
			m_removeItem.Click += new System.EventHandler(OnRemoveItemClick_);
			m_dataStoreContextMenu.Items.Add(m_removeItem);

			m_infoItem = new System.Windows.Forms.ToolStripMenuItem("Info");
			m_infoItem.Click += new System.EventHandler(OnInfoItemClick_);
			m_dataStoreContextMenu.Items.Add(m_infoItem);

			m_licenseInfoItem = new System.Windows.Forms.ToolStripMenuItem("License Info");
			m_licenseInfoItem.Click += new System.EventHandler(OnLicenseInfoItemClick_);
			m_dataStoreContextMenu.Items.Add(m_licenseInfoItem);

            m_networkLANInfoItem = new System.Windows.Forms.ToolStripMenuItem("Network to LAN");
            m_networkLANInfoItem.Click += new System.EventHandler(OnNetworkLANInfoItemClick_);
            m_dataStoreContextMenu.Items.Add(m_networkLANInfoItem);

            m_networkWANInfoItem = new System.Windows.Forms.ToolStripMenuItem("Network to WAN");
            m_networkWANInfoItem.Click += new System.EventHandler(OnNetworkWANInfoItemClick_);
            m_dataStoreContextMenu.Items.Add(m_networkWANInfoItem);


			UpdateInfo_();
			AddAllAlarms_();
		}

		/// <summary>
		/// Raised when the user selects to perform a backup on a nvr
		/// </summary>
		public event NvrHandler BackupNvr;

		/// <summary>
		/// Raised when the user selects to remove a nvr
		/// </summary>
		public event NvrHandler RemoveNvr;

		/// <summary>
		/// The name that is applied to this node
		/// </summary>
		/// <param name="nvr"></param>
		/// <returns></returns>
		public static System.String ExtractNodeName(AvigilonDotNet.INvr nvr)
		{
			return nvr.Class.ToString() + " " + nvr.NvrUuid.ToString();
		}

		/// <summary>
		/// Nvr info change event handler
		/// </summary>
		private void NvrInfoChangeHandler_()
		{
			UpdateInfo_();
			if (TreeView != null)
				TreeView.Refresh();
		}

		/// <summary>
		/// Nvr ready change event handler
		/// </summary>
		private void NvrReadyChangeHandler_(bool bReady)
		{
			UpdateInfo_();
			if (TreeView != null)
				TreeView.Refresh();
		}

		/// <summary>
		/// Add alarms list change event handler
		/// </summary>
		private void NvrAlarmAddHandler_(System.String alarmId)
		{
			AvigilonDotNet.IAlarm alarm = m_nvr.GetAlarmById(alarmId);
			if (alarm != null &&
				Nodes.Find(TreeNodeAlarm.ExtractNodeName(alarm), false).Length == 0)
			{
				// Does not already exist in the tree
				TreeNodeAlarm newAlarmNode = new TreeNodeAlarm(alarm);
				Nodes.Add(newAlarmNode);
				if (TreeView != null)
					TreeView.Refresh();
			}
		}

		/// <summary>
		/// Remove alarms list change event handler
		/// </summary>
		private void NvrAlarmListRemoveHandler_(System.String alarmId)
		{
			for (int ix = 0; ix < Nodes.Count; ++ix)
			{
				if (Nodes[ix].GetType() != typeof(TreeNodeAlarm))
				{

				}
				else if (((TreeNodeAlarm)Nodes[ix]).AlarmId == alarmId)
				{
					// Node found, remove
					Nodes.RemoveAt(ix);
					return;
				}
			}
		}

		/// <summary>
		/// Updates the information displayed based on the state of the nvr.
		/// </summary>
		private void UpdateInfo_()
		{
			if (m_nvr.Valid)
			{
                Text = "Site";

				if (m_nvr.Ready)
					Text += "[r]: " + m_nvr.Name;
				else
					Text += "[nr]: " + m_nvr.Name;

				Text += " O:" + m_nvr.Online;
				Text += " A:" + m_nvr.Authenticated;

                AvigilonDotNet.NetworkConnectionType networkType = m_nvr.NvrNetworkConnectionType;
			    string nct = networkType.ToString();
			    Text += " C:" + nct;
               
				Name = ExtractNodeName(m_nvr);
				if (m_nvr.Address != null)
					ToolTipText = m_nvr.Address.ToString();
				else
					ToolTipText = "";
			}
			else
			{
				Text = "Invalid";
			}
		}

		private void AddAllAlarms_()
		{
			System.Collections.Generic.List<AvigilonDotNet.IAlarm> alarms = m_nvr.Alarms;
			foreach (AvigilonDotNet.IAlarm alarm in alarms)
			{
				TreeNodeAlarm newAlarmNode = new TreeNodeAlarm(alarm);
				Nodes.Add(newAlarmNode);
			}
		}

		/// <summary>
		/// Update items in the popup menu
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void UpdatePopupMenu_(
			object sender,
			System.ComponentModel.CancelEventArgs e)
		{
			m_logoutItem.Visible = m_nvr.Authenticated;
			m_queryItem.Visible = m_nvr.Authenticated;
			m_loginItem.Visible = true;
			m_copyUuidItem.Visible = true;
			m_backupItem.Visible = m_nvr.Authenticated;
			m_removeItem.Visible = true;
			m_infoItem.Visible = true;
			m_licenseInfoItem.Visible = true;
		}

		/// <summary>
		/// Event handler for the login menu item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLoginItemClick_(System.Object sender, System.EventArgs e)
		{
			LoginForm form = new LoginForm();
			if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				AvigilonDotNet.LoginResult result = m_nvr.Login(form.UserName, form.Password);

				if (result != AvigilonDotNet.LoginResult.Successful)
				{
					System.Windows.Forms.MessageBox.Show(
						"An error occurred while logging in to the NVR: " + result.ToString());
				}
			}
		}

		/// <summary>
		/// Event handler for the logout menu item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLogoutItemClick_(System.Object sender, System.EventArgs e)
		{
			m_nvr.Logout();
		}

		/// <summary>
		/// Event handler for the backup menu item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBackupItemClick_(System.Object sender, System.EventArgs e)
		{
			BackupNvr(sender, m_nvr);
		}

		/// <summary>
		/// Event handler for the remove menu item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRemoveItemClick_(System.Object sender, System.EventArgs e)
		{
			RemoveNvr(sender, m_nvr);
		}

		/// <summary>
		/// Event handler for the info menu item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnInfoItemClick_(System.Object sender, System.EventArgs e)
		{
			NvrInfoForm itemInfo = new NvrInfoForm(m_nvr);
			itemInfo.Show();
		}

		/// <summary>
		/// Event handler for the license info menu item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLicenseInfoItemClick_(System.Object sender, System.EventArgs e)
		{
			LicenseInfoForm itemInfo = new LicenseInfoForm(m_nvr.License);
			itemInfo.Show();
		}


        /// <summary>
        /// Event handler for the Set network to LAN
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnNetworkLANInfoItemClick_(System.Object sender, System.EventArgs e)
        {
            m_nvr.NvrNetworkConnectionType = AvigilonDotNet.NetworkConnectionType.LAN;
        }

        /// <summary>
        /// Event handler for the Set network to WAN
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnNetworkWANInfoItemClick_(System.Object sender, System.EventArgs e)
        {
            m_nvr.NvrNetworkConnectionType = AvigilonDotNet.NetworkConnectionType.WAN;
        }

		AvigilonDotNet.INvr m_nvr;
		private System.Windows.Forms.ToolStripMenuItem m_loginItem;
		private System.Windows.Forms.ToolStripMenuItem m_logoutItem;
		private System.Windows.Forms.ToolStripMenuItem m_backupItem;
		private System.Windows.Forms.ToolStripMenuItem m_removeItem;
		private System.Windows.Forms.ToolStripMenuItem m_infoItem;
		private System.Windows.Forms.ToolStripMenuItem m_licenseInfoItem;

        private System.Windows.Forms.ToolStripMenuItem m_networkLANInfoItem;
        private System.Windows.Forms.ToolStripMenuItem m_networkWANInfoItem;


	};
} /* namespace DemoApp */