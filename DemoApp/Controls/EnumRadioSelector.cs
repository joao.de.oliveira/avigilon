/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DemoApp
{
	public class EnumRadioSelector<EnumT>
		: System.Windows.Forms.UserControl
	{
		public event System.EventHandler ValueChanged;

		public EnumRadioSelector(int numRadioPerRow, int colSpacing)
		{
			m_radioButtons = new System.Collections.Generic.List<System.Windows.Forms.RadioButton>();

			// populate check box with event types
			bool isFirst = true;
			int rows = 0;
			int cols = 0;
			foreach (EnumT theEnum in Enum.GetValues(typeof(EnumT)))
			{
				System.Windows.Forms.RadioButton newRadio = new System.Windows.Forms.RadioButton();

				newRadio.Location = new System.Drawing.Point(cols * colSpacing, rows * 20);
				newRadio.AutoSize = true;
				newRadio.Tag = theEnum;
				newRadio.Text = theEnum.ToString();
				newRadio.Checked = isFirst; isFirst = false; // only check the first item
				newRadio.CheckedChanged += new EventHandler(CheckedChanged);
				
				m_radioButtons.Add(newRadio);
				Controls.Add(newRadio);

				++cols;
				if (cols >= numRadioPerRow)
				{
					cols = 0;
					++rows;
				}
			}

			this.Height = (rows + 1)* 20 + 5;
			this.Width = numRadioPerRow * colSpacing;
		}

		void CheckedChanged(object sender, EventArgs e)
		{
			if (ValueChanged != null)
				ValueChanged(sender, e);
		}

		/// <summary>
		/// Gets/sets the currently selected rouding value
		/// </summary>
		public EnumT Value
		{
			get
			{
				foreach (System.Windows.Forms.RadioButton radio in m_radioButtons)
				{
					if (radio.Checked)
						return (EnumT)radio.Tag;
				}

				return (EnumT)m_radioButtons[0].Tag;
			}

			set
			{
				foreach (System.Windows.Forms.RadioButton radio in m_radioButtons)
				{
					if (radio.Tag.Equals(value))
					{
						radio.Checked = true;
						return;
					}
				}
			}
		}

		private System.Collections.Generic.List<System.Windows.Forms.RadioButton> m_radioButtons;
	};
}