/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AvigilonDotNet;


namespace DemoApp
{
	public class ImageQualitySelector
		: System.Windows.Forms.UserControl
	{
		public ImageQualitySelector()
		{
			this.Size = new System.Drawing.Size(150, 75);
			m_iqMax = new System.Windows.Forms.RadioButton();
			m_iqHigh = new System.Windows.Forms.RadioButton();
			m_iqMedium = new System.Windows.Forms.RadioButton();
			m_iqLow = new System.Windows.Forms.RadioButton();

			Controls.Add(m_iqMax);
			Controls.Add(m_iqHigh);
			Controls.Add(m_iqMedium);
			Controls.Add(m_iqLow);

			m_iqMax.Location = new System.Drawing.Point(0, 0);
			m_iqMax.CheckedChanged += OnCheckChanged_;
			m_iqMax.AutoSize = true;
			m_iqMax.Text = "Max Image Quality";

			m_iqHigh.Location = new System.Drawing.Point(0, 20);
			m_iqHigh.CheckedChanged += OnCheckChanged_;
			m_iqHigh.AutoSize = true;
			m_iqHigh.Text = "High Image Quality";

			m_iqMedium.Location = new System.Drawing.Point(0, 40);
			m_iqMedium.CheckedChanged += OnCheckChanged_;
			m_iqMedium.AutoSize = true;
			m_iqMedium.Text = "Med Image Quality";

			m_iqLow.Location = new System.Drawing.Point(0, 60);
			m_iqLow.CheckedChanged += OnCheckChanged_;
			m_iqLow.AutoSize = true;
			m_iqLow.Text = "Min Image Quality";
		}

		public event System.EventHandler ValueChanged;

		public AvigilonDotNet.ImageQuality Value
		{
			get
			{
				if (m_iqMax.Checked)
					return AvigilonDotNet.ImageQuality.Max;
				else if (m_iqHigh.Checked)
					return AvigilonDotNet.ImageQuality.High;
				else if (m_iqMedium.Checked)
					return AvigilonDotNet.ImageQuality.Medium;
				else if (m_iqLow.Checked)
					return AvigilonDotNet.ImageQuality.Low;
				else
					return AvigilonDotNet.ImageQuality.High;
			}

			set
			{
				switch (value)
				{
					case AvigilonDotNet.ImageQuality.Max:
						m_iqMax.Checked = true;
						break;
					case AvigilonDotNet.ImageQuality.High:
						m_iqHigh.Checked = true;
						break;
					case AvigilonDotNet.ImageQuality.Medium:
						m_iqMedium.Checked = true;
						break;
					case AvigilonDotNet.ImageQuality.Low:
						m_iqLow.Checked = true;
						break;
					default:
						break;
				}
			}
		}

		private void OnCheckChanged_(
			System.Object sender,
			System.EventArgs args)
		{
			if (ValueChanged != null)
				ValueChanged(sender, args);
		}

		private System.Windows.Forms.RadioButton m_iqMax;
		private System.Windows.Forms.RadioButton m_iqHigh;
		private System.Windows.Forms.RadioButton m_iqMedium;
		private System.Windows.Forms.RadioButton m_iqLow;
	}
}