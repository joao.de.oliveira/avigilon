﻿/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

namespace DemoApp
{
	/// <summary>
	/// Defines a delegate for handling archive events
	/// </summary>
	/// <param name="source"></param>
	/// <param name="entity">The entity applicable to the event</param>
	public delegate void ArchiveHandler(object source, AvigilonDotNet.IFileArchive archive);

	/// <summary>
	/// Defines a tree node that holds archive information
	/// </summary>
	public class TreeNodeArchive
		: TreeNodeDataStore
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="archive">archive to display</param>
		public TreeNodeArchive(AvigilonDotNet.IFileArchive archive)
			: base(archive)
		{
			m_archive = archive;
            m_archive.InfoChanged += ArchiveInfoChangeHandler_;

			// Context menu
			m_closeItem = new System.Windows.Forms.ToolStripMenuItem("Close");
			m_closeItem.Click += new System.EventHandler(OnCloseItemClick_);
			m_dataStoreContextMenu.Items.Add(m_closeItem);

            m_infoItem = new System.Windows.Forms.ToolStripMenuItem("Info");
            m_infoItem.Click += new System.EventHandler(OnInfoItemClick_);
            m_dataStoreContextMenu.Items.Add(m_infoItem);

			UpdateInfo_();
		}

		/// <summary>
		/// The name that is applied to this node
		/// </summary>
		/// <param name="archive"></param>
		/// <returns></returns>
		public static System.String ExtractNodeName(AvigilonDotNet.IFileArchive archive)
		{
			return archive.Class.ToString() + " " + archive.DataStoreUuid.ToString();
		}

		/// <summary>
		/// archive info change event handler
		/// </summary>
		private void ArchiveInfoChangeHandler_()
		{
			UpdateInfo_();
            if (TreeView != null)
                TreeView.Refresh();
		}

		/// <summary>
		/// Updates the information displayed based on the state of the archive.
		/// </summary>
		private void UpdateInfo_()
		{
			if (m_archive.Valid)
			{
				Text = m_archive.Class.ToString();
				if (m_archive.IsOpen)
				{
					Text += " [" + m_archive.FileArchivePath + "]";
				}
				else
				{
					Text += " [closed]";
				}

				Name = ExtractNodeName(m_archive);
				if (m_archive.DataStoreUuid != null)
					ToolTipText = m_archive.DataStoreUuid.ToString();
				else
					ToolTipText = "";
			}
			else
			{
				Text = "Invalid";
			}
		}

		/// <summary>
		/// Update items in the popup menu
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void UpdatePopupMenu_(
			object sender,
			System.ComponentModel.CancelEventArgs e)
		{
			m_queryItem.Visible = m_archive.IsOpen;
			m_copyUuidItem.Visible = true;
			m_closeItem.Visible = m_archive.IsOpen;
            m_infoItem.Visible = true;
		}

		/// <summary>
		/// Event handler for the close menu item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCloseItemClick_(System.Object sender, System.EventArgs e)
		{
			AvigilonDotNet.AvgError err = m_archive.Close();
			if (AvigilonDotNet.ErrorHelper.IsError(err))
			{
				System.Windows.Forms.MessageBox.Show("Failed to close: " + err.ToString());
			}
		}

        /// <summary>
        /// Event handler for the info menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnInfoItemClick_(System.Object sender, System.EventArgs e)
        {
            ArchiveInfoForm itemInfo = new ArchiveInfoForm(m_archive);
            itemInfo.Show();
        }
	
		private AvigilonDotNet.IFileArchive m_archive;
		private System.Windows.Forms.ToolStripMenuItem m_closeItem;
        private System.Windows.Forms.ToolStripMenuItem m_infoItem;
	};
} /* namespace DemoApp */