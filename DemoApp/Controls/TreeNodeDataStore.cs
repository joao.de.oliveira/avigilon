﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DemoApp
{
	public abstract class TreeNodeDataStore
		: System.Windows.Forms.TreeNode
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="dataStore">DataStore to display</param>
		public TreeNodeDataStore(AvigilonDotNet.IDataStore dataStore)
		{
			m_dataStore = dataStore;

			m_dataStore.DeviceListAdd += DataStoreDeviceListAddHandler_;
			m_dataStore.TextSourceAdd += DataStoreTextSourceListAddAddHandler_;
			m_dataStore.TextSourceRemove += DataStoreTextSourceListAddRemoveHandler_;
			m_dataStore.BookmarkAdd += DateStoreBookmarkAddHandler_;
			m_dataStore.BookmarkRemove += DataStoreBookmarkRemoveHandler_;

			// Context menu
			m_dataStoreContextMenu = new System.Windows.Forms.ContextMenuStrip();
			m_dataStoreContextMenu.Opening += new System.ComponentModel.CancelEventHandler(UpdatePopupMenu_);
			this.ContextMenuStrip = m_dataStoreContextMenu;

			m_queryItem = new System.Windows.Forms.ToolStripMenuItem("Query");
			m_queryItem.Click += new System.EventHandler(OnQueryItemClick_);
			m_dataStoreContextMenu.Items.Add(m_queryItem);

			m_copyUuidItem = new System.Windows.Forms.ToolStripMenuItem("Copy Uuid");
			m_copyUuidItem.Click += new System.EventHandler(OnCopyUuidItemClick_);
			m_dataStoreContextMenu.Items.Add(m_copyUuidItem);

			AddAllDevices_();
			AddAllTextSources_();
			AddAllBookmarks_();
		}

		/// <summary>
		/// Raised with the user selects to display a device
		/// </summary>
		public event EntityHandler DisplayEntity;

		/// <summary>
		/// Raised with the user selects to export data from a device
		/// </summary>
		public event EntityHandler ExportEntity;

		/// <summary>
		/// Raised with the user selects to perform an image query on a device
		/// </summary>
		public event EntityHandler ImageQueryEntity;
		
		/// <summary>
		/// DataStore device list change event handler
		/// </summary>
		private void DataStoreDeviceListAddHandler_(System.String deviceId)
		{
			AvigilonDotNet.IDevice device = m_dataStore.GetDeviceById(deviceId);
			if (device != null &&
				Nodes.Find(TreeNodeDevice.ExtractNodeName(device), false).Length == 0)
			{
				// Does not already exist in the tree
				TreeNodeDevice newDeviceNode = new TreeNodeDevice(device);
				newDeviceNode.DisplayEntity += OnDisplayEntity_;
				newDeviceNode.ExportEntity += OnExportEntity_;
				newDeviceNode.ImageQueryEntity += OnImageQueryEntity_;

				Nodes.Add(newDeviceNode);
                if (TreeView != null)
                    TreeView.Refresh();
			}
		}

		private void AddAllDevices_()
		{
			System.Collections.Generic.List<AvigilonDotNet.IDevice> devices = m_dataStore.Devices;
			foreach (AvigilonDotNet.IDevice device in devices)
			{
				TreeNodeDevice newDeviceNode = new TreeNodeDevice(device);
				newDeviceNode.DisplayEntity += OnDisplayEntity_;
				newDeviceNode.ExportEntity += OnExportEntity_;
				newDeviceNode.ImageQueryEntity += OnImageQueryEntity_;

				Nodes.Add(newDeviceNode);
			}
		}

		/// <summary>
		/// DataStore text source list change event handler
		/// </summary>
		private void DataStoreTextSourceListAddAddHandler_(System.String textSourceId)
		{
			AvigilonDotNet.ITextSource textSource = m_dataStore.GetTextSourceById(textSourceId);
			if (textSource != null &&
				Nodes.Find(TreeNodeTextSource.ExtractNodeName(textSource), false).Length == 0)
			{
				// Does not already exist in the tree
				TreeNodeTextSource newTextSourceNode = new TreeNodeTextSource(textSource);
				Nodes.Add(newTextSourceNode);
                if (TreeView != null)
                    TreeView.Refresh();
			}
		}

		/// <summary>
		/// Remove alarms list change event handler
		/// </summary>
		private void DataStoreTextSourceListAddRemoveHandler_(System.String textSourceId)
		{
			for (int ix = 0; ix < Nodes.Count; ++ix)
			{
				if (Nodes[ix].GetType() != typeof(TreeNodeTextSource))
				{

				}
				else if (((TreeNodeTextSource)Nodes[ix]).TextSourceId == textSourceId)
				{
					// Node found, remove
					Nodes.RemoveAt(ix);
					return;
				}
			}
		}

		private void DateStoreBookmarkAddHandler_(string bookmarkId)
		{
			AvigilonDotNet.IBookmark bookmark = m_dataStore.GetBookmarkById(bookmarkId);
			if (bookmark != null &&
				Nodes.Find(TreeNodeBookmark.ExtractNodeName(bookmark), false).Length == 0)
			{

				// Does not already exist in the tree
				TreeNodeBookmark newBookmarkNode = new TreeNodeBookmark(bookmark);
				Nodes.Add(newBookmarkNode);
				if (TreeView != null)
					TreeView.Refresh();
			}
		}

		public void DataStoreBookmarkRemoveHandler_(string bookmarkId)
		{
			for (int ix = 0; ix < Nodes.Count; ++ix)
			{
				if (Nodes[ix].GetType() != typeof(TreeNodeBookmark))
				{

				}
				else if (((TreeNodeBookmark)Nodes[ix]).BookmarkId == bookmarkId)
				{
					// Node found, remove
					Nodes.RemoveAt(ix);
					return;
				}
			}
		}

		private void AddAllTextSources_()
		{
			System.Collections.Generic.List<AvigilonDotNet.ITextSource> textSources = m_dataStore.TextSources;
			foreach (AvigilonDotNet.ITextSource textSource in textSources)
			{
				TreeNodeTextSource newTextSourceTreeNode = new TreeNodeTextSource(textSource);
				Nodes.Add(newTextSourceTreeNode);
			}
		}

		private void AddAllBookmarks_()
		{
			System.Collections.Generic.List<AvigilonDotNet.IBookmark> bookmarks= m_dataStore.Bookmarks;
			foreach (AvigilonDotNet.IBookmark bookmark in bookmarks)
			{
				TreeNodeBookmark newBookmarkTreeNode = new TreeNodeBookmark(bookmark);
				Nodes.Add(newBookmarkTreeNode);
			}
		}

		/// <summary>
		/// Update items in the popup menu
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void UpdatePopupMenu_(
			object sender,
			System.ComponentModel.CancelEventArgs e)
		{}

		/// <summary>
		/// Event handler for the query menu item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnQueryItemClick_(System.Object sender, System.EventArgs e)
		{
			// Bring up query dialog for this dataStore
			QueryForm queryForm = new QueryForm(m_dataStore);
			queryForm.Show();
		}

		/// <summary>
		/// Event handler for the copy uuid menu item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCopyUuidItemClick_(System.Object sender, System.EventArgs e)
		{
			System.Windows.Forms.Clipboard.SetText(m_dataStore.DataStoreUuid.ToString());
		}

		/// <summary>
		/// Display device menu selection event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="device"></param>
		private void OnDisplayEntity_(
			System.Object sender,
			AvigilonDotNet.IEntity entity)
		{
			DisplayEntity(sender, entity);
		}

		/// <summary>
		/// Export device menu selection event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="device"></param>
		private void OnExportEntity_(
			System.Object sender,
			AvigilonDotNet.IEntity entity)
		{
			ExportEntity(sender, entity);
		}

		/// <summary>
		/// Image Query device menu selection event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="device"></param>
		private void OnImageQueryEntity_(
			System.Object sender,
			AvigilonDotNet.IEntity entity)
		{
			ImageQueryEntity(sender, entity);
		}

		protected AvigilonDotNet.IDataStore m_dataStore;
		protected System.Windows.Forms.ContextMenuStrip m_dataStoreContextMenu;

		protected System.Windows.Forms.ToolStripMenuItem m_queryItem;
		protected System.Windows.Forms.ToolStripMenuItem m_copyUuidItem;
	}
}
