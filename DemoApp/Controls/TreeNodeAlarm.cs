/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DemoApp 
{
	/// <summary>
	/// Form to take in alarm trigger information
	/// </summary>
	public class AlarmNoteForm
		: System.Windows.Forms.Form
	{
		/// <summary>
		/// Constructor
		/// </summary>
		public AlarmNoteForm()
		{
			this.Size = new System.Drawing.Size(300, 200);

			System.Windows.Forms.Label noteLabel = new System.Windows.Forms.Label();
			noteLabel.Location = new System.Drawing.Point(10, 40);
			noteLabel.Text = "Note:";
			noteLabel.AutoSize = true;
			Controls.Add(noteLabel);

			m_noteEdit = new System.Windows.Forms.TextBox();
			m_noteEdit.Location = new System.Drawing.Point(150, 40);
			Controls.Add(m_noteEdit);

			m_okButton = new System.Windows.Forms.Button();
			m_okButton.Location = new System.Drawing.Point(Right - 10 - m_okButton.Width, 70);
			m_okButton.Text = "Ok";
			m_okButton.Click += new EventHandler(OkButtonClick_);
			Controls.Add(m_okButton);

			m_cancelButton = new System.Windows.Forms.Button();
			m_cancelButton.Location = new System.Drawing.Point(Right - 20 - 2 * m_okButton.Width, 70);
			m_cancelButton.Text = "Cancel";
			m_cancelButton.AutoSize = true;
			m_cancelButton.Click += new EventHandler(CancelButtonClick_);
			Controls.Add(m_cancelButton);
		}

		/// <summary>
		/// Gets the note entered by the user
		/// </summary>
		public System.String Note
		{
			get
			{
				return m_noteEdit.Text;
			}
		}

		/// <summary>
		/// Ok button clicked event handler
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="e"></param>
		void OkButtonClick_(
			System.Object obj,
			System.EventArgs e)
		{
			DialogResult = DialogResult.OK;
			Close();
		}

		/// <summary>
		/// Cancel button clicked event handler
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="e"></param>
		void CancelButtonClick_(
			System.Object obj,
			System.EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
			Close();
		}

		private System.Windows.Forms.TextBox m_noteEdit;
		private System.Windows.Forms.Button m_okButton;
		private System.Windows.Forms.Button m_cancelButton;
	};

	/// <summary>
	/// Form to take in alarm acknowledge information
	/// </summary>
	public class AcknowledgeAlarmForm
		: System.Windows.Forms.Form
	{
		/// <summary>
		/// Constructor
		/// </summary>
		public AcknowledgeAlarmForm()
		{
			this.Size = new System.Drawing.Size(300, 200);

			Text = "Acknowledge Alarm";

			System.Windows.Forms.Label noteLabel = new System.Windows.Forms.Label();
			noteLabel.Location = new System.Drawing.Point(10, 40);
			noteLabel.Text = "Acknowledge Note:";
			noteLabel.AutoSize = true;
			Controls.Add(noteLabel);

			m_noteEdit = new System.Windows.Forms.TextBox();
			m_noteEdit.Location = new System.Drawing.Point(150, 40);
			Controls.Add(m_noteEdit);

			System.Windows.Forms.Label ackPermLabel = new System.Windows.Forms.Label();
			ackPermLabel.Location = new System.Drawing.Point(10, 70);
			ackPermLabel.Text = "Ack Permission:";
			ackPermLabel.AutoSize = true;
			Controls.Add(ackPermLabel);

			m_ackPermissionCombo = new System.Windows.Forms.ComboBox();
			m_ackPermissionCombo.Location = new System.Drawing.Point(150, 70);
			m_ackPermissionCombo.Items.Add(AvigilonDotNet.AlarmAcknowledgePermission.Normal);
			m_ackPermissionCombo.Items.Add(AvigilonDotNet.AlarmAcknowledgePermission.Grant);
			m_ackPermissionCombo.Items.Add(AvigilonDotNet.AlarmAcknowledgePermission.Deny);
			m_ackPermissionCombo.SelectedIndex = 0;
			Controls.Add(m_ackPermissionCombo);

			m_okButton = new System.Windows.Forms.Button();
			m_okButton.Location = new System.Drawing.Point(Right - 10 - m_okButton.Width, 100);
			m_okButton.Text = "Ok";
			m_okButton.AutoSize = true;
			m_okButton.Click += new EventHandler(OkButtonClick_);
			Controls.Add(m_okButton);

			m_cancelButton = new System.Windows.Forms.Button();
			m_cancelButton.Location = new System.Drawing.Point(Right - 20 - 2 * m_okButton.Width, 100);
			m_cancelButton.Text = "Cancel";
			m_cancelButton.AutoSize = true;
			m_cancelButton.Click += new EventHandler(CancelButtonClick_);
			Controls.Add(m_cancelButton);
		}

		/// <summary>
		/// Gets the note
		/// </summary>
		public System.String AcknowledgeNote
		{
			get
			{
				return m_noteEdit.Text;
			}
		}

		/// <summary>
		/// Gets the ack permission
		/// </summary>
		public AvigilonDotNet.AlarmAcknowledgePermission AcknowledgePermission
		{
			get
			{
				return (AvigilonDotNet.AlarmAcknowledgePermission)m_ackPermissionCombo.SelectedItem;
			}
		}

		/// <summary>
		/// Ok button clicked event handler
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="e"></param>
		void OkButtonClick_(
			System.Object obj,
			System.EventArgs e)
		{
			DialogResult = DialogResult.OK;
			Close();
		}

		/// <summary>
		/// Cancel button clicked event handler
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="e"></param>
		void CancelButtonClick_(
			System.Object obj,
			System.EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
			Close();
		}

		private System.Windows.Forms.TextBox m_noteEdit;
		private System.Windows.Forms.ComboBox m_ackPermissionCombo;
		private System.Windows.Forms.Button m_okButton;
		private System.Windows.Forms.Button m_cancelButton;
	};

	/// <summary>
	/// Defines a tree node that holds alarm information
	/// </summary>
    public class TreeNodeAlarm
        : System.Windows.Forms.TreeNode
    {
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="alarm">alarm to display</param>
        public TreeNodeAlarm(AvigilonDotNet.IAlarm alarm)
		{        
            m_alarm = alarm;
            m_alarm.InfoChanged += AlarmInfoChangeHandler_;

            // Context menu
            m_alarmContextMenu = new System.Windows.Forms.ContextMenuStrip();
            m_alarmContextMenu.Opening += new System.ComponentModel.CancelEventHandler(UpdatePopupMenu_);
            this.ContextMenuStrip = m_alarmContextMenu;
        
			m_triggerItem = new System.Windows.Forms.ToolStripMenuItem("Trigger");
			m_triggerItem.Click += new System.EventHandler(OnTriggerItemClick_);
            m_alarmContextMenu.Items.Add(m_triggerItem);

			m_acknowledgeItem = new System.Windows.Forms.ToolStripMenuItem("Acknowledge");
			m_acknowledgeItem.Click += new System.EventHandler(OnAcknowledgeItemClick_);
            m_alarmContextMenu.Items.Add(m_acknowledgeItem);

			m_purgeItem = new System.Windows.Forms.ToolStripMenuItem("Purge");
			m_purgeItem.Click += new System.EventHandler(OnPurgeItemClick_);
			m_alarmContextMenu.Items.Add(m_purgeItem);

            m_infoItem = new System.Windows.Forms.ToolStripMenuItem("Info");
            m_infoItem.Click += new System.EventHandler(OnInfoItemClick_);
            m_alarmContextMenu.Items.Add(m_infoItem);

			UpdateInfo_();
        }

        


        public System.String AlarmId
		{
			get
			{
                if (m_alarm != null)
                    return m_alarm.AlarmId;
                else
                    return "";
			}
		}

		/// <summary>
		/// The name that is applied to this node
		/// </summary>
		/// <param name="alarm"></param>
		/// <returns></returns>
		public static System.String ExtractNodeName(AvigilonDotNet.IAlarm alarm)
		{
			return "Alarm" + alarm.AlarmId;
		}

		/// <summary>
		/// alarm info change event handler
		/// </summary>
        private void AlarmInfoChangeHandler_()
        {
			UpdateInfo_();
            if(TreeView != null)
			    TreeView.Refresh();
        }

		/// <summary>
		/// Updates the information displayed based on the state of the alarm.
		/// </summary>
        private void UpdateInfo_()
        {
			Name = ExtractNodeName(m_alarm);

			if (m_alarm.Valid)
			{
				Text = "alarm: " + m_alarm.Name;
				Text += " state:";
				Text += m_alarm.State.ToString();

				ToolTipText = m_alarm.Type.ToString();
			}
			else
			{
				Text = "Alarm invalid";
			}
        }

		/// <summary>
		/// Update items in the popup menu
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void UpdatePopupMenu_(
            object sender, 
            System.ComponentModel.CancelEventArgs e)
        {}

		/// <summary>
		/// Event handler for the trigger menu item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void OnTriggerItemClick_(System.Object sender, System.EventArgs e)
        {
			AlarmNoteForm triggerForm = new AlarmNoteForm();
			triggerForm.Text = "Trigger Alarm";
			if (triggerForm.ShowDialog() == DialogResult.OK)
			{
				m_alarm.TriggerAlarm(triggerForm.Note);
			}
		}

		/// <summary>
		/// Event handler for the acknowledge menu item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void OnAcknowledgeItemClick_(System.Object sender, System.EventArgs e)
        {
			AcknowledgeAlarmForm ackForm = new AcknowledgeAlarmForm();
			if (ackForm.ShowDialog() == DialogResult.OK)
			{
				m_alarm.AcknowledgeAlarm(ackForm.AcknowledgePermission,
					ackForm.AcknowledgeNote);
			}
        }

		/// <summary>
		/// Event handler for the purge menu item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPurgeItemClick_(System.Object sender, System.EventArgs e)
        {
			AlarmNoteForm purgeForm = new AlarmNoteForm();
			purgeForm.Text = "Purge Alarm";
			if (purgeForm.ShowDialog() == DialogResult.OK)
			{
				m_alarm.PurgeAlarm(purgeForm.Note);
			}
        }

        /// <summary>
        /// Event handler for the info menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnInfoItemClick_(System.Object sender, System.EventArgs e)
        {
            AlarmInfoForm itemInfo = new AlarmInfoForm(m_alarm);
            itemInfo.Show();
        }

        private AvigilonDotNet.IAlarm m_alarm;
        private System.Windows.Forms.ContextMenuStrip m_alarmContextMenu;
		private System.Windows.Forms.ToolStripMenuItem m_triggerItem;
		private System.Windows.Forms.ToolStripMenuItem m_acknowledgeItem;
		private System.Windows.Forms.ToolStripMenuItem m_purgeItem;
        private System.Windows.Forms.ToolStripMenuItem m_infoItem;
	};
} /* namespace DemoApp */