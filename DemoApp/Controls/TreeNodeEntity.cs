/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

namespace DemoApp 
{
	/// <summary>
	/// Defines a delegate for handling entity events
	/// </summary>
	/// <param name="source"></param>
	/// <param name="entity">The entity applicable to the event</param>
	public delegate void EntityHandler(object source, AvigilonDotNet.IEntity entity);

	/// <summary>
	/// Defines a tree node that holds entity information
	/// </summary>
    public class TreeNodeEntity
        : System.Windows.Forms.TreeNode
    {
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="entity">entity to display</param>
        public TreeNodeEntity(AvigilonDotNet.IEntity entity)
        {
			// Set members
            m_entity = entity;
            m_entity.InfoChanged += HandleEntityInfoChange_;
            
            if (m_entity.Type == AvigilonDotNet.EntityType.Camera)
            {
                AvigilonDotNet.IEntityCamera entityCam = (AvigilonDotNet.IEntityCamera)m_entity;
                entityCam.ManualRecordStateChange += new AvigilonDotNet.ManualRecordStateChangeHandler(HandleManualRecordStateChange_);
                entityCam.ManualRecordError += new AvigilonDotNet.ManualRecordErrorHandler(HandleManualRecordError_);
				entityCam.MotionDetectedStateChange += new AvigilonDotNet.MotionDetectedStateChangeHandler(HandleMotionDetectedStateChange_);
            }

            // Context menu
			m_entityContextMenu = new System.Windows.Forms.ContextMenuStrip();
            m_entityContextMenu.Opening += new System.ComponentModel.CancelEventHandler(UpdatePopupMenu_);
			this.ContextMenuStrip = m_entityContextMenu;
			
			m_displayItem = new System.Windows.Forms.ToolStripMenuItem("Display");
			m_displayItem.Click += new System.EventHandler(OnDisplayItemClick_);
            m_entityContextMenu.Items.Add(m_displayItem);

			m_exportItem = new System.Windows.Forms.ToolStripMenuItem("Export");
			m_exportItem.Click += new System.EventHandler(OnExportItemClick_);
            m_entityContextMenu.Items.Add(m_exportItem);

			m_imageQueryItem = new System.Windows.Forms.ToolStripMenuItem("Image Query");
			m_imageQueryItem.Click += new System.EventHandler(OnImageQueryItemClick_);
            m_entityContextMenu.Items.Add(m_imageQueryItem);

			m_startManualRecItem = new System.Windows.Forms.ToolStripMenuItem("Start Manual Rec");
			m_startManualRecItem.Click += new System.EventHandler(OnStartManualRecItemClick);
            m_entityContextMenu.Items.Add(m_startManualRecItem);

			m_endManualRecItem = new System.Windows.Forms.ToolStripMenuItem("End Manual Rec");
			m_endManualRecItem.Click += new System.EventHandler(OnEndManualRecItemClick);
            m_entityContextMenu.Items.Add(m_endManualRecItem);

            m_infoItem = new System.Windows.Forms.ToolStripMenuItem("Info");
            m_infoItem.Click += new System.EventHandler(OnInfoItemClick_);
            m_entityContextMenu.Items.Add(m_infoItem);

            m_triggerItem = new System.Windows.Forms.ToolStripMenuItem("Trigger");
            m_triggerItem.Click += new System.EventHandler(OnTriggerItemClick_);
            m_entityContextMenu.Items.Add(m_triggerItem);

			m_lockUnlockItem = new System.Windows.Forms.ToolStripMenuItem("Lock");
			m_lockUnlockItem.Click += new System.EventHandler(OnLockUnlockItemClick_);
			m_entityContextMenu.Items.Add(m_lockUnlockItem); 

            UpdateInfo();			
        }
		
		/// <summary>
		/// Raised with the user selects to display a entity
		/// </summary>
		public event EntityHandler DisplayEntity;

		/// <summary>
		/// Raised with the user selects to export data from a entity
		/// </summary>
		public event EntityHandler ExportEntity;

		/// <summary>
		/// Raised with the user selects to perform an image query on a entity
		/// </summary>
		public event EntityHandler ImageQueryEntity;

		/// <summary>
		/// Extracts the name applied to the node
		/// </summary>
		/// <param name="entity"></param>
		/// <returns></returns>
		public static System.String ExtractNodeName(AvigilonDotNet.IEntity entity)
		{
			return "Entity" + entity.DeviceId.ToString();
		}

		/// <summary>
		/// entity info changed event handler
		/// </summary>
		private void HandleEntityInfoChange_()
        {
			UpdateInfo();
            if (TreeView != null)
                TreeView.Refresh();
        }

		/// <summary>
		/// Manual record state event change handler
		/// </summary>
		void HandleManualRecordStateChange_(string nvrUuid, string deviceId, bool bIsManuallyRecording)
		{
			System.Diagnostics.Trace.Assert(m_entity.Type == AvigilonDotNet.EntityType.Camera);

			UpdateInfo();
            if (TreeView != null)
                TreeView.Refresh();
		}

		/// <summary>
		/// Manual record state error handler
		/// </summary>
		void HandleManualRecordError_(string nvrUuid, string deviceId, bool bIsManuallyRecording, AvigilonDotNet.AvgError error)
		{
			System.Diagnostics.Trace.Assert(m_entity.Type == AvigilonDotNet.EntityType.Camera);

			UpdateInfo();
            if (TreeView != null)
                TreeView.Refresh();

			System.Windows.Forms.MessageBox.Show("Failed to set manual record state: " + error.ToString());
		}

		/// <summary>
		/// Motion detected state event change handler
		/// </summary>
		void HandleMotionDetectedStateChange_(string nvrUuid, string deviceId, bool bIsMotionDetected)
		{
			System.Diagnostics.Trace.Assert(m_entity.Type == AvigilonDotNet.EntityType.Camera);

			UpdateInfo();
			if (TreeView != null)
				TreeView.Refresh();
		}

		/// <summary>
		/// Updates the information displayed based on the state of the entity.
		/// </summary>
        private void UpdateInfo()
        {
            Name = ExtractNodeName(m_entity);

            // Set text
            if (!m_entity.Valid)
            {
				Text = "Invalid";
			}
			else
			{
				if (m_entity.Enabled)
				{
					Text = "En: ";
				}
				else
				{
					Text = "Di: ";
				}

				if (m_entity.LogicalId != AvigilonDotNet.Constants.InvalidLogicalId)
				{
					Text += "{" + m_entity.LogicalId + "} ";
				}
				else
				{
					Text += "{-} ";
				}

				Text += "[" + m_entity.Type.ToString();

				if (m_entity.Type == AvigilonDotNet.EntityType.Camera)
				{
					AvigilonDotNet.IEntityCamera entityCam = (AvigilonDotNet.IEntityCamera)m_entity;
					Text += " ";
					Text += entityCam.IsMotionDetected ? "M" : "-";
					Text += entityCam.IsManualRecord ? "R" : "-";
				}
				Text += "] ";
                Text += m_entity.Name;

				
            }
        }

		/// <summary>
		/// Defines items for popup menu
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void UpdatePopupMenu_(
			object sender,
			System.ComponentModel.CancelEventArgs e)
		{
            m_exportItem.Visible = false;
            m_displayItem.Visible = true;
            m_exportItem.Visible = false;
            m_imageQueryItem.Visible = false;
            m_startManualRecItem.Visible = false;
            m_endManualRecItem.Visible = false;
            m_triggerItem.Visible = false;
			m_lockUnlockItem.Visible = false;

            switch (m_entity.Type)
            {
                case AvigilonDotNet.EntityType.Camera:
                    m_displayItem.Visible = true;
                    m_exportItem.Visible = true;
                    m_imageQueryItem.Visible = true;
                    m_startManualRecItem.Visible = true;
                    m_endManualRecItem.Visible = true;
                    break;

                case AvigilonDotNet.EntityType.DigitalOutput:
                    m_triggerItem.Visible = true;
                    break;

				case AvigilonDotNet.EntityType.Ptz:
					{
						m_lockUnlockItem.Visible = true;

						AvigilonDotNet.IEntityPtz entityPtz = (AvigilonDotNet.IEntityPtz)m_entity;
						m_lockUnlockItem.Text = entityPtz.HasPtzControlsLock ? "Unlock" : "Lock";
					}
					break;
            }
		}

		/// <summary>
		/// Called when the 'Display' menu item is clicked
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDisplayItemClick_(System.Object sender, System.EventArgs e)
		{
			// Raise display event
			DisplayEntity(e,m_entity);
		}

		/// <summary>
		/// Called when the 'Export' menu item is clicked
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExportItemClick_(System.Object sender, System.EventArgs e)
		{
			// Raise display event
			ExportEntity(e,m_entity);
		}

		/// <summary>
		/// Called when the 'ImageQuery' menu item is clicked
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnImageQueryItemClick_(System.Object sender, System.EventArgs e)
		{
			// Raise display event
			ImageQueryEntity(e, m_entity);
		}

		/// <summary>
		/// Called when the 'Start Manual Recording' menu item is clicked
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnStartManualRecItemClick(System.Object sender, System.EventArgs e)
		{
			System.Diagnostics.Trace.Assert(m_entity.Type == AvigilonDotNet.EntityType.Camera);

			AvigilonDotNet.IEntityCamera entityCam = (AvigilonDotNet.IEntityCamera)m_entity;
			entityCam.SetManualRecord(true);
		}

		/// <summary>
		/// Called when the 'End Manual Recording' menu item is clicked
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEndManualRecItemClick(System.Object sender, System.EventArgs e)
		{
			System.Diagnostics.Trace.Assert(m_entity.Type == AvigilonDotNet.EntityType.Camera);

			AvigilonDotNet.IEntityCamera entityCam = (AvigilonDotNet.IEntityCamera)m_entity;
			entityCam.SetManualRecord(false);
		}

        /// <summary>
        /// Event handler for the info menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnInfoItemClick_(System.Object sender, System.EventArgs e)
        {
			EntityInfoForm eif = null;
			switch (m_entity.Type)
			{
				case AvigilonDotNet.EntityType.Ptz:
					eif = new EntityInfoForm(m_entity as AvigilonDotNet.IEntityPtz);
					break;

				default:
					eif = new EntityInfoForm(m_entity);
					break;

			}
            eif.Show();
        }

        /// <summary>
        /// Event handler for the trigger menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTriggerItemClick_(System.Object sender, System.EventArgs e)
        {
            AvigilonDotNet.IEntityDigitalOutput digitalOutputEntity = (AvigilonDotNet.IEntityDigitalOutput)m_entity;
            digitalOutputEntity.Trigger();
        }

		private void OnLockUnlockItemClick_(System.Object sender, System.EventArgs e)
		{
			AvigilonDotNet.IEntityPtz entityPtz = (AvigilonDotNet.IEntityPtz)m_entity;
			if (entityPtz.HasPtzControlsLock)
			{
				entityPtz.ReleasePtzControlsLock();
			}
			else
			{
				entityPtz.AcquirePtzControlsLock();
			}
		}

        private AvigilonDotNet.IEntity m_entity;
		private System.Windows.Forms.ContextMenuStrip m_entityContextMenu;
		private System.Windows.Forms.ToolStripMenuItem m_displayItem;
		private System.Windows.Forms.ToolStripMenuItem m_exportItem;
		private System.Windows.Forms.ToolStripMenuItem m_imageQueryItem;
		private System.Windows.Forms.ToolStripMenuItem m_startManualRecItem;
		private System.Windows.Forms.ToolStripMenuItem m_endManualRecItem;
        private System.Windows.Forms.ToolStripMenuItem m_infoItem;
        private System.Windows.Forms.ToolStripMenuItem m_triggerItem;
		private System.Windows.Forms.ToolStripMenuItem m_lockUnlockItem;
    };
} /* namespace DemoApp */