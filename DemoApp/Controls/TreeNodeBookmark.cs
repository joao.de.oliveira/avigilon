﻿/*--------------------------------------------------------------------------------------
 * Copyright 2015 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

namespace DemoApp
{
	/// <summary>
	/// Defines a tree node that holds bookmark information
	/// </summary>
	public class TreeNodeBookmark
		: System.Windows.Forms.TreeNode
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="bookmark">bookmark to display</param>
		public TreeNodeBookmark(AvigilonDotNet.IBookmark bookmark)
		{
			// Set members
			m_bookmark = bookmark;
			m_bookmarkDeviceIds = bookmark.DeviceIds;
			m_bookmark.InfoChanged += HandleBookmarkInfoChange_;

			// Context menu
			m_bookmarkContextMenu = new System.Windows.Forms.ContextMenuStrip();
			m_bookmarkContextMenu.Opening += new System.ComponentModel.CancelEventHandler(UpdatePopupMenu_);
			this.ContextMenuStrip = m_bookmarkContextMenu;

			m_infoItem = new System.Windows.Forms.ToolStripMenuItem("Info");
			m_infoItem.Click += new System.EventHandler(OnInfoItemClick_);
			m_bookmarkContextMenu.Items.Add(m_infoItem);

			UpdateInfo();
		}

		/// <summary>
		/// Get bookmark ID.
		/// </summary>
		public System.String BookmarkId
		{
			get
			{
				if (m_bookmark != null)
					return m_bookmark.BookmarkId;
				else
					return "";
			}
		}

		/// <summary>
		/// Extracts the name applied to the node
		/// </summary>
		/// <param name="bookmark"></param>
		/// <returns></returns>
		public static System.String ExtractNodeName(AvigilonDotNet.IBookmark bookmark)
		{
			return "Bookmark" + bookmark.BookmarkId;
		}

		/// <summary>
		/// bookmark info changed event handler
		/// </summary>
		private void HandleBookmarkInfoChange_()
		{
			UpdateInfo();
			if (TreeView != null)
				TreeView.Refresh();
		}

		/// <summary>
		/// Updates the information displayed based on the state of the entity.
		/// </summary>
		private void UpdateInfo()
		{
			Name = ExtractNodeName(m_bookmark);

			// Set text
			if (!m_bookmark.Valid)
			{
				Text = "Invalid";
			}
			else
			{
				Text = "Bookmark: " + m_bookmark.BookmarkName;
				if (!System.String.IsNullOrEmpty(m_bookmark.BookmarkId))
				{
					Text += "{" + m_bookmark.BookmarkId + "} ";
				}
				else
				{
					Text += "{-} ";
				}
			}
		}

		/// <summary>
		/// Update items in the popup menu
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void UpdatePopupMenu_(
			object sender,
			System.ComponentModel.CancelEventArgs e)
		{ }

		/// <summary>
		/// Event handler for the info menu item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnInfoItemClick_(System.Object sender, System.EventArgs e)
		{
			BookmarkInfoForm bif = new BookmarkInfoForm(m_bookmark);
			bif.Show();
		}

		private AvigilonDotNet.IBookmark m_bookmark;
		private System.Collections.Generic.List<string> m_bookmarkDeviceIds = new System.Collections.Generic.List<string>(); 

		private System.Windows.Forms.ContextMenuStrip m_bookmarkContextMenu;
		private System.Windows.Forms.ToolStripMenuItem m_infoItem;
	};
} /* namespace DemoApp */