/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/
using System.Diagnostics;

namespace DemoApp 
{	
	/// <summary>
	/// Tree view customized to display the known nvrs and devices
	/// </summary>
    public class TreeViewAvigilon
        : System.Windows.Forms.TreeView
    {
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="controlCenter">Control center interface object</param>
        public TreeViewAvigilon()
        {
			this.ShowNodeToolTips = true;
			this.Sort();
        }

		/// <summary>
		/// Raised with the user selects to display a device
		/// </summary>
		public event EntityHandler DisplayEntity;

		/// <summary>
		/// Raised with the user selects to export data from a device
		/// </summary>
		public event EntityHandler ExportEntity;

		/// <summary>
		/// Raised with the user selects to perform an image query on a device
		/// </summary>
		public event EntityHandler ImageQueryEntity;

		/// <summary>
		/// Raised when the user selects to perform a backup on a nvr
		/// </summary>
		public event NvrHandler BackupNvr;

		public AvigilonDotNet.IAvigilonControlCenter ControlCenter
		{
			get
			{
				return m_controlCenter;
			}

			set
			{
				Nodes.Clear();
				if (m_controlCenter != null)
				{
					m_controlCenter.NvrListAdd -= NvrListAddHandler_;
					m_controlCenter.FileArchiveListAdd -= ArchiveListAddHandler_;
				}

				m_controlCenter = value;
				if (m_controlCenter != null)
				{
					m_controlCenter.NvrListAdd += NvrListAddHandler_;
					m_controlCenter.FileArchiveListAdd += ArchiveListAddHandler_;

					AddAllNvrs_();
					AddAllArchives_();
				}
			}
		}

		/// <summary>
		/// Nvr list change event handler
		/// </summary>
		private void NvrListAddHandler_(System.String nvrUuid)
        {
			AvigilonDotNet.INvr nvr = m_controlCenter.GetNvr(nvrUuid);
			if (nvr != null &&
				Nodes.Find(TreeNodeNvr.ExtractNodeName(nvr), false).Length == 0)
			{
				// Does not already exist in the tree
				TreeNodeNvr newNvrNode = new TreeNodeNvr(nvr);
				newNvrNode.DisplayEntity += OnDisplayEntity_;
				newNvrNode.ExportEntity += OnExportEntity_;
				newNvrNode.ImageQueryEntity += OnImageQueryEntity_;
				newNvrNode.BackupNvr += OnBackupNvr_;
				newNvrNode.RemoveNvr += OnRemoveNvr_;

				Nodes.Add(newNvrNode);
			}
        }

		/// <summary>
		/// Archive list change event handler
		/// </summary>
		private void ArchiveListAddHandler_(
			System.String nvrUuid,
			System.String dataStoreUuid)
		{
			AvigilonDotNet.IFileArchive archive = m_controlCenter.GetFileArchive(dataStoreUuid);
			if (archive != null &&
				Nodes.Find(TreeNodeArchive.ExtractNodeName(archive), false).Length == 0)
			{
				// Does not already exist in the tree
				TreeNodeArchive newArchiveNode = new TreeNodeArchive(archive);
				newArchiveNode.DisplayEntity += OnDisplayEntity_;
				newArchiveNode.ExportEntity += OnExportEntity_;
				newArchiveNode.ImageQueryEntity += OnImageQueryEntity_;

				Nodes.Add(newArchiveNode);
			}
		}

		/// <summary>
		/// Adds subnodes to handle newly discovered nvrs
		/// </summary>
        private void AddAllNvrs_()
        {
            // Go through the list of Nvrs add any new Nvrs
            System.Collections.Generic.List<AvigilonDotNet.INvr> nvrs = m_controlCenter.Nvrs;
            foreach (AvigilonDotNet.INvr nvr in nvrs)
            {
				TreeNodeNvr newNvrNode = new TreeNodeNvr(nvr);
				newNvrNode.DisplayEntity += OnDisplayEntity_;
				newNvrNode.ExportEntity += OnExportEntity_;
				newNvrNode.ImageQueryEntity += OnImageQueryEntity_;
				newNvrNode.BackupNvr += OnBackupNvr_;
				newNvrNode.RemoveNvr += OnRemoveNvr_;

				Nodes.Add(newNvrNode);
            }
        }

		/// <summary>
		/// Adds subnodes to handle newly discovered archives
		/// </summary>
		private void AddAllArchives_()
		{
			// Go through the list of Nvrs add any new Nvrs
			System.Collections.Generic.List<AvigilonDotNet.IFileArchive> archives = m_controlCenter.FileArchives;
			foreach (AvigilonDotNet.IFileArchive archive in archives)
			{
				TreeNodeArchive newArchiveNode = new TreeNodeArchive(archive);
				newArchiveNode.DisplayEntity += OnDisplayEntity_;
				newArchiveNode.ExportEntity += OnExportEntity_;
				newArchiveNode.ImageQueryEntity += OnImageQueryEntity_;

				Nodes.Add(newArchiveNode);
			}
		}

		/// <summary>
		/// Display device menu selection event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="device"></param>
		private void OnDisplayEntity_(
			System.Object sender,
			AvigilonDotNet.IEntity entity)
		{
			DisplayEntity(sender, entity);
		}

		/// <summary>
		/// Export device menu selection event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="device"></param>
		private void OnExportEntity_(
			System.Object sender,
			AvigilonDotNet.IEntity entity)
		{
			ExportEntity(sender, entity);
		}

		/// <summary>
		/// Image Query device menu selection event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="device"></param>
		private void OnImageQueryEntity_(
			System.Object sender,
			AvigilonDotNet.IEntity entity)
		{
			ImageQueryEntity(sender, entity);
		}

		/// <summary>
		/// Backup Nvr menu selection event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="device"></param>
		private void OnBackupNvr_(
			System.Object sender,
			AvigilonDotNet.INvr nvr)
		{
			BackupNvr(sender, nvr);
		}

		/// <summary>
		/// Remove Nvr menu selection event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="device"></param>
		private void OnRemoveNvr_(
			System.Object sender,
			AvigilonDotNet.INvr nvr)
		{
			m_controlCenter.RemoveNvr(nvr.NvrUuid);
		}

        private AvigilonDotNet.IAvigilonControlCenter m_controlCenter;
    };
} /* namespace DemoApp */