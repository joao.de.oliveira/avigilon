/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DemoApp
{
	/// <summary>
	/// Defines a tree node that holds textSource information
	/// </summary>
	public class TreeNodeTextSource
		: System.Windows.Forms.TreeNode
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="textSource">textSource to display</param>
		public TreeNodeTextSource(AvigilonDotNet.ITextSource textSource)
		{
			m_textSource = textSource;
            m_textSource.InfoChanged += TextSourceInfoChangeHandler_;
            
            // Context menu
			m_textSourceContextMenu = new System.Windows.Forms.ContextMenuStrip();
            m_textSourceContextMenu.Opening += new System.ComponentModel.CancelEventHandler(UpdatePopupMenu_);
			this.ContextMenuStrip = m_textSourceContextMenu;

			m_copyUuidItem = new System.Windows.Forms.ToolStripMenuItem("Copy Uuid");
			m_copyUuidItem.Click += new System.EventHandler(OnCopyUuidItemClick_);
			m_textSourceContextMenu.Items.Add(m_copyUuidItem);

            m_infoItem = new System.Windows.Forms.ToolStripMenuItem("Info");
            m_infoItem.Click += new System.EventHandler(OnInfoItemClick_);
            m_textSourceContextMenu.Items.Add(m_infoItem);

			UpdateInfo_();
		}

		public System.String TextSourceId
		{
			get
			{
                if (m_textSource != null)
                    return m_textSource.TextSourceId;
                else
                    return "";
			}
		}

		/// <summary>
		/// The name that is applied to this node
		/// </summary>
		/// <param name="textSource"></param>
		/// <returns></returns>
		public static System.String ExtractNodeName(AvigilonDotNet.ITextSource textSource)
		{
			return "TextSource" + textSource.TextSourceId;
		}

		/// <summary>
		/// textSource info change event handler
		/// </summary>
		private void TextSourceInfoChangeHandler_()
		{
			UpdateInfo_();
            if (TreeView != null)
                TreeView.Refresh();
		}

		/// <summary>
		/// Updates the information displayed based on the state of the textSource.
		/// </summary>
		private void UpdateInfo_()
		{
			Name = ExtractNodeName(m_textSource);

			if (m_textSource.Valid)
			{
				Text = "TextSource: " + m_textSource.Name;
				Text += " state:";
				Text += m_textSource.State.ToString();

				ToolTipText = "TextSourceId: " + m_textSource.TextSourceId;
			}
			else
			{
				Text = "TextSource invalid";
			}
		}

		/// <summary>
		/// Event handler for the copy uuid menu item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCopyUuidItemClick_(System.Object sender, System.EventArgs e)
		{
			System.Windows.Forms.Clipboard.SetText(m_textSource.TextSourceId.ToString());
		}

		/// <summary>
		/// Update items in the popup menu
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void UpdatePopupMenu_(
			object sender,
			System.ComponentModel.CancelEventArgs e)
		{}

        /// <summary>
        /// Event handler for the info menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnInfoItemClick_(System.Object sender, System.EventArgs e)
        {
            TextSourceInfoForm itemInfo = new TextSourceInfoForm(m_textSource);
            itemInfo.Show();
        }

		private AvigilonDotNet.ITextSource m_textSource;
		private System.Windows.Forms.ContextMenuStrip m_textSourceContextMenu;
        private System.Windows.Forms.ToolStripMenuItem m_copyUuidItem;
        private System.Windows.Forms.ToolStripMenuItem m_infoItem;
	};
} /* namespace DemoApp */