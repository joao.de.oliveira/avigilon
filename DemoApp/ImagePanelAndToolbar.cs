/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DemoApp
{
    //-------------------------------------------
    class ImagePanelToolbarButton 
        : ToolStripButton
    {
        public ImagePanelToolbarButton(System.String name,
            AvigilonDotNet.ImagePanelMouseAction operation)
            : base(name)
        {
            m_operation = operation;
        }

        public AvigilonDotNet.ImagePanelMouseAction TheOperation
        {
            get
            {
                return m_operation;
            }
        }

        private AvigilonDotNet.ImagePanelMouseAction m_operation;
    }

    //-------------------------------------------
	public class ImagePanelToolbar 
        : System.Windows.Forms.ToolStrip
	{
		public ImagePanelToolbar()
		{
			RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
			GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			ImageScalingSize = new System.Drawing.Size(24, 24);

			btnNone = new ImagePanelToolbarButton("None", AvigilonDotNet.ImagePanelMouseAction.None);
			btnPan = new ImagePanelToolbarButton("Pan", AvigilonDotNet.ImagePanelMouseAction.Pan);
			btnZoomIn = new ImagePanelToolbarButton("ZoomIn", AvigilonDotNet.ImagePanelMouseAction.ZoomIn);
			btnZoomOut = new ImagePanelToolbarButton("ZoomOut", AvigilonDotNet.ImagePanelMouseAction.ZoomOut);
            btnAudio = new ToolStripButton("");
            btnAudioOut = new ToolStripButton("");
            btnClose = new ToolStripButton("Close");
            
			Items.Add(btnNone);
			Items.Add(btnPan);
			Items.Add(btnZoomIn);
			Items.Add(btnZoomOut);
            Items.Add(btnAudio);
            Items.Add(btnAudioOut);
            Items.Add(btnClose);

			m_currentOperation = AvigilonDotNet.ImagePanelMouseAction.None;
			SetImagePanelMouseAction_();
            UpdateAudioButton_();
            UpdateAudioOutButton_();

			this.ItemClicked += ToolbarClicked_;
		}

		public event AvigilonDotNet.VoidHandler ImagePanelMouseActionChanged;
		public event AvigilonDotNet.VoidHandler ImagePanelClose;

		public AvigilonDotNet.ImagePanelMouseAction CurrentOperation
		{
			get
			{
				return m_currentOperation;
			}
        }

        public AvigilonDotNet.IStream Stream
        {
            set
            {
                m_stream = value;

                if (m_stream != null)
                {
                    m_stream.AudioAvailable += StreamAudioAvailable_;
                    m_stream.AudioOutAvailable += StreamAudioOutAvailable_;
                }

                UpdateAudioButton_();
                UpdateAudioOutButton_();
            }
        }

        void StreamAudioAvailable_()
        {
            UpdateAudioButton_();
        }

        void StreamAudioOutAvailable_()
        {
            UpdateAudioOutButton_();
        }

        private void UpdateAudioButton_()
        {
            if (m_stream != null &&
                m_stream.AudioPresent)
            {
                if (m_stream.EnableAudio)
                {
                    // Currently enabled
                    btnAudio.Text = "Audio Off";
                }
                else
                {
                    // Currently disabled
                    btnAudio.Text = "Audio On";
                }
                btnAudio.Enabled = true;
            }
            else
            {
                btnAudio.Text = "No Audio";
                btnAudio.Enabled = false;
            }

            Invalidate();
        }

        private void UpdateAudioOutButton_()
        {
            if (m_stream != null &&
                m_stream.AudioOutPresent)
            {
                if (m_stream.EnableAudioOut)
                {
                    // Currently enabled
                    btnAudioOut.Text = "Mic Off";
                }
                else
                {
                    // Currently disabled
                    btnAudioOut.Text = "Mic On";
                }
                btnAudioOut.Enabled = true;
            }
            else
            {
                btnAudioOut.Text = "No Mic";
                btnAudioOut.Enabled = false;
            }

            Invalidate();
        }

        private void ToolbarClicked_(
			Object sender,
			ToolStripItemClickedEventArgs e)
		{
            if(e.ClickedItem == btnClose)
            {
                if(ImagePanelClose != null)
					ImagePanelClose();
            }
            else if (e.ClickedItem == btnAudio)
            {
                bool audioCurrentlyEnabled = m_stream.EnableAudio;

                if (audioCurrentlyEnabled)
                {
                    m_stream.EnableAudio = false;
                }
                else
                {
                    m_stream.EnableAudio = true;
                }
             
                UpdateAudioButton_();
            }
            else if (e.ClickedItem == btnAudioOut)
            {
                bool audioOutCurrentlyEnabled = m_stream.EnableAudioOut;

                if (audioOutCurrentlyEnabled)
                {
                    m_stream.EnableAudioOut = false;
                }
                else
                {
                    m_stream.EnableAudioOut = true;
                }

                UpdateAudioOutButton_();
            }
            else
            {
                m_currentOperation = ((ImagePanelToolbarButton)e.ClickedItem).TheOperation;
                if (ImagePanelMouseActionChanged != null)
                    ImagePanelMouseActionChanged();
            }

            SetImagePanelMouseAction_();			
		}

		private void SetImagePanelMouseAction_()
		{
			switch (m_currentOperation)
			{
				case AvigilonDotNet.ImagePanelMouseAction.None:
					btnNone.Checked = true;
					btnPan.Checked = false;
					btnZoomIn.Checked = false;
					btnZoomOut.Checked = false;
					break;

				case AvigilonDotNet.ImagePanelMouseAction.Pan:
					btnNone.Checked = false;
					btnPan.Checked = true;
					btnZoomIn.Checked = false;
					btnZoomOut.Checked = false;
					break;

				case AvigilonDotNet.ImagePanelMouseAction.ZoomIn:
					btnNone.Checked = false;
					btnPan.Checked = false;
					btnZoomIn.Checked = true;
					btnZoomOut.Checked = false;
					break;

				case AvigilonDotNet.ImagePanelMouseAction.ZoomOut:
					btnNone.Checked = false;
					btnPan.Checked = false;
					btnZoomIn.Checked = false;
					btnZoomOut.Checked = true;
					break;

				default:
					break;
			}
		}

		private AvigilonDotNet.ImagePanelMouseAction m_currentOperation;

		ImagePanelToolbarButton btnNone;
		ImagePanelToolbarButton btnPan;
		ImagePanelToolbarButton btnZoomIn;
		ImagePanelToolbarButton btnZoomOut;
        ToolStripButton btnAudio;
        ToolStripButton btnAudioOut;
        ToolStripButton btnClose;

        AvigilonDotNet.IStream m_stream;
	}

    //-------------------------------------------
	public class ImagePanelAndToolbar
		: System.Windows.Forms.Control
	{
		const System.Int32 mouseWheelDeltaScale = 3000;

		public event System.EventHandler ImagePanelClick;
		public event AvigilonDotNet.VoidHandler ImagePanelClose;

		public ImagePanelAndToolbar()
		{
			m_toolbar = new ImagePanelToolbar();
			m_toolbar.ImagePanelMouseActionChanged += ImagePanelMouseActionChange_;
			m_toolbar.ImagePanelClose += ImagePanelClose_;
			Controls.Add(m_toolbar);

			m_imagePanel = new AvigilonDotNet.ImagePanel();
			m_imagePanel.Overlays = AvigilonDotNet.Overlay.None;
			m_imagePanel.Location = new System.Drawing.Point(0, m_toolbar.Height);
			m_imagePanel.MouseDown += OnImagePanelClick_;
			Controls.Add(m_imagePanel);

			m_imagePanel.DefaultMouseAction = m_toolbar.CurrentOperation;
		
			this.Layout += OnLayout_;
		}

		void OnImagePanelClick_(object sender, EventArgs e)
		{
			if (ImagePanelClick != null)
				ImagePanelClick(sender, e);
		}

		public AvigilonDotNet.IStreamWindow Stream
		{
			get
			{
				return m_imagePanel.StreamWindow;
			}

			set
			{
				m_imagePanel.StreamWindow = value;
                m_toolbar.Stream = value;
			}
		}

		public AvigilonDotNet.Overlay Overlays
		{
			get
			{
				return m_imagePanel.Overlays;
			}

			set
			{
				m_imagePanel.Overlays = value;
			}
		}

		private void OnLayout_(
			System.Object sender,
			System.Windows.Forms.LayoutEventArgs e)
		{
			System.Drawing.Rectangle clientRect = this.ClientRectangle;
			m_imagePanel.Size = new System.Drawing.Size(clientRect.Width, clientRect.Height - m_toolbar.Height);
		}

		void ImagePanelMouseActionChange_()
		{
			m_imagePanel.DefaultMouseAction = m_toolbar.CurrentOperation;
		}

		void ImagePanelClose_()
		{
            if (ImagePanelClose != null)
            {
                ImagePanelClose();
            }
		}
		
		private AvigilonDotNet.ImagePanel m_imagePanel;
		private ImagePanelToolbar m_toolbar;
	}
}
