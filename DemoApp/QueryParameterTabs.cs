﻿/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AvigilonDotNet;

namespace DemoApp
{
	/// <summary>
	/// Defines the query all greater than parameters tab page
	/// </summary>
	public class QueryEventAllTab :
		System.Windows.Forms.TabPage
	{
		public QueryEventAllTab()
		{
			this.Text = GetText();

			///////////////////////////
			System.Windows.Forms.Label incLinkedLabel = new System.Windows.Forms.Label();
			incLinkedLabel.Text = "Incl linked events: ";
			incLinkedLabel.AutoSize = true;
			incLinkedLabel.Location = new System.Drawing.Point(10, 50);
			Controls.Add(incLinkedLabel);

			m_inclLinkedEvents = new System.Windows.Forms.CheckBox();
			m_inclLinkedEvents.Location = new System.Drawing.Point(150, 50);
			m_inclLinkedEvents.Checked = false;
			Controls.Add(m_inclLinkedEvents);
		}

		public static System.String GetText()
		{
			return "All";
		}
		
		public bool InclLinkedEvents
		{
			get { return m_inclLinkedEvents.Checked; }
		}

		private System.Windows.Forms.CheckBox m_inclLinkedEvents;
	}

	/// <summary>
	/// Defines the query id range parameters tab page
	/// </summary>
	public class QueryEventTimeTab :
		System.Windows.Forms.TabPage
	{
		public QueryEventTimeTab()
		{
			this.Text = GetText();

			System.Windows.Forms.Label startTimeLabel = new System.Windows.Forms.Label();
			startTimeLabel.Text = "Start time: ";
			startTimeLabel.AutoSize = true;
			startTimeLabel.Location = new System.Drawing.Point(10, 10);
			Controls.Add(startTimeLabel);

			m_startDtPicker = new System.Windows.Forms.DateTimePicker();
			m_startDtPicker.Format = DateTimePickerFormat.Custom;
			m_startDtPicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_startDtPicker.Location = new System.Drawing.Point(150, 10);
			Controls.Add(m_startDtPicker);

			///////////////////////////
			System.Windows.Forms.Label endTimeLabel = new System.Windows.Forms.Label();
			endTimeLabel.Text = "End time: ";
			endTimeLabel.AutoSize = true;
			endTimeLabel.Location = new System.Drawing.Point(10, 50);
			Controls.Add(endTimeLabel);

			m_endDtPicker = new System.Windows.Forms.DateTimePicker();
			m_endDtPicker.Format = DateTimePickerFormat.Custom;
			m_endDtPicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_endDtPicker.Location = new System.Drawing.Point(150, 50);
			Controls.Add(m_endDtPicker);

			///////////////////////////
			System.Windows.Forms.Label incLinkedLabel = new System.Windows.Forms.Label();
			incLinkedLabel.Text = "Incl linked events: ";
			incLinkedLabel.AutoSize = true;
			incLinkedLabel.Location = new System.Drawing.Point(10, 90);
			Controls.Add(incLinkedLabel);

			m_inclLinkedEvents = new System.Windows.Forms.CheckBox();
			m_inclLinkedEvents.Location = new System.Drawing.Point(150, 90);
			m_inclLinkedEvents.Checked = false;
			Controls.Add(m_inclLinkedEvents);
		}

		public static System.String GetText()
		{
			return "Time";
		}

		public System.DateTime StartDateTime
		{
			get { return m_startDtPicker.Value; }
		}

		public System.DateTime EndDateTime
		{
			get { return m_endDtPicker.Value; }
		}

		public bool InclLinkedEvents
		{
			get { return m_inclLinkedEvents.Checked; }
		}


		private System.Windows.Forms.DateTimePicker m_startDtPicker;
		private System.Windows.Forms.DateTimePicker m_endDtPicker;
		private System.Windows.Forms.CheckBox m_inclLinkedEvents;
	}

	/// <summary>
	/// Defines the query alarm record event parameters tab page
	/// </summary>
	public class QueryEventTypesTab :
		System.Windows.Forms.TabPage
	{
		public QueryEventTypesTab()
		{
			this.Text = GetText();

			System.Windows.Forms.Label startTimeLabel = new System.Windows.Forms.Label();
			startTimeLabel.Text = "Start time: ";
			startTimeLabel.AutoSize = true;
			startTimeLabel.Location = new System.Drawing.Point(10, 10);
			Controls.Add(startTimeLabel);

			m_startDtPicker = new System.Windows.Forms.DateTimePicker();
			m_startDtPicker.Format = DateTimePickerFormat.Custom;
			m_startDtPicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_startDtPicker.Location = new System.Drawing.Point(150, 10);
			Controls.Add(m_startDtPicker);

			///////////////////////////
			System.Windows.Forms.Label endTimeLabel = new System.Windows.Forms.Label();
			endTimeLabel.Text = "End time: ";
			endTimeLabel.AutoSize = true;
			endTimeLabel.Location = new System.Drawing.Point(10, 50);
			Controls.Add(endTimeLabel);

			m_endDtPicker = new System.Windows.Forms.DateTimePicker();
			m_endDtPicker.Format = DateTimePickerFormat.Custom;
			m_endDtPicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_endDtPicker.Location = new System.Drawing.Point(150, 50);
			Controls.Add(m_endDtPicker);

			///////////////////////////
			System.Windows.Forms.Label incLinkedLabel = new System.Windows.Forms.Label();
			incLinkedLabel.Text = "Incl linked events: ";
			incLinkedLabel.AutoSize = true;
			incLinkedLabel.Location = new System.Drawing.Point(10, 90);
			Controls.Add(incLinkedLabel);

			m_inclLinkedEvents = new System.Windows.Forms.CheckBox();
			m_inclLinkedEvents.Location = new System.Drawing.Point(150, 90);
			m_inclLinkedEvents.Checked = false;
			Controls.Add(m_inclLinkedEvents);

			///////////////////////////
			System.Windows.Forms.Label eventTypesLabel = new System.Windows.Forms.Label();
			eventTypesLabel.Text = "Event Types: ";
			eventTypesLabel.AutoSize = true;
			eventTypesLabel.Location = new System.Drawing.Point(10, 130);
			Controls.Add(eventTypesLabel);

			m_eventTypes = new EnumListSelector<AvigilonDotNet.EventType>(new List<AvigilonDotNet.EventType>());
			m_eventTypes.Location = new System.Drawing.Point(150, 130);
			m_eventTypes.Size = new System.Drawing.Size(200, 70);
			Controls.Add(m_eventTypes);
		}

		public static System.String GetText()
		{
			return "Event Types";
		}
		
		public System.DateTime StartDateTime
		{
			get { return m_startDtPicker.Value; }
		}

		public System.DateTime EndDateTime
		{
			get { return m_endDtPicker.Value; }
		}

		public bool InclLinkedEvents
		{
			get { return m_inclLinkedEvents.Checked; }
		}

		public System.Collections.Generic.List<AvigilonDotNet.EventType> EventTypes
		{
			get { return m_eventTypes.Selection; }
		}

		private System.Windows.Forms.DateTimePicker m_startDtPicker;
		private System.Windows.Forms.DateTimePicker m_endDtPicker;
		private System.Windows.Forms.CheckBox m_inclLinkedEvents;
		private EnumListSelector<AvigilonDotNet.EventType> m_eventTypes;
	}

	/// <summary>
	/// Defines the license plate event parameters tab page
	/// </summary>
	public class QueryEventCategoriesTab :
		System.Windows.Forms.TabPage
	{
		public QueryEventCategoriesTab()
		{
			this.Text = GetText();

			System.Windows.Forms.Label startTimeLabel = new System.Windows.Forms.Label();
			startTimeLabel.Text = "Start time: ";
			startTimeLabel.AutoSize = true;
			startTimeLabel.Location = new System.Drawing.Point(10, 10);
			Controls.Add(startTimeLabel);

			m_startDtPicker = new System.Windows.Forms.DateTimePicker();
			m_startDtPicker.Format = DateTimePickerFormat.Custom;
			m_startDtPicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_startDtPicker.Location = new System.Drawing.Point(150, 10);
			Controls.Add(m_startDtPicker);

			///////////////////////////
			System.Windows.Forms.Label endTimeLabel = new System.Windows.Forms.Label();
			endTimeLabel.Text = "End time: ";
			endTimeLabel.AutoSize = true;
			endTimeLabel.Location = new System.Drawing.Point(10, 50);
			Controls.Add(endTimeLabel);

			m_endDtPicker = new System.Windows.Forms.DateTimePicker();
			m_endDtPicker.Format = DateTimePickerFormat.Custom;
			m_endDtPicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_endDtPicker.Location = new System.Drawing.Point(150, 50);
			Controls.Add(m_endDtPicker);

			///////////////////////////
			System.Windows.Forms.Label incLinkedLabel = new System.Windows.Forms.Label();
			incLinkedLabel.Text = "Incl linked events: ";
			incLinkedLabel.AutoSize = true;
			incLinkedLabel.Location = new System.Drawing.Point(10, 90);
			Controls.Add(incLinkedLabel);

			m_inclLinkedEvents = new System.Windows.Forms.CheckBox();
			m_inclLinkedEvents.Location = new System.Drawing.Point(150, 90);
			m_inclLinkedEvents.Checked = false;
			Controls.Add(m_inclLinkedEvents);

			///////////////////////////
			System.Windows.Forms.Label eventTypesLabel = new System.Windows.Forms.Label();
			eventTypesLabel.Text = "Event Categories: ";
			eventTypesLabel.AutoSize = true;
			eventTypesLabel.Location = new System.Drawing.Point(10, 130);
			Controls.Add(eventTypesLabel);

			m_eventCategoryTypes = new EnumListSelector<AvigilonDotNet.EventCategory>(new List<AvigilonDotNet.EventCategory>());
			m_eventCategoryTypes.Location = new System.Drawing.Point(150, 130);
			m_eventCategoryTypes.Size = new System.Drawing.Size(200, 70);
			Controls.Add(m_eventCategoryTypes);

			///////////////////////////
			System.Windows.Forms.Label filteredEventTypesLabel = new System.Windows.Forms.Label();
			filteredEventTypesLabel.Text = "Filtered Event Types: ";
			filteredEventTypesLabel.AutoSize = true;
			filteredEventTypesLabel.Location = new System.Drawing.Point(400, 10);
			Controls.Add(filteredEventTypesLabel);

			m_filteredEventTypes = new EnumListSelector<AvigilonDotNet.EventType>(new List<AvigilonDotNet.EventType>());
			m_filteredEventTypes.Location = new System.Drawing.Point(540, 10);
			m_filteredEventTypes.Size = new System.Drawing.Size(200, 70);
			Controls.Add(m_filteredEventTypes);
		}

		public static System.String GetText()
		{
			return "Event Category";
		}
		
		public System.DateTime StartDateTime
		{
			get { return m_startDtPicker.Value; }
		}

		public System.DateTime EndDateTime
		{
			get { return m_endDtPicker.Value; }
		}

		public bool InclLinkedEvents
		{
			get { return m_inclLinkedEvents.Checked; }
		}

		public System.Collections.Generic.List<AvigilonDotNet.EventCategory> EventCategories
		{
			get { return m_eventCategoryTypes.Selection; }
		}

		public System.Collections.Generic.List<AvigilonDotNet.EventType> FilteredEventTypes
		{
			get { return m_filteredEventTypes.Selection; }
		}

		private System.Windows.Forms.DateTimePicker m_startDtPicker;
		private System.Windows.Forms.DateTimePicker m_endDtPicker;
		private System.Windows.Forms.CheckBox m_inclLinkedEvents;
		private EnumListSelector<AvigilonDotNet.EventCategory> m_eventCategoryTypes;
		private EnumListSelector<AvigilonDotNet.EventType> m_filteredEventTypes;
	}

	/// <summary>
	/// Defines the motion event parameters tab page
	/// </summary>
	public class QueryEventDeviceIdTab :
		System.Windows.Forms.TabPage
	{
		public QueryEventDeviceIdTab()
		{
			this.Text = GetText();

			System.Windows.Forms.Label startTimeLabel = new System.Windows.Forms.Label();
			startTimeLabel.Text = "Start time: ";
			startTimeLabel.AutoSize = true;
			startTimeLabel.Location = new System.Drawing.Point(10, 10);
			Controls.Add(startTimeLabel);

			m_startDtPicker = new System.Windows.Forms.DateTimePicker();
			m_startDtPicker.Format = DateTimePickerFormat.Custom;
			m_startDtPicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_startDtPicker.Location = new System.Drawing.Point(200, 10);
			Controls.Add(m_startDtPicker);

			///////////////////////////
			System.Windows.Forms.Label endTimeLabel = new System.Windows.Forms.Label();
			endTimeLabel.Text = "End time: ";
			endTimeLabel.AutoSize = true;
			endTimeLabel.Location = new System.Drawing.Point(10, 50);
			Controls.Add(endTimeLabel);

			m_endDtPicker = new System.Windows.Forms.DateTimePicker();
			m_endDtPicker.Format = DateTimePickerFormat.Custom;
			m_endDtPicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_endDtPicker.Location = new System.Drawing.Point(200, 50);
			Controls.Add(m_endDtPicker);

			///////////////////////////
			System.Windows.Forms.Label incLinkedLabel = new System.Windows.Forms.Label();
			incLinkedLabel.Text = "Incl linked events: ";
			incLinkedLabel.AutoSize = true;
			incLinkedLabel.Location = new System.Drawing.Point(10, 90);
			Controls.Add(incLinkedLabel);

			m_inclLinkedEvents = new System.Windows.Forms.CheckBox();
			m_inclLinkedEvents.Location = new System.Drawing.Point(200, 90);
			m_inclLinkedEvents.Checked = false;
			Controls.Add(m_inclLinkedEvents);

			///////////////////////////
			System.Windows.Forms.Label deviceIdLabel = new System.Windows.Forms.Label();
			deviceIdLabel.Text = "DeviceIds (space seperated): ";
			deviceIdLabel.AutoSize = true;
			deviceIdLabel.Location = new System.Drawing.Point(10, 130);
			Controls.Add(deviceIdLabel);

			m_deviceIdEdit = new System.Windows.Forms.TextBox();
			m_deviceIdEdit.Location = new System.Drawing.Point(200, 130);
			m_deviceIdEdit.Width = 400;
			Controls.Add(m_deviceIdEdit);
		}

		public static System.String GetText()
		{
			return "Event Device Id";
		}
		
		public System.DateTime StartDateTime
		{
			get { return m_startDtPicker.Value; }
		}

		public System.DateTime EndDateTime
		{
			get { return m_endDtPicker.Value; }
		}

		public bool InclLinkedEvents
		{
			get { return m_inclLinkedEvents.Checked; }
		}

		public System.Collections.Generic.List<System.String> DeviceIds
		{
			get
			{
				System.Collections.Generic.List<System.String> retVal = new System.Collections.Generic.List<System.String>();
				System.String[] tokens = m_deviceIdEdit.Text.Split(new Char[] { ' ' });
				foreach (System.String theString in tokens)
				{
					if (theString != "")
						retVal.Add(theString);
				}

				return retVal;
			}
		}

		private System.Windows.Forms.DateTimePicker m_startDtPicker;
		private System.Windows.Forms.DateTimePicker m_endDtPicker;
		private System.Windows.Forms.CheckBox m_inclLinkedEvents;
		private System.Windows.Forms.TextBox m_deviceIdEdit;
	}

	/// <summary>
	/// Defines the query active parameters tab page
	/// </summary>
	public class QueryEventActiveTab :
		System.Windows.Forms.TabPage
	{
		public QueryEventActiveTab()
		{
			this.Text = GetText();
		}


		public static System.String GetText()
		{
			return "Active";
		}
	}

	/// <summary>
	/// Defines the motion event parameters tab page
	/// </summary>
	public class QueryTextTransactionsTab :
		System.Windows.Forms.TabPage
	{
		public QueryTextTransactionsTab()
		{
			this.Text = GetText();

			System.Windows.Forms.Label startTimeLabel = new System.Windows.Forms.Label();
			startTimeLabel.Text = "Start time: ";
			startTimeLabel.AutoSize = true;
			startTimeLabel.Location = new System.Drawing.Point(10, 10);
			Controls.Add(startTimeLabel);

			m_startDtPicker = new System.Windows.Forms.DateTimePicker();
			m_startDtPicker.Format = DateTimePickerFormat.Custom;
			m_startDtPicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_startDtPicker.Location = new System.Drawing.Point(200, 10);
			Controls.Add(m_startDtPicker);

			///////////////////////////
			System.Windows.Forms.Label endTimeLabel = new System.Windows.Forms.Label();
			endTimeLabel.Text = "End time: ";
			endTimeLabel.AutoSize = true;
			endTimeLabel.Location = new System.Drawing.Point(10, 50);
			Controls.Add(endTimeLabel);

			m_endDtPicker = new System.Windows.Forms.DateTimePicker();
			m_endDtPicker.Format = DateTimePickerFormat.Custom;
			m_endDtPicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_endDtPicker.Location = new System.Drawing.Point(200, 50);
			Controls.Add(m_endDtPicker);

			///////////////////////////
			System.Windows.Forms.Label textSourceIdLabel = new System.Windows.Forms.Label();
			textSourceIdLabel.Text = "textSourceIds (space seperated): ";
			textSourceIdLabel.AutoSize = true;
			textSourceIdLabel.Location = new System.Drawing.Point(10, 130);
			Controls.Add(textSourceIdLabel);

			m_textSourceIdEdit = new System.Windows.Forms.TextBox();
			m_textSourceIdEdit.Location = new System.Drawing.Point(200, 130);
			m_textSourceIdEdit.Width = 400;
			Controls.Add(m_textSourceIdEdit);
		}

		public static System.String GetText()
		{
			return "Text Transactions";
		}
		
		public System.DateTime StartDateTime
		{
			get { return m_startDtPicker.Value; }
		}

		public System.DateTime EndDateTime
		{
			get { return m_endDtPicker.Value; }
		}

		public System.Collections.Generic.List<System.String> TextSourceIds
		{
			get
			{
				System.Collections.Generic.List<System.String> retVal = new System.Collections.Generic.List<System.String>();
				System.String[] tokens = m_textSourceIdEdit.Text.Split(new Char[] { ' ' });
				foreach (System.String theString in tokens)
				{
					retVal.Add(theString);
				}

				return retVal;
			}
		}

		private System.Windows.Forms.DateTimePicker m_startDtPicker;
		private System.Windows.Forms.DateTimePicker m_endDtPicker;
		private System.Windows.Forms.TextBox m_textSourceIdEdit;
	}

	/// <summary>
	/// Defines the motion event parameters tab page
	/// </summary>
	public class QueryRecordingsTab :
		System.Windows.Forms.TabPage
	{
		public QueryRecordingsTab()
		{
			this.Text = GetText();

			System.Windows.Forms.Label startTimeLabel = new System.Windows.Forms.Label();
			startTimeLabel.Text = "Start time: ";
			startTimeLabel.AutoSize = true;
			startTimeLabel.Location = new System.Drawing.Point(10, 10);
			Controls.Add(startTimeLabel);

			m_startDtPicker = new System.Windows.Forms.DateTimePicker();
			m_startDtPicker.Format = DateTimePickerFormat.Custom;
			m_startDtPicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_startDtPicker.Location = new System.Drawing.Point(200, 10);
			Controls.Add(m_startDtPicker);

			///////////////////////////
			System.Windows.Forms.Label endTimeLabel = new System.Windows.Forms.Label();
			endTimeLabel.Text = "End time: ";
			endTimeLabel.AutoSize = true;
			endTimeLabel.Location = new System.Drawing.Point(10, 50);
			Controls.Add(endTimeLabel);

			m_endDtPicker = new System.Windows.Forms.DateTimePicker();
			m_endDtPicker.Format = DateTimePickerFormat.Custom;
			m_endDtPicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_endDtPicker.Location = new System.Drawing.Point(200, 50);
			Controls.Add(m_endDtPicker);

			///////////////////////////
			System.Windows.Forms.Label deviceIdLabel = new System.Windows.Forms.Label();
			deviceIdLabel.Text = "DeviceIds (space seperated): ";
			deviceIdLabel.AutoSize = true;
			deviceIdLabel.Location = new System.Drawing.Point(10, 130);
			Controls.Add(deviceIdLabel);

			m_deviceIdEdit = new System.Windows.Forms.TextBox();
			m_deviceIdEdit.Location = new System.Drawing.Point(200, 130);
			m_deviceIdEdit.Width = 400;
			Controls.Add(m_deviceIdEdit);
		}

		public static System.String GetText()
		{
			return "Recordings";
		}
		
		public System.DateTime StartDateTime
		{
			get { return m_startDtPicker.Value; }
		}

		public System.DateTime EndDateTime
		{
			get { return m_endDtPicker.Value; }
		}


		public System.Collections.Generic.List<System.String> DeviceIds
		{
			get
			{
				System.Collections.Generic.List<System.String> retVal = new System.Collections.Generic.List<System.String>();
				System.String[] tokens = m_deviceIdEdit.Text.Split(new Char[] { ' ' });
				foreach (System.String theString in tokens)
				{
					if (theString != "")
						retVal.Add(theString);
				}

				return retVal;
			}
		}

		private System.Windows.Forms.DateTimePicker m_startDtPicker;
		private System.Windows.Forms.DateTimePicker m_endDtPicker;
		private System.Windows.Forms.TextBox m_deviceIdEdit;
	}

	/// <summary>
	/// Defines the timeline parameters tab page
	/// </summary>
	public class QueryTimelineTab :
		System.Windows.Forms.TabPage
	{
		struct PrecisionOption
		{
			public PrecisionOption(string label, AvigilonDotNet.TimelineEventPrecision precision)
			{
				m_label = label;
				m_precision = precision;
			}

			public AvigilonDotNet.TimelineEventPrecision Precision { get { return m_precision; } }
			public override string ToString() { return m_label; }

			private string m_label;
			private AvigilonDotNet.TimelineEventPrecision m_precision;
		}

		public QueryTimelineTab()
		{
			this.Text = GetText();

			System.Windows.Forms.Label startTimeLabel = new System.Windows.Forms.Label();
			startTimeLabel.Text = "Start time: ";
			startTimeLabel.AutoSize = true;
			startTimeLabel.Location = new System.Drawing.Point(10, 10);
			Controls.Add(startTimeLabel);

			m_startDtPicker = new System.Windows.Forms.DateTimePicker();
			m_startDtPicker.Format = DateTimePickerFormat.Custom;
			m_startDtPicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_startDtPicker.Location = new System.Drawing.Point(200, 10);
			Controls.Add(m_startDtPicker);

			///////////////////////////
			System.Windows.Forms.Label endTimeLabel = new System.Windows.Forms.Label();
			endTimeLabel.Text = "End time: ";
			endTimeLabel.AutoSize = true;
			endTimeLabel.Location = new System.Drawing.Point(10, 50);
			Controls.Add(endTimeLabel);

			m_endDtPicker = new System.Windows.Forms.DateTimePicker();
			m_endDtPicker.Format = DateTimePickerFormat.Custom;
			m_endDtPicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_endDtPicker.Location = new System.Drawing.Point(200, 50);
			Controls.Add(m_endDtPicker);

			///////////////////////////
			System.Windows.Forms.Label deviceIdLabel = new System.Windows.Forms.Label();
			deviceIdLabel.Text = "DeviceIds (space seperated): ";
			deviceIdLabel.AutoSize = true;
			deviceIdLabel.Location = new System.Drawing.Point(10, 130);
			Controls.Add(deviceIdLabel);

			m_deviceIdEdit = new System.Windows.Forms.TextBox();
			m_deviceIdEdit.Location = new System.Drawing.Point(200, 130);
			m_deviceIdEdit.Width = 400;
			Controls.Add(m_deviceIdEdit);

			///////////////////////////
			System.Windows.Forms.Label precisionLabel = new System.Windows.Forms.Label();
			precisionLabel.Text = "Precision: ";
			precisionLabel.AutoSize = true;
			precisionLabel.Location = new System.Drawing.Point(10, 160);
			Controls.Add(precisionLabel);

			m_precision = new System.Windows.Forms.ComboBox();
			m_precision.Items.Add(new PrecisionOption("1/10th Second", TimelineEventPrecision.TenthSecond));
			m_precision.Items.Add(new PrecisionOption("1 Second", TimelineEventPrecision.OneSecond));
			m_precision.Items.Add(new PrecisionOption("10 Seconds", TimelineEventPrecision.TenSeconds));
			m_precision.Items.Add(new PrecisionOption("100 Seconds", TimelineEventPrecision.HundredSeconds));
			m_precision.Items.Add(new PrecisionOption("1,000 Seconds", TimelineEventPrecision.ThousandSeconds));
			m_precision.Items.Add(new PrecisionOption("10,000 Seconds", TimelineEventPrecision.TenThousandSeconds));
			m_precision.Items.Add(new PrecisionOption("100,000 Seconds", TimelineEventPrecision.HundredThousandSeconds));
			m_precision.Items.Add(new PrecisionOption("1,000,000 Seconds", TimelineEventPrecision.MillionSeconds));
			m_precision.AutoSize = true;
			m_precision.Location = new System.Drawing.Point(200, 160);
			m_precision.SelectedIndex = 0;
			Controls.Add(m_precision);
		}

		public static System.String GetText()
		{
			return "Timeline";
		}

		public System.DateTime StartDateTime
		{
			get { return m_startDtPicker.Value; }
		}

		public System.DateTime EndDateTime
		{
			get { return m_endDtPicker.Value; }
		}


		public System.Collections.Generic.List<System.String> DeviceIds
		{
			get
			{
				System.Collections.Generic.List<System.String> retVal = new System.Collections.Generic.List<System.String>();
				System.String[] tokens = m_deviceIdEdit.Text.Split(new Char[] { ' ' });
				foreach (System.String theString in tokens)
				{
					if (theString != "")
						retVal.Add(theString);
				}

				return retVal;
			}
		}

		public AvigilonDotNet.TimelineEventPrecision Precision
		{
			get { return ((PrecisionOption)m_precision.SelectedItem).Precision; }
		}

		private System.Windows.Forms.DateTimePicker m_startDtPicker;
		private System.Windows.Forms.DateTimePicker m_endDtPicker;
		private System.Windows.Forms.TextBox m_deviceIdEdit;
		private System.Windows.Forms.ComboBox m_precision;
	}
}