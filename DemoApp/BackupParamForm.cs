﻿/*--	------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DemoApp
{
	class BackupNodeEntity
		: System.Windows.Forms.TreeNode
	{
		public BackupNodeEntity(AvigilonDotNet.IEntity entity)
		{
			m_entity = entity;
			m_entity.InfoChanged += EntityInfoChange_;

			Name = entity.DeviceId;

			UpdateInfo();
		}

		void EntityInfoChange_()
		{
			UpdateInfo();
			TreeView.Refresh();
		}

		void UpdateInfo()
		{
			Text = m_entity.Name;
		}

		private AvigilonDotNet.IEntity m_entity;
	};


	class BackupNodeDevice
		: System.Windows.Forms.TreeNode
	{
		public BackupNodeDevice(AvigilonDotNet.IDevice device)
		{
			m_device = device;
			m_device.InfoChanged += DeviceInfoChanged_;
			m_device.EntityListAdd += EntityListAdd_;

			Name = m_device.DeviceId;
			UpdateInfo_();
			AddAllEntities_();
		}

		void DeviceInfoChanged_()
		{
			UpdateInfo_();
			TreeView.Refresh();
		}

		void UpdateInfo_()
		{
			Text = Device.Name;
		}

		void EntityListAdd_(string deviceId)
		{
			AvigilonDotNet.IEntity entity = m_device.GetEntityById(deviceId);
			if (entity != null &&
				Nodes.Find(deviceId, false).Length == 0)
			{
				// Only show camera entities
				if (entity.Type == AvigilonDotNet.EntityType.Camera)
				{
					// Does not already exist in the tree
					BackupNodeEntity newEntityNode = new BackupNodeEntity(entity);
					Nodes.Add(newEntityNode);

					TreeView.Refresh();
				}
			}
		}

		/// <summary>
		/// Adds subnodes to handle newly discovered nvrs
		/// </summary>
		private void AddAllEntities_()
		{
			// Go through the list of Nvrs add any new Nvrs
			System.Collections.Generic.List<AvigilonDotNet.IEntity> entities = m_device.Entities;
			foreach (AvigilonDotNet.IEntity entity in entities)
			{
				// Only show camera entities
				if (entity.Type == AvigilonDotNet.EntityType.Camera)
				{
					BackupNodeEntity newEntityNode = new BackupNodeEntity(entity);
					Nodes.Add(newEntityNode);
				}
			}
		}

		public AvigilonDotNet.IDevice Device
		{
			get
			{
				return m_device;
			}
		}

		private AvigilonDotNet.IDevice m_device;
	};

	
	/// <summary>
	/// A form that collects information on an backup
	/// </summary>
	class BackupParamForm
		: System.Windows.Forms.Form
	{
		public BackupParamForm(AvigilonDotNet.INvr nvr)
		{
			m_nvr = nvr;
			m_nvr.InfoChanged += new AvigilonDotNet.VoidHandler(NvrInfoChanged);
			m_nvr.DeviceListAdd += DeviceListAdd_;

			Label startTimeLabel = new Label();
			startTimeLabel.Text = "Start Time:";
			startTimeLabel.AutoSize = true;
			startTimeLabel.Location = new Point(10, 10);
			Controls.Add(startTimeLabel);

			m_startTimePicker = new DateTimePicker();
			m_startTimePicker.Format = DateTimePickerFormat.Custom;
			m_startTimePicker.Location = new Point(150, 10);
			m_startTimePicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_startTimePicker.Width = 300;
			Controls.Add(m_startTimePicker);

			Label endTimeLabel = new Label();
			endTimeLabel.Text = "End Time:";
			endTimeLabel.AutoSize = true;
			endTimeLabel.Location = new Point(10, m_startTimePicker.Bottom + 10);
			Controls.Add(endTimeLabel);

			m_endTimePicker = new DateTimePicker();
			m_endTimePicker.Format = DateTimePickerFormat.Custom;
			m_endTimePicker.Location = new Point(150, m_startTimePicker.Bottom + 10);
			m_endTimePicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_endTimePicker.Width = 300;
			Controls.Add(m_endTimePicker);

			Label cameraLabel = new Label();
			cameraLabel.Text = "Select Cameras:";
			cameraLabel.AutoSize = true;
			cameraLabel.Location = new Point(10, m_endTimePicker.Bottom + 10);
			Controls.Add(cameraLabel);

			m_cameras = new TreeView();
			m_cameras.Location = new Point(150, m_endTimePicker.Bottom + 10);
			m_cameras.Size = new Size(300, 200);
			m_cameras.CheckBoxes = true;
			
			Controls.Add(m_cameras);

			Label deleteLabel = new Label();
			deleteLabel.Text = "Delete oldest (if req):";
			deleteLabel.AutoSize = true;
			deleteLabel.Location = new Point(10, m_cameras.Bottom + 10);
			Controls.Add(deleteLabel);

			Controls.Add(startTimeLabel);
			m_bDelete = new CheckBox();
			m_bDelete.AutoSize = true;
			m_bDelete.Location = new Point(150, m_cameras.Bottom + 10);
			Controls.Add(m_bDelete);

			m_CancelButton = new System.Windows.Forms.Button();
			m_CancelButton.Text = "Cancel";
			m_CancelButton.Location = new Point(m_cameras.Right - m_CancelButton.Width, deleteLabel.Bottom + 10);
			m_CancelButton.Click += new EventHandler(CancelClick_);
			Controls.Add(m_CancelButton);

			m_OkButton = new System.Windows.Forms.Button();
			m_OkButton.Text = "Ok";
			m_OkButton.Location = new Point(m_CancelButton.Left - 10 - m_OkButton.Width, deleteLabel.Bottom + 10);
			m_OkButton.Click += new EventHandler(OkClick_);
			Controls.Add(m_OkButton);

			Size = new System.Drawing.Size(m_cameras.Right + 20 , m_CancelButton.Bottom + 50);
			Text = "Backup";
			StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;

			AddAllDevices_();
			UpdateInfo_();

			m_cameras.ExpandAll();
		}

		void NvrInfoChanged()
		{
			UpdateInfo_();
			m_cameras.Refresh();
		}

		void UpdateInfo_()
		{
			Text = "Backup " + m_nvr.Name;
		}

		void DeviceListAdd_(string deviceId)
		{
			AvigilonDotNet.IDevice device = m_nvr.GetDeviceById(deviceId);
			if (device != null &&
				m_cameras.Nodes.Find(deviceId, false).Length == 0)
			{
				// Does not already exist in the tree
				TreeNodeDevice newDeviceNode = new TreeNodeDevice(device);
				m_cameras.Nodes.Add(newDeviceNode);
				m_cameras.Refresh();

				newDeviceNode.ExpandAll();
			}
		}

		/// <summary>
		/// Adds subnodes to handle newly discovered devices
		/// </summary>
		private void AddAllDevices_()
		{
			// Go through the list of Nvrs add any new Nvrs
			System.Collections.Generic.List<AvigilonDotNet.IDevice> devices = m_nvr.Devices;
			foreach (AvigilonDotNet.IDevice device in devices)
			{
				BackupNodeDevice newDeviceNode = new BackupNodeDevice(device);
				m_cameras.Nodes.Add(newDeviceNode);
			}
		}

		/// <summary>
		/// Ok button click handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		void OkClick_(
			System.Object sender,
			System.EventArgs args)
		{
			DialogResult = DialogResult.OK;
			Close();
		}

		/// <summary>
		///  Cancel button click handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		void CancelClick_(
			System.Object sender,
			System.EventArgs args)
		{
			DialogResult = DialogResult.Cancel;
			Close();
		}

		public List<String> DeviceIds
		{
			get
			{
				List<String> retVal = new List<string>();
				foreach (TreeNode deviceNode in m_cameras.Nodes)
				{
					foreach (TreeNode camNode in deviceNode.Nodes)
					{
						if (camNode.Checked)
						{
							retVal.Add(camNode.Name);
						}
					}
				}

				return retVal;
			}
		}

		public DateTime StartTime
		{
			get
			{
				return m_startTimePicker.Value;
			}
		}

		public DateTime EndTime
		{
			get
			{
				return m_endTimePicker.Value;
			}
		}

		public bool DeleteIfReq
		{
			get
			{
				return m_bDelete.Checked;
			}
		}

		private AvigilonDotNet.INvr m_nvr;
		private System.Windows.Forms.DateTimePicker m_startTimePicker;
		private System.Windows.Forms.DateTimePicker m_endTimePicker;
		private System.Windows.Forms.TreeView m_cameras;
		private System.Windows.Forms.CheckBox m_bDelete;

		private System.Windows.Forms.Button m_OkButton;
		private System.Windows.Forms.Button m_CancelButton;
	}
}
