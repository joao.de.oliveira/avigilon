/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DemoApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			Application.Run(new InitForm());
		}
    }
}
