/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

namespace DemoApp
{
	public delegate System.String GetInfoValue();

	public class InfoFormItem
		: System.Windows.Forms.ListViewItem
	{
		public InfoFormItem(
			System.String itemName,
			System.String itemValue)
			: base(itemName)
		{
			SubItems.Add(itemValue);
		}
	}

	abstract public class InfoForm
		: System.Windows.Forms.Form
	{
		protected System.Object m_theObj;
		protected System.Windows.Forms.ListView m_infoList;
		System.Windows.Forms.ContextMenuStrip m_contextMenu;
		System.Windows.Forms.ToolStripMenuItem m_copyValItem;

		public InfoForm(System.Object theObj)
		{
			m_theObj = theObj;

			m_infoList = new System.Windows.Forms.ListView();
			m_infoList.View = System.Windows.Forms.View.Details;
			m_infoList.GridLines = true;
			m_infoList.Location = new System.Drawing.Point(10, 10);
			m_infoList.MultiSelect = false;
			m_infoList.FullRowSelect = true;

			// Context menu
			m_contextMenu = new System.Windows.Forms.ContextMenuStrip();
			m_infoList.ContextMenuStrip = m_contextMenu;

			m_copyValItem = new System.Windows.Forms.ToolStripMenuItem("Copy Value");
			m_copyValItem.Click += new System.EventHandler(OnCopyValue_);
			m_contextMenu.Items.Add(m_copyValItem);

			Controls.Add(m_infoList);

			LayoutControls_();

			m_infoList.Columns.Add("Field", 100, System.Windows.Forms.HorizontalAlignment.Left);
			m_infoList.Columns.Add("Value", 100, System.Windows.Forms.HorizontalAlignment.Left);

			this.Text = "Info";
			this.Layout += OnLayout_;
		}

		private void OnCopyValue_(System.Object sender, System.EventArgs e)
		{
			if (m_infoList.SelectedItems.Count != 0)
			{
				System.Windows.Forms.Clipboard.SetText(m_infoList.SelectedItems[0].SubItems[1].Text);
			}
		}

		protected void RefreshContents()
		{
			m_infoList.Items.Clear();

			// Using reflection, insert each property the object exposes
			System.Type objType = m_theObj.GetType();
			System.Reflection.PropertyInfo[] props = objType.GetProperties();

			foreach (System.Reflection.PropertyInfo prop in props)
			{
				// Don't show obsolete attributes
				object propValue = prop.GetValue(m_theObj, null);
				AddItem_(prop.Name, propValue);
			}
		}

		private void AddItem_(
			System.String name,
			object val)
		{
			if (val != null)
				m_infoList.Items.Add(new InfoFormItem(name, val.ToString()));
			else
				m_infoList.Items.Add(new InfoFormItem(name, "null"));
		}

		private void OnLayout_(
			System.Object sender,
			System.Windows.Forms.LayoutEventArgs e)
		{
			LayoutControls_();
		}

		private void LayoutControls_()
		{
			System.Drawing.Rectangle clientRect = this.ClientRectangle;
			m_infoList.Size = new System.Drawing.Size(clientRect.Width - 20, clientRect.Height - 20);
		}

	}

	public class AlarmInfoForm : InfoForm
	{
		public AlarmInfoForm(AvigilonDotNet.IAlarm alarm)
			: base(alarm)
		{
			alarm.InfoChanged += AlarmInfoChanged;
			AlarmInfoChanged();
		}

		void AlarmInfoChanged()
		{
			Text = ((AvigilonDotNet.IAlarm)m_theObj).Name;
			RefreshContents();
		}
	}

	public class ArchiveInfoForm : InfoForm
	{
		public ArchiveInfoForm(AvigilonDotNet.IFileArchive archive)
			: base(archive)
		{
			archive.InfoChanged += ArchiveInfoChanged;
			ArchiveInfoChanged();
		}

		void ArchiveInfoChanged()
		{
			Text = ((AvigilonDotNet.IFileArchive)m_theObj).FileArchivePath;
			RefreshContents();
		}
	}

	public class DeviceInfoForm : InfoForm
	{
		public DeviceInfoForm(AvigilonDotNet.IDevice device)
			: base(device)
		{
			device.InfoChanged += DeviceInfoChanged;
			DeviceInfoChanged();
		}

		void DeviceInfoChanged()
		{
			Text = ((AvigilonDotNet.IDevice)m_theObj).Name;
			RefreshContents();
		}
	}

	public class EntityInfoForm : InfoForm
	{
		public EntityInfoForm(AvigilonDotNet.IEntity entity)
			: base(entity)
		{
			entity.InfoChanged += EntityInfoChanged;
			EntityInfoChanged();
		}

		protected virtual void EntityInfoChanged()
		{
			Text = ((AvigilonDotNet.IEntity)m_theObj).Name;
			RefreshContents();
		}
	}

	public class NvrInfoForm : InfoForm
	{
		public NvrInfoForm(AvigilonDotNet.INvr nvr)
			: base(nvr)
		{
			nvr.InfoChanged += Update_;
			Update_();
		}

		void Update_()
		{
			Text = ((AvigilonDotNet.INvr)m_theObj).Name;
			RefreshContents();
		}
	}

	public class LicenseInfoForm : InfoForm
	{
		public LicenseInfoForm(AvigilonDotNet.ILicenseAbilities license)
			: base(license)
		{
			license.InfoChanged += Update_;
			Update_();
		}

		void Update_()
		{
			Text = "License";
			RefreshContents();
		}
	}

	public class TextSourceInfoForm : InfoForm
	{
		public TextSourceInfoForm(AvigilonDotNet.ITextSource textSource)
			: base(textSource)
		{
			textSource.InfoChanged += TextSourceInfoChanged;
			TextSourceInfoChanged();
		}

		void TextSourceInfoChanged()
		{
			Text = ((AvigilonDotNet.ITextSource)m_theObj).Name;
			RefreshContents();
		}
	}

	public class BookmarkInfoForm : InfoForm
	{
		public BookmarkInfoForm(AvigilonDotNet.IBookmark bookmark) : base(bookmark)
		{
			bookmark.InfoChanged += BookmarkInfoChanged_;
			BookmarkInfoChanged_();
		}

		void BookmarkInfoChanged_()
		{
			Text = ((AvigilonDotNet.IBookmark)m_theObj).BookmarkName;
			RefreshContents();
		}
	}

}
