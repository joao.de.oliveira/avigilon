/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DemoApp
{
	/// <summary>
	/// A form that collects information on an export
	/// </summary>
	class ExportParamForm 
		: System.Windows.Forms.Form
	{
		/// <summary>
		/// Constructor
		/// </summary>
		public ExportParamForm()
		{
			Text = "Export";
			StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;

			m_startTimeLabel = new System.Windows.Forms.Label();
			m_startTimeLabel.Text = "Start Time:";
			m_startTimeLabel.AutoSize = true;
			m_startTimeLabel.Location = new System.Drawing.Point(10, 10);
			Controls.Add(m_startTimeLabel);

			m_startTimePicker = new System.Windows.Forms.DateTimePicker();
			m_startTimePicker.Format = DateTimePickerFormat.Custom;
			m_startTimePicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_startTimePicker.Location = new System.Drawing.Point(100, 8);
			m_startTimePicker.Width = 145;
			Controls.Add(m_startTimePicker);

			m_endTimeLabel = new System.Windows.Forms.Label();
			m_endTimeLabel.Text = "End Time:";
			m_endTimeLabel.AutoSize = true;
			m_endTimeLabel.Location = new System.Drawing.Point(10, 35);
			Controls.Add(m_endTimeLabel);

			m_endTimePicker = new System.Windows.Forms.DateTimePicker();
			m_endTimePicker.Format = DateTimePickerFormat.Custom;
			m_endTimePicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_endTimePicker.Location = new System.Drawing.Point(100, 33);
			m_endTimePicker.Width = 145;
			Controls.Add(m_endTimePicker);

			m_widthLabel = new System.Windows.Forms.Label();
			m_widthLabel.Text = "Width:";
			m_widthLabel.AutoSize = true;
			m_widthLabel.Location = new System.Drawing.Point(10, 60);
			Controls.Add(m_widthLabel);

			m_widthUpDown = new NumericUpDown();
			m_widthUpDown.Location = new System.Drawing.Point(100, 60);
			m_widthUpDown.Minimum = 0;
			m_widthUpDown.Maximum = 65536;
			m_widthUpDown.Increment = 1;
			m_widthUpDown.DecimalPlaces = 0;
			Controls.Add(m_widthUpDown);

			m_heightLabel = new System.Windows.Forms.Label();
			m_heightLabel.Text = "Height:";
			m_heightLabel.AutoSize = true;
			m_heightLabel.Location = new System.Drawing.Point(10, 85);
			Controls.Add(m_heightLabel);

			m_heightUpDown = new NumericUpDown();
			m_heightUpDown.Location = new System.Drawing.Point(100, 85);
			m_heightUpDown.Minimum = 0;
			m_heightUpDown.Maximum = 65536;
			m_heightUpDown.Increment = 1;
			m_heightUpDown.DecimalPlaces = 0;
			Controls.Add(m_heightUpDown);

			m_leftLabel = new System.Windows.Forms.Label();
			m_leftLabel.Text = "Left:";
			m_leftLabel.AutoSize = true;
			m_leftLabel.Location = new System.Drawing.Point(10, 110);
			Controls.Add(m_leftLabel);

			m_leftUpDown = new NumericUpDown();
			m_leftUpDown.Location = new System.Drawing.Point(100, 110);
			m_leftUpDown.Minimum = 0.0M;
			m_leftUpDown.Value = 0.0M;
			m_leftUpDown.Maximum = 1.0M;
			m_leftUpDown.Increment = 0.01M;
			m_leftUpDown.DecimalPlaces = 2;
			Controls.Add(m_leftUpDown);

			m_topLabel = new System.Windows.Forms.Label();
			m_topLabel.Text = "Top:";
			m_topLabel.AutoSize = true;
			m_topLabel.Location = new System.Drawing.Point(10, 135);
			Controls.Add(m_topLabel);

			m_topUpDown = new NumericUpDown();
			m_topUpDown.Location = new System.Drawing.Point(100, 135);
			m_topUpDown.Minimum = 0.0M;
			m_topUpDown.Value = 0.0M;
			m_topUpDown.Maximum = 1.0M;
			m_topUpDown.Increment = 0.01M;
			m_topUpDown.DecimalPlaces = 2;
			Controls.Add(m_topUpDown);

			m_rightLabel = new System.Windows.Forms.Label();
			m_rightLabel.Text = "Right:";
			m_rightLabel.AutoSize = true;
			m_rightLabel.Location = new System.Drawing.Point(10, 160);
			Controls.Add(m_rightLabel);

			m_rightUpDown = new NumericUpDown();
			m_rightUpDown.Location = new System.Drawing.Point(100, 160);
			m_rightUpDown.Minimum = 0.0M;
			m_rightUpDown.Value = 1.0M;
			m_rightUpDown.Maximum = 1.0M;
			m_rightUpDown.Increment = 0.01M;
			m_rightUpDown.DecimalPlaces = 2;
			Controls.Add(m_rightUpDown);

			m_bottomLabel = new System.Windows.Forms.Label();
			m_bottomLabel.Text = "Bottom:";
			m_bottomLabel.AutoSize = true;
			m_bottomLabel.Location = new System.Drawing.Point(10, 185);
			Controls.Add(m_bottomLabel);

			m_bottomUpDown = new NumericUpDown();
			m_bottomUpDown.Location = new System.Drawing.Point(100, 185);
			m_bottomUpDown.Minimum = 0.0M;
			m_bottomUpDown.Value = 1.0M;
			m_bottomUpDown.Maximum = 1.0M;
			m_bottomUpDown.Increment = 0.01M;
			m_bottomUpDown.DecimalPlaces = 2;
			Controls.Add(m_bottomUpDown);

			m_exportTypePicker = new EnumRadioSelector<AvigilonDotNet.ExportType>(2,(Width-10)/2);
			m_exportTypePicker.Location = new System.Drawing.Point(10,210);
			m_exportTypePicker.Width = ClientSize.Width - 20;
			m_exportTypePicker.Value = AvigilonDotNet.ExportType.AVE; // default;
			m_exportTypePicker.ValueChanged += new EventHandler(RadioChangeHandler);
			Controls.Add(m_exportTypePicker);

			m_compressionPicker = new EnumRadioSelector<AvigilonDotNet.CompressionFormat>(2, (Width - 2) / 2);
			m_compressionPicker.Location = new System.Drawing.Point(10, m_exportTypePicker.Bottom + 10);
			m_compressionPicker.Width = ClientSize.Width - 20;
			m_compressionPicker.Value = AvigilonDotNet.CompressionFormat.VC1; // default;
			m_compressionPicker.ValueChanged += new EventHandler(RadioChangeHandler);
			Controls.Add(m_compressionPicker);

			m_overlayPicker = new CheckedListBox();
			m_overlayPicker.Location = new System.Drawing.Point(10, m_compressionPicker.Bottom + 2);
			m_overlayPicker.Width = ClientSize.Width - 20;
			m_overlayPicker.CheckOnClick = true;
			foreach(AvigilonDotNet.VideoOverlayOption overlay in Enum.GetValues(typeof(AvigilonDotNet.VideoOverlayOption)))
			{
				if(overlay != 0)
					m_overlayPicker.Items.Add(overlay);
			}
			Controls.Add(m_overlayPicker);

			m_OkButton = new System.Windows.Forms.Button();
			m_OkButton.Text = "Ok";
			m_OkButton.Location = new System.Drawing.Point(95, m_overlayPicker.Bottom + 2);
			m_OkButton.Click += new EventHandler(OkClick_);
			Controls.Add(m_OkButton);

			m_CancelButton = new System.Windows.Forms.Button();
			m_CancelButton.Text = "Cancel";
			m_CancelButton.Location = new System.Drawing.Point(175, m_overlayPicker.Bottom + 2);
			m_CancelButton.Click += new EventHandler(CancelClick_);
			Controls.Add(m_CancelButton);

			ClientSize = new Size(ClientSize.Width, m_CancelButton.Bottom + 2);
			Invalidate();

			UpdateControlsEnabled();
		}

		void RadioChangeHandler(object sender, EventArgs e)
		{
			UpdateControlsEnabled();
		}

		void UpdateControlsEnabled()
		{
			switch (m_exportTypePicker.Value)
			{
				case AvigilonDotNet.ExportType.AVE:
				case AvigilonDotNet.ExportType.AVI:
				case AvigilonDotNet.ExportType.PNG:
				case AvigilonDotNet.ExportType.JPEG:
				case AvigilonDotNet.ExportType.TIFF:
				case AvigilonDotNet.ExportType.PGM:
				case AvigilonDotNet.ExportType.PDF:
					m_heightUpDown.Enabled = true;
					m_widthUpDown.Enabled = true;
					m_leftUpDown.Enabled = true;
					m_topUpDown.Enabled = true;
					m_rightUpDown.Enabled = true;
					m_bottomUpDown.Enabled = true;
					break;

				case AvigilonDotNet.ExportType.WAV:
					m_heightUpDown.Enabled = false;
					m_widthUpDown.Enabled = false;
					m_leftUpDown.Enabled = false;
					m_topUpDown.Enabled = false;
					m_rightUpDown.Enabled = false;
					m_bottomUpDown.Enabled = false;
					break;
			}


			m_overlayPicker.Enabled = !(m_exportTypePicker.Value == AvigilonDotNet.ExportType.AVE ||
				m_exportTypePicker.Value == AvigilonDotNet.ExportType.WAV);
			m_compressionPicker.Enabled = (m_exportTypePicker.Value == AvigilonDotNet.ExportType.AVI);
		}

		/// <summary>
		/// Ok button click handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		void OkClick_(
			System.Object sender,
			System.EventArgs args)
		{
			// Check validity of export params
			if (!ExportParams.IsValid)
			{
				MessageBox.Show("Invalid export arguments specified");
				return;
			}

			SaveFileDialog fd = new SaveFileDialog();
			
			// Determine file dialog filter
			switch (m_exportTypePicker.Value)
			{
				case AvigilonDotNet.ExportType.AVE:
					fd.Filter = AvigilonDotNet.ExportType.AVE.ToString() + "|*" + ExportParams.DefaultExtension;
					break;

				case AvigilonDotNet.ExportType.AVI:
					fd.Filter = AvigilonDotNet.ExportType.AVI.ToString() + "|*" + ExportParams.DefaultExtension;
					break;

				case AvigilonDotNet.ExportType.PNG:
					fd.Filter = AvigilonDotNet.ExportType.PNG.ToString() + "|*" + ExportParams.DefaultExtension;
					break;

				case AvigilonDotNet.ExportType.JPEG:
					fd.Filter = AvigilonDotNet.ExportType.JPEG.ToString() + "|*" + ExportParams.DefaultExtension;
					break;

				case AvigilonDotNet.ExportType.TIFF:
					fd.Filter = AvigilonDotNet.ExportType.TIFF.ToString() + "|*" + ExportParams.DefaultExtension;
					break;

				case AvigilonDotNet.ExportType.PGM:
					fd.Filter = AvigilonDotNet.ExportType.PGM.ToString() + "|*" + ExportParams.DefaultExtension;
					break;

				case AvigilonDotNet.ExportType.PDF:
					fd.Filter = AvigilonDotNet.ExportType.PDF.ToString() + "|*" + ExportParams.DefaultExtension;
					break;

				case AvigilonDotNet.ExportType.WAV:
					fd.Filter = AvigilonDotNet.ExportType.WAV.ToString() + "|*" + ExportParams.DefaultExtension;
					break;

				default:
					DialogResult = DialogResult.Cancel;
					Close();
					break;
			}

			// Display save file dialog and save result
			if (fd.ShowDialog() == DialogResult.Cancel)
				return;

			m_exportFile = fd.FileName;
			DialogResult = DialogResult.OK;
		}

		/// <summary>
		///  Cancel button click handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		void CancelClick_(
			System.Object sender,
			System.EventArgs args)
		{
			DialogResult = DialogResult.Cancel;
			Close();
		}

		/// <summary>
		/// Gets the file name to write to
		/// </summary>
		public System.String FileName
		{
			get
			{
				return m_exportFile;
			}
		}

		/// <summary>
		/// Export start time
		/// </summary>
		public System.DateTime StartTime
		{
			get
			{
				return m_startTimePicker.Value;
			}
		}

		/// <summary>
		/// Export end time
		/// </summary>
		public System.DateTime EndTime
		{
			get
			{
				return m_endTimePicker.Value;
			}
		}

		/// <summary>
		/// Export type
		/// </summary>
		public AvigilonDotNet.ExportType ExportType
		{
			get
			{
				return m_exportTypePicker.Value;
			}
		}

		public AvigilonDotNet.ExportParams ExportParams
		{
			get
			{
				switch (m_exportTypePicker.Value)
				{
					case AvigilonDotNet.ExportType.AVE:
						{
							AvigilonDotNet.ExportParamsAve ep = new AvigilonDotNet.ExportParamsAve();
							ep.Resolution = new System.Drawing.Size((int)m_widthUpDown.Value, (int)m_heightUpDown.Value);
							ep.Left= (float)m_leftUpDown.Value;
							ep.Top = (float)m_topUpDown.Value;
							ep.Right = (float)m_rightUpDown.Value;
							ep.Bottom = (float)m_bottomUpDown.Value;
							return ep;
						}

					case AvigilonDotNet.ExportType.AVI:
						{
							AvigilonDotNet.ExportParamsAvi ep = new AvigilonDotNet.ExportParamsAvi();
							ep.Resolution = new System.Drawing.Size((int)m_widthUpDown.Value, (int)m_heightUpDown.Value);
							ep.Left= (float)m_leftUpDown.Value;
							ep.Top = (float)m_topUpDown.Value;
							ep.Right = (float)m_rightUpDown.Value;
							ep.Bottom = (float)m_bottomUpDown.Value;
							ep.Compression = m_compressionPicker.Value;
							ep.VideoOverlays = Overlays;
							return ep;
						}

					case AvigilonDotNet.ExportType.PNG:
						{
							AvigilonDotNet.ExportParamsPng ep = new AvigilonDotNet.ExportParamsPng();
							ep.Resolution = new System.Drawing.Size((int)m_widthUpDown.Value, (int)m_heightUpDown.Value);
							ep.Left= (float)m_leftUpDown.Value;
							ep.Top = (float)m_topUpDown.Value;
							ep.Right = (float)m_rightUpDown.Value;
							ep.Bottom = (float)m_bottomUpDown.Value;
							ep.VideoOverlays = Overlays;
							return ep;
						}

					case AvigilonDotNet.ExportType.JPEG:
						{
							AvigilonDotNet.ExportParamsJpeg ep = new AvigilonDotNet.ExportParamsJpeg();
							ep.Resolution = new System.Drawing.Size((int)m_widthUpDown.Value, (int)m_heightUpDown.Value);
							ep.Left= (float)m_leftUpDown.Value;
							ep.Top = (float)m_topUpDown.Value;
							ep.Right = (float)m_rightUpDown.Value;
							ep.Bottom = (float)m_bottomUpDown.Value;
							ep.VideoOverlays = Overlays;
							return ep;
						}

					case AvigilonDotNet.ExportType.TIFF:
						{
							AvigilonDotNet.ExportParamsTiff ep = new AvigilonDotNet.ExportParamsTiff();
							ep.Resolution = new System.Drawing.Size((int)m_widthUpDown.Value, (int)m_heightUpDown.Value);
							ep.Left= (float)m_leftUpDown.Value;
							ep.Top = (float)m_topUpDown.Value;
							ep.Right = (float)m_rightUpDown.Value;
							ep.Bottom = (float)m_bottomUpDown.Value;
							ep.VideoOverlays = Overlays;
							return ep;
						}

					case AvigilonDotNet.ExportType.PGM:
						{
							AvigilonDotNet.ExportParamsPgm ep = new AvigilonDotNet.ExportParamsPgm();
							ep.Resolution = new System.Drawing.Size((int)m_widthUpDown.Value, (int)m_heightUpDown.Value);
							ep.Left= (float)m_leftUpDown.Value;
							ep.Top = (float)m_topUpDown.Value;
							ep.Right = (float)m_rightUpDown.Value;
							ep.Bottom = (float)m_bottomUpDown.Value;
							ep.VideoOverlays = Overlays;
							return ep;
						}

					case AvigilonDotNet.ExportType.PDF:
						{
							AvigilonDotNet.ExportParamsPdf ep = new AvigilonDotNet.ExportParamsPdf();
							ep.Resolution = new System.Drawing.Size((int)m_widthUpDown.Value, (int)m_heightUpDown.Value);
							ep.Left= (float)m_leftUpDown.Value;
							ep.Top = (float)m_topUpDown.Value;
							ep.Right = (float)m_rightUpDown.Value;
							ep.Bottom = (float)m_bottomUpDown.Value;
							ep.VideoOverlays = Overlays;
							return ep;
						}

					case AvigilonDotNet.ExportType.WAV:
						{
							AvigilonDotNet.ExportParamsWav ep = new AvigilonDotNet.ExportParamsWav();
							return ep;
						}

					default:
						return null;
				}
			}
		}

		private AvigilonDotNet.VideoOverlayOption Overlays
		{
			get
			{
				AvigilonDotNet.VideoOverlayOption retVal = 0;
				foreach(AvigilonDotNet.VideoOverlayOption obj in m_overlayPicker.CheckedItems)
				{
					retVal |= obj;
				}

				return retVal;
			}
		}

		private System.Windows.Forms.Label m_startTimeLabel;
		private System.Windows.Forms.DateTimePicker m_startTimePicker;
		private System.Windows.Forms.Label m_endTimeLabel;
		private System.Windows.Forms.DateTimePicker m_endTimePicker;
		private EnumRadioSelector<AvigilonDotNet.ExportType> m_exportTypePicker;
		private System.Windows.Forms.Label m_widthLabel;
		private System.Windows.Forms.NumericUpDown m_widthUpDown;
		private System.Windows.Forms.Label m_heightLabel;
		private System.Windows.Forms.NumericUpDown m_heightUpDown;

		private System.Windows.Forms.Label m_leftLabel;
		private System.Windows.Forms.NumericUpDown m_leftUpDown;
		private System.Windows.Forms.Label m_topLabel;
		private System.Windows.Forms.NumericUpDown m_topUpDown;
		private System.Windows.Forms.Label m_rightLabel;
		private System.Windows.Forms.NumericUpDown m_rightUpDown;
		private System.Windows.Forms.Label m_bottomLabel;
		private System.Windows.Forms.NumericUpDown m_bottomUpDown;

		private EnumRadioSelector<AvigilonDotNet.CompressionFormat> m_compressionPicker;

		private System.Windows.Forms.CheckedListBox m_overlayPicker;

		private string m_exportFile;

		private System.Windows.Forms.Button m_OkButton;
		private System.Windows.Forms.Button m_CancelButton;
	}
}
