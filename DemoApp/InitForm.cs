﻿/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DemoApp
{
	public partial class InitForm
		: System.Windows.Forms.Form
	{
		public InitForm()
		{
			Text = "ACC Demo App";
			this.Size = new Size(300, 250);

			m_initSharedButton = new System.Windows.Forms.Button();
			m_initSharedButton.Text = "Init Shared";
			m_initSharedButton.Location = new System.Drawing.Point(10, 10);
			m_initSharedButton.Width = Width - 40;
			m_initSharedButton.Click += new EventHandler(m_initSharedButton_Click);
			Controls.Add(m_initSharedButton);

			m_initUniqueButton = new System.Windows.Forms.Button();
			m_initUniqueButton.Text = "Init Unique";
			m_initUniqueButton.Location = new System.Drawing.Point(10, m_initSharedButton.Bottom + 10);
			m_initUniqueButton.Width = Width - 40;
			m_initUniqueButton.Click += new EventHandler(m_initUniqueButton_Click);
			Controls.Add(m_initUniqueButton);

			m_autoDicoverCheckBox = new CheckBox();
			m_autoDicoverCheckBox.Text = "Auto discover sites";
			m_autoDicoverCheckBox.Location = new System.Drawing.Point(10, m_initUniqueButton.Bottom + 10);
			m_autoDicoverCheckBox.Width = Width - 40;
			m_autoDicoverCheckBox.Checked = true;
			Controls.Add(m_autoDicoverCheckBox);

			m_exitButton = new System.Windows.Forms.Button();
			m_exitButton.Text = "Exit";
			m_exitButton.Location = new System.Drawing.Point(10, m_autoDicoverCheckBox.Bottom + 10);
			m_exitButton.Width = Width - 40;
			m_exitButton.Click += new EventHandler(m_exitButton_Click);
			Controls.Add(m_exitButton);

			this.MinimumSize = this.Size;
			this.MaximumSize = this.Size;
			this.MaximizeBox = false;
			this.SizeGripStyle = SizeGripStyle.Hide;
		}

		void m_exitButton_Click(object sender, EventArgs e)
		{
			Close();
		}

		void m_initUniqueButton_Click(object sender, EventArgs e)
		{
			try
			{
				AvigilonDotNet.SdkInitParams initParams = new AvigilonDotNet.SdkInitParams(
					AvigilonDotNet.AvigilonSdk.MajorVersion,
					AvigilonDotNet.AvigilonSdk.MinorVersion);
				initParams.AutoDiscoverNvrs = m_autoDicoverCheckBox.Checked;
				initParams.ServiceMode = false;

				AvigilonDotNet.AvigilonSdk sdk = new AvigilonDotNet.AvigilonSdk();
				AvigilonDotNet.IAvigilonControlCenter acc = sdk.CreateInstance(initParams);

				DemoAppForm daf = new DemoAppForm(
					acc,
					true); // dispose of acc when done
				daf.Show(this);
			}
			catch (System.Exception err)
			{
				MessageBox.Show("Failed to initialize Avigilon Control Center: " + err.Message);
			}
		}

		void m_initSharedButton_Click(object sender, EventArgs e)
		{
			try
			{
				if (m_sharedAcc == null)
				{
					AvigilonDotNet.AvigilonSdk sdk = new AvigilonDotNet.AvigilonSdk();

					AvigilonDotNet.SdkInitParams initParams = new AvigilonDotNet.SdkInitParams(
						AvigilonDotNet.AvigilonSdk.MajorVersion,
						AvigilonDotNet.AvigilonSdk.MinorVersion);
					initParams.AutoDiscoverNvrs = m_autoDicoverCheckBox.Checked;
					initParams.ServiceMode = false;

					AvigilonDotNet.IAvigilonControlCenter acc = sdk.CreateInstance(initParams);
					m_sharedAcc = acc;
				}

				DemoAppForm daf = new DemoAppForm(
					m_sharedAcc,
					false); // do not dispose of acc
				daf.Show(this);
			}
			catch (System.Exception err)
			{
				MessageBox.Show("Failed to initialize Avigilon Control Center: " + err.Message);
			}
		}

		protected override void OnFormClosing(FormClosingEventArgs e)
		{
			if (m_sharedAcc != null)
			{
				m_sharedAcc = null;
			}

			AvigilonDotNet.AvigilonSdk sdk = new AvigilonDotNet.AvigilonSdk();
			AvigilonDotNet.AvgError err = sdk.Shutdown();
			if (AvigilonDotNet.ErrorHelper.IsError(err))
			{
				MessageBox.Show("Failed to shutdown sdk: " + err.ToString());
				e.Cancel = true;
			}
		}

		AvigilonDotNet.IAvigilonControlCenter m_sharedAcc = null;

		private System.Windows.Forms.Button m_initSharedButton;
		private System.Windows.Forms.Button m_initUniqueButton;
		private System.Windows.Forms.CheckBox m_autoDicoverCheckBox;
		private System.Windows.Forms.Button m_exitButton;
	}
}
