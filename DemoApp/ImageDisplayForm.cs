/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DemoApp
{
	class ImageDisplayForm
		: System.Windows.Forms.Form
	{
		public ImageDisplayForm(AvigilonDotNet.IFrame frame)
		{
			m_frameImage = frame as AvigilonDotNet.IFrameImage;
			if (m_frameImage != null)
			{
				//AvigilonDotNet.IFrameBitmapRepresentation fbr = m_frameImage as AvigilonDotNet.IFrameBitmapRepresentation;
				//if (fbr != null)
				//{
				//    m_bitmap = fbr.GetBitmap();
				//}

				// Create from bitmap - for informational purposes only. If the users intention is
				// to display the image, it is much more efficient to obtain a bitmap representation/
				// as there is less copying involved.
				AvigilonDotNet.IFrameImageRaw fir = m_frameImage as AvigilonDotNet.IFrameImageRaw;
				if (fir != null)
				{
					m_bitmap = MakeBitmapFromImageRaw(fir);
				}
			}

			InitializeForm();
		}

		private void InitializeForm()
		{
			Label widthLabel = new Label();
			widthLabel.Location = new Point(10, 10);
			Controls.Add(widthLabel);
			widthLabel.AutoSize = true;
			widthLabel.Text = "Width: " + (m_frameImage != null ? m_frameImage.Size.Width.ToString() : "No Image");

			Label heightLabel = new Label();
			heightLabel.Location = new Point(110, 10);
			Controls.Add(heightLabel);
			heightLabel.AutoSize = true;
			heightLabel.Text = "Height: " + (m_frameImage != null ? m_frameImage.Size.Height.ToString() : "No Image");

			Label bppLabel = new Label();
			bppLabel.Location = new Point(210, 10);
			Controls.Add(bppLabel);
			bppLabel.AutoSize = true;
			bppLabel.Text = "Pixel Format: " + (m_bitmap != null ? m_bitmap.PixelFormat.ToString() : "No Bitmap");

			String tsString;
			if (m_frameImage != null)
			{
				tsString = m_frameImage.Timestamp.ToShortDateString() + " " + m_frameImage.Timestamp.ToLongTimeString();
			}
			else
			{
			    tsString = "No frame";
			}

			Label tsLabel = new Label();
			tsLabel.Location = new Point(410, 10);
			Controls.Add(tsLabel);
			tsLabel.AutoSize = true;
			tsLabel.Text = "Timestamp: " + tsString;

			Size = new Size(800, 600);

			FormClosing += OnClose_;
			StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			Text = "Frame Query Result";
			FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
		}

		private void OnClose_(
			System.Object sender,
			System.Windows.Forms.FormClosingEventArgs e)
		{
			// We're done with it. Dispose of it so that we can
			// make another request immediately.
			m_frameImage.Dispose();
		}

		protected override void OnSizeChanged(EventArgs e)
		{
			Invalidate();
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			e.Graphics.Clear(System.Drawing.SystemColors.Control);

			
			if(m_bitmap != null)
			{
				// calculate aspect ratio
				Rectangle clientRectMax = new Rectangle(10, 40, ClientSize.Width - 20, ClientSize.Height - 50);
				float bitMapAspect = (float)(m_bitmap.Width) / (float)(m_bitmap.Height);
				float windowAspect = (float)(clientRectMax.Width) / (float)(clientRectMax.Height);

				Rectangle drawToRect;
				if(bitMapAspect > windowAspect)
				{
					// Fit to window width
					drawToRect = new Rectangle(clientRectMax.X,clientRectMax.Y,
						clientRectMax.Width, (int)((float)(clientRectMax.Width) / bitMapAspect));
				}
				else
				{
					// Fit to window height
					drawToRect = new Rectangle(clientRectMax.X,clientRectMax.Y,
						(int)((float)(clientRectMax.Height)*bitMapAspect),clientRectMax.Height);
				}

				e.Graphics.DrawImage(m_bitmap, drawToRect);
			}
		}

		private Bitmap MakeBitmapFromImageRaw(AvigilonDotNet.IFrameImageRaw rawFrame)
		{
			Rectangle imageRect = new Rectangle(0, 0, rawFrame.Size.Width, rawFrame.Size.Height);

			switch (rawFrame.DefaultPixelFormat)
			{
				case AvigilonDotNet.PixelFormat.Gray8:
					{
						Bitmap retVal = new Bitmap(
							rawFrame.Size.Width,
							rawFrame.Size.Height,
							System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

						// Define palette for 8bit grayscale
						System.Drawing.Imaging.ColorPalette pal = retVal.Palette;
						for (int ix = 0; ix < pal.Entries.Length; ++ix)
						{
							pal.Entries[ix] = System.Drawing.Color.FromArgb(ix, ix, ix);
						}
						retVal.Palette = pal;

						System.Drawing.Imaging.BitmapData bitmapData = retVal.LockBits(
							imageRect,
							System.Drawing.Imaging.ImageLockMode.WriteOnly,
							System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

						byte[] byteArray = rawFrame.GetAsArray(
							AvigilonDotNet.PixelFormat.Gray8,
							(ushort)bitmapData.Stride);

						System.Runtime.InteropServices.Marshal.Copy(
							byteArray,
							0,
							bitmapData.Scan0,
							byteArray.Length);
						retVal.UnlockBits(bitmapData);

						return retVal;
					}

				case AvigilonDotNet.PixelFormat.RGB24:
					{
						Bitmap retVal = new Bitmap(
							rawFrame.Size.Width,
							rawFrame.Size.Height,
							System.Drawing.Imaging.PixelFormat.Format24bppRgb);

						System.Drawing.Imaging.BitmapData bitmapData = retVal.LockBits(
							imageRect,
							System.Drawing.Imaging.ImageLockMode.WriteOnly,
							System.Drawing.Imaging.PixelFormat.Format24bppRgb);

						byte[] byteArray = rawFrame.GetAsArray(
							AvigilonDotNet.PixelFormat.RGB24,
							(ushort)bitmapData.Stride);

						System.Runtime.InteropServices.Marshal.Copy(
							byteArray,
							0,
							bitmapData.Scan0,
							byteArray.Length);
						retVal.UnlockBits(bitmapData);

						return retVal;
					}

				case AvigilonDotNet.PixelFormat.RGB32:
					{
						Bitmap retVal = new Bitmap(
							rawFrame.Size.Width,
							rawFrame.Size.Height,
							System.Drawing.Imaging.PixelFormat.Format32bppRgb);

						System.Drawing.Imaging.BitmapData bitmapData = retVal.LockBits(
							imageRect,
							System.Drawing.Imaging.ImageLockMode.WriteOnly,
							System.Drawing.Imaging.PixelFormat.Format32bppRgb);

						byte[] byteArray = rawFrame.GetAsArray(
							AvigilonDotNet.PixelFormat.RGB32,
							(ushort)bitmapData.Stride);

						System.Runtime.InteropServices.Marshal.Copy(
							byteArray,
							0,
							bitmapData.Scan0,
							byteArray.Length);
						retVal.UnlockBits(bitmapData);

						return retVal;
					}

				default:
					return null;
			}
		}

		private AvigilonDotNet.IFrameImage	m_frameImage;
		private Bitmap m_bitmap;
	}
}
