/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AvigilonDotNet;

namespace DemoApp
{
	public class TextInputForm
		: System.Windows.Forms.Form
	{
		private int PADDING = 5;

		public TextInputForm()
		{
			this.ClientSize = new System.Drawing.Size(250, 100); 

			// Initialize the form
			m_textInputLabel = new System.Windows.Forms.Label();
			m_textInputLabel.Text = "Value:";
			m_textInputLabel.AutoSize = true;
			Controls.Add(m_textInputLabel);

			m_textInputTextBox = new System.Windows.Forms.TextBox();
			m_textInputTextBox.Width = 150;
			m_textInputTextBox.Text = "";
			Controls.Add(m_textInputTextBox);
			
			m_okButton = new System.Windows.Forms.Button();
			m_okButton.Text = "OK";
			m_okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			Controls.Add(m_okButton);

			m_cancelButton = new System.Windows.Forms.Button();
			m_cancelButton.Text = "Cancel";
			m_cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			Controls.Add(m_cancelButton);

			m_textInputLabel.Location = new System.Drawing.Point(PADDING, PADDING);
			m_textInputTextBox.Location = new System.Drawing.Point(m_textInputLabel.Right + PADDING, PADDING);
			m_textInputTextBox.Width = ClientSize.Width - m_textInputLabel.Right - PADDING;

			m_cancelButton.Location = new System.Drawing.Point(ClientSize.Width - m_cancelButton.Width - PADDING, m_textInputTextBox.Bottom + PADDING);
			m_okButton.Location = new System.Drawing.Point(m_cancelButton.Left - m_okButton.Width - PADDING, m_textInputTextBox.Bottom + PADDING);

			Text = "Enter Value:";
			FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			AcceptButton = m_okButton;
			CancelButton = m_cancelButton;
		}

		public string TextInput
		{
			get { return m_textInputTextBox.Text; }
		}
		
		private System.Windows.Forms.Label m_textInputLabel;
		private System.Windows.Forms.TextBox m_textInputTextBox;
		private System.Windows.Forms.Button m_okButton;
		private System.Windows.Forms.Button m_cancelButton;

	}

	/// <summary>
	/// The is the main form for the Avigilon Sdk Test application
	/// </summary>
	public class DemoAppForm
		: System.Windows.Forms.Form
	{
		/// <summary>
		/// Constructor
		/// </summary>
		public DemoAppForm(AvigilonDotNet.IAvigilonControlCenter acc,
			bool bDisposeAcc)
		{
			m_controlCenter = acc;
			m_disposeAcc = bDisposeAcc;

			if (m_controlCenter == null)
			{
				MessageBox.Show("Null control center");
				Close();
			}

			InitializeComponents_();

			Text = "Avigilon Control Center .NET SDK Demo App";
			Icon = Resources.App;
			StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;

			this.Layout += OnLayout_;
			this.FormClosing += OnClose_;
		}

		/// <summary>
		/// Form closing event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClose_(
			System.Object sender,
			System.Windows.Forms.FormClosingEventArgs e)
		{
			// Remote reference to m_controlCenter in the tree view
			m_avigilonTreeView.ControlCenter = null;
			foreach (DisplayPanel dp in m_displayPanels)
			{
				dp.Close();
			}
			m_controlCenter = null;
		}

		/// <summary>
		/// Form layout event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLayout_(
			System.Object sender,
			System.Windows.Forms.LayoutEventArgs e)
		{
			LayoutControls_();
		}

		/// <summary>
		/// Avigilon Tree view display device event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="device">Device to be displayed</param>
		private void OnDisplayEntity_(
			System.Object sender,
			AvigilonDotNet.IEntity entity)
		{
			CreateStream_(entity, ref m_displayPanels[m_imagePanelCount % NUM_IMAGE_PANELS]);
			++m_imagePanelCount;
		}

		/// <summary>
		/// Avigilon Tree view export device event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="device">Device to be exported</param>
		private void OnExportEntity_(
			System.Object sender,
			AvigilonDotNet.IEntity entity)
		{
			// Can only stream cameras
			if (entity == null ||
				entity.Type != EntityType.Camera)
				return;

			//Get the export parameters and the perform the export
			ExportParamForm ep = new ExportParamForm();
			if (ep.ShowDialog() == DialogResult.OK)
			{
				AvigilonDotNet.IExport exportInf = null;
				AvgError error = m_controlCenter.CreateExport(
					ep.ExportParams,
					(AvigilonDotNet.IEntityCamera)entity,
					ep.StartTime,
					ep.EndTime,
					ep.FileName,
					out exportInf);
				if (ErrorHelper.IsError(error))
				{
					MessageBox.Show("Failed to create export: " + error.ToString());
				}
				else
				{
					ExportForm ef = new ExportForm(exportInf);
					ef.ShowDialog();
				}
			}
		}

		private void OnImageQueryEntity_(
			System.Object sender,
			AvigilonDotNet.IEntity entity)
		{
			AvigilonDotNet.IRequestor requestor = m_controlCenter.CreateRequestor();
			RequestParams requestParams = new RequestParams();
			requestParams.DeviceId = entity.DeviceId;
			requestParams.NvrUuid = entity.ReportingNvrUuid;
			requestParams.DataStoreUuid = entity.ReportingDataStoreUuid;

			ImageQueryForm fqf = new ImageQueryForm(
				requestParams,
				requestor);
			fqf.ShowDialog();
		}

		private void OnBackupNvr_(System.Object sender,
			AvigilonDotNet.INvr nvr)
		{
			if (nvr == null)
				return;

			//Get the export parameters and the perform the export
			BackupParamForm bp = new BackupParamForm(nvr);
			if (bp.ShowDialog() == DialogResult.OK)
			{
				AvigilonDotNet.IBackup backupInf = m_controlCenter.CreateBackup(
					nvr.NvrUuid,
					bp.DeviceIds,
					bp.StartTime,
					bp.EndTime,
					bp.DeleteIfReq);
				if (backupInf != null)
				{
					BackupForm ef = new BackupForm(backupInf);
					ef.ShowDialog();
				}
			}
		}

		/// <summary>
		/// Live to recorded event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void OnLiveRecordedChange_(
			System.Object sender,
			System.EventArgs e)
		{
			DefineStreamGroup();
			for (int ix = 0; ix < m_displayPanels.Length; ++ix)
			{
				CreateStream_(m_displayPanels[ix].streamEntity, ref m_displayPanels[ix]);
			}
		}

		/// <summary>
		/// Creates a live stream with the specified device
		/// </summary>
		/// <param name="device">Device to create a live stream</param>
		private void CreateStream_(AvigilonDotNet.IEntity entity,
			ref DisplayPanel panel)
		{
			if (entity == null || panel == null)
				return;

			// Can only stream cameras
			if (entity.Type != EntityType.Camera)
				return;

			// Must dispose of previous stream before creating a new one
			if (panel.stream != null)
			{
				panel.stream = null;
			}

			// Display video
			AvgError error = m_controlCenter.CreateStreamWindow(
				(AvigilonDotNet.IEntityCamera)entity,
				m_streamGroup,
				out panel.stream);
			if (ErrorHelper.IsSuccess(error))
			{
				// Apply current overlays
				panel.stream.Overlays = CurrentOverlays;

				// Attach stream to image panel and enable
				panel.streamEntity = entity;
				panel.imagePanel.Stream = panel.stream;
				panel.stream.Enable = true;
			}
			else
			{
				IFileArchive fileArchive = m_controlCenter.GetFileArchive(entity.ParentDevice.ReportingDataStoreUuid);
				if ((fileArchive != null) &&
					(m_streamGroup.Mode == PlaybackMode.Live))
				{
					MessageBox.Show(entity.Name + " does not support Live playback mode.");
				}
				else
				{
					MessageBox.Show("Failed to create stream for " + entity.Name);
				}
			}
		}

		/// <summary>
		/// Play button clicked event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPlay_(
			System.Object sender,
			System.EventArgs e)
		{
			if (m_streamGroup != null)
				m_streamGroup.Play();
		}

		/// <summary>
		/// Pause button clicked event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPause_(
			System.Object sender,
			System.EventArgs e)
		{
			if (m_streamGroup != null)
				m_streamGroup.Pause();
		}

		/// <summary>
		/// Image advance button clicked event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFAdv_(
			System.Object sender,
			System.EventArgs e)
		{
			if (m_streamGroup != null)
				m_streamGroup.Step(true);
		}

		/// <summary>
		/// Image back button clicked event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFBack_(
			System.Object sender,
			System.EventArgs e)
		{
			if (m_streamGroup != null)
				m_streamGroup.Step(false);
		}

		private void OnBoundTimesButton_(
			System.Object sender,
			System.EventArgs e)
		{
			if (m_displayPanels[0].stream != null)
			{
				System.DateTime st;
				if (!m_displayPanels[0].stream.AdjustPlaybackStartTime(m_startTimePicker.Value, out st))
				{
					MessageBox.Show("Failed to find appropriate start time");
					return;
				}

				System.DateTime et;
				if (!m_displayPanels[0].stream.AdjustPlaybackEndTime(m_endTimePicker.Value, out et))
				{
					MessageBox.Show("Failed to find appropriate end time");
					return;
				}

				if (et < st)
				{
					MessageBox.Show("End time < start time");
					return;
				}

				m_startTimePicker.Value = DateOps.ClampToPicker(st);
				m_endTimePicker.Value = DateOps.ClampToPicker(et);
			}
		}

		private void OnChangeMaxRx_(
			System.Object sender,
			System.EventArgs e)
		{
			m_controlCenter.SystemLimitedRxDataRate = (System.UInt64)m_maxRxDataRate.Value;
		}

		private void OnDecompBiasChange_(
			System.Object sender,
			System.EventArgs e)
		{
			m_controlCenter.DecompressionBias = m_decompBias.Value;
		}

		private void OnDeinterlacingChange_(
			System.Object sender,
			System.EventArgs e)
		{
			m_controlCenter.Deinterlacing = m_deinterlacing.Checked;
		}

		private void OnMonochromeChange_(
			System.Object sender,
			System.EventArgs e)
		{
			m_controlCenter.Monochrome = m_monochrome.Checked;
		}

		/// <summary>
		/// Overlay check state change event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OverlaysChecked_(
			System.Object sender,
			System.EventArgs e)
		{
			foreach (DisplayPanel panel in m_displayPanels)
			{
				panel.imagePanel.Overlays = CurrentOverlays;
			}
		}

		public AvigilonDotNet.Overlay CurrentOverlays
		{
			get
			{
				AvigilonDotNet.Overlay overlayFlags = 0;
				System.Collections.Generic.List<AvigilonDotNet.Overlay> overList = m_overlaysSelector.Selection;
				foreach (AvigilonDotNet.Overlay overlay in overList)
				{
					overlayFlags |= overlay;
				}
				return overlayFlags;
			}
		}

		/// <summary>
		/// Bound time checkbox checked event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void BoundTimesChanged_(
			System.Object sender,
			System.EventArgs e)
		{
			m_startTimePicker.Enabled = m_boundTimes.Checked;
			m_endTimePicker.Enabled = m_boundTimes.Checked;

			if (m_boundTimes.Checked)
			{
				m_streamGroup.SetCurrentPositionWithBounds(
					m_startTimePicker.Value,
					m_endTimePicker.Value,
					m_currentTimePicker.Value);
			}
			else
			{
				m_streamGroup.SetCurrentPositionWithBounds(
					DateTime.MinValue,
					DateTime.MaxValue,
					m_currentTimePicker.Value);
			}
		}

		/// <summary>
		/// Set time button clicked event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void SetTimeClick_(
			System.Object sender,
			System.EventArgs e)
		{
			if (m_streamGroup != null)
			{
				if (m_boundTimes.Checked)
				{
					m_streamGroup.SetCurrentPositionWithBounds(
						m_startTimePicker.Value,
						m_endTimePicker.Value,
						m_currentTimePicker.Value);
				}
				else
				{
					m_streamGroup.SetCurrentPosition(m_currentTimePicker.Value);
				}
			}
		}

		/// <summary>
		/// Playback speed scroll moved event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void SpeedTrackerScroll_(
			System.Object sender,
			System.EventArgs e)
		{
			SetSpeed_();
		}

		private void VolumeTrackerScroll_(
			System.Object sender,
			System.EventArgs e)
		{
			SetVolume_();
		}

		private void DefineStreamGroup()
		{
			if (m_streamGroup != null)
			{
				m_streamGroup = null;
			}
			if (m_livePlaybackCombo.SelectedItem.ToString() == "Live")
			{
				m_streamGroup = m_controlCenter.CreateStreamGroup(PlaybackMode.Live);
			}
			else
			{
				m_streamGroup = m_controlCenter.CreateStreamGroup(PlaybackMode.Recorded);
				SetSpeed_();
			}
		}

		/// <summary>
		/// Applies the current speed parameters onto the stream.
		/// </summary>
		private void SetSpeed_()
		{
			double speedVal;
			switch (m_speedTrackBar.Value)
			{
				case 0:
					speedVal = -8.0;
					m_speedReadout.Text = "-8"; break;
				case 1:
					speedVal = -4.0;
					m_speedReadout.Text = "-4"; break;
				case 2:
					speedVal = -2.0;
					m_speedReadout.Text = "-2"; break;
				case 3:
					speedVal = -1.0;
					m_speedReadout.Text = "-1"; break;
				case 4:
					speedVal = -0.5;
					m_speedReadout.Text = "-1/2"; break;
				case 5:
					speedVal = -0.25;
					m_speedReadout.Text = "-1/4"; break;
				case 6:
					speedVal = 0.25;
					m_speedReadout.Text = "1/4"; break;
				case 7:
					speedVal = 0.5;
					m_speedReadout.Text = "1/2"; break;
				case 8:
					speedVal = 1;
					m_speedReadout.Text = "1"; break;
				case 9:
					speedVal = 2;
					m_speedReadout.Text = "2"; break;
				case 10:
					speedVal = 4;
					m_speedReadout.Text = "4"; break;
				case 11:
					speedVal = 8;
					m_speedReadout.Text = "8"; break;
				default:
					m_speedReadout.Text = "???";
					return;
			}

			if (m_streamGroup != null)
				m_streamGroup.Speed = speedVal;
		}

		private void SetVolume_()
		{
			float val = m_volumeTrackBar.Value;
			float total = m_volumeTrackBar.Maximum;
			float percent = val / total;
			m_controlCenter.GlobalAudioVolume = percent;
		}

		/// <summary>
		/// Callback for adding NVRs to the system.
		/// </summary>
		private void OnAddNvr_(System.Object sender, System.EventArgs e)
		{
			AddNvrForm form = new AddNvrForm(m_controlCenter.DefaultNvrPortNumber);
			if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				System.Net.IPAddress address;
				int port = form.Port;

				if (!System.Net.IPAddress.TryParse(form.Address, out address))
				{
					System.Windows.Forms.MessageBox.Show("IP Address is invalid");
					return;
				}

				AvigilonDotNet.AvgError result = m_controlCenter.AddNvr(
					new System.Net.IPEndPoint(address, port));

				if (AvigilonDotNet.ErrorHelper.IsError(result))
				{
					System.Windows.Forms.MessageBox.Show(
						"An error occurred while adding the NVR.");
				}
			}
		}

		/// <summary>
		/// Callback for adding NVRs to the system.
		/// </summary>
		private void OnContinueQuery_(System.Object sender, System.EventArgs e)
		{
			TextInputForm tif = new TextInputForm();
			if (tif.ShowDialog() == DialogResult.OK)
			{
				IQueryContinuable continueQuery;
				AvgError error = m_controlCenter.QueryContinue(tif.TextInput, out continueQuery);
				if (AvigilonDotNet.ErrorHelper.IsError(error))
				{
					System.Windows.Forms.MessageBox.Show("Unable to continue query: " + error.ToString());
				}
				else
				{
					QueryForm qf = new QueryForm(continueQuery);
					qf.Show();
				}
			}
		}

		/// <summary>
		/// Callback for loading backups
		/// </summary>
		private void OnLoadArchive_(System.Object sender, System.EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();
			ofd.Filter = "Avigilon Backups (*.avk)|*.avk|Avigilon Native Export (*.ave)|*.ave";

			if (ofd.ShowDialog() == DialogResult.OK)
			{
				// Load the file
				System.String dataStoreUuid;
				m_controlCenter.OpenFileArchive(ofd.FileName, out dataStoreUuid);
			}
		}

		/// <summary>
		/// Callback for closing backups
		/// </summary>
		private void OnCloseArchives_(System.Object sender, System.EventArgs e)
		{
			m_controlCenter.CloseAllFileArchives();
		}

		/// <summary>
		/// Called to initialize the controls on the form
		/// </summary>
		private void InitializeComponents_()
		{
			this.Size = new System.Drawing.Size(1000, 480);

			// Create image panels
			int totalDisplayPanels = NUM_IMAGE_PANELS;
			m_displayPanels = new DisplayPanel[totalDisplayPanels];
			for (int ix = 0; ix < totalDisplayPanels; ++ix)
			{
				m_displayPanels[ix] = new DisplayPanel();
				m_displayPanels[ix].imagePanel.ImagePanelClick += new EventHandler(OnImagePanelClick_);
				Controls.Add(m_displayPanels[ix].imagePanel);
			}

			m_avigilonTreeView = new TreeViewAvigilon();
			m_avigilonTreeView.DisplayEntity += new EntityHandler(OnDisplayEntity_);
			m_avigilonTreeView.ExportEntity += new EntityHandler(OnExportEntity_);
			m_avigilonTreeView.ImageQueryEntity += new EntityHandler(OnImageQueryEntity_);
			m_avigilonTreeView.BackupNvr += new NvrHandler(OnBackupNvr_);
			m_avigilonTreeView.ControlCenter = m_controlCenter;
			Controls.Add(m_avigilonTreeView);

			m_addNvr = new System.Windows.Forms.Button();
			m_addNvr.Text = "Add Site";
			m_addNvr.Click += OnAddNvr_;
			Controls.Add(m_addNvr);

			m_continueQuery = new System.Windows.Forms.Button();
			m_continueQuery.Text = "Continue Query";
			m_continueQuery.Click += OnContinueQuery_;
			Controls.Add(m_continueQuery);

			m_loadArchive = new System.Windows.Forms.Button();
			m_loadArchive.Text = "Load Archive";
			m_loadArchive.AutoSize = true;
			m_loadArchive.Click += OnLoadArchive_;
			Controls.Add(m_loadArchive);

			m_closeArchives = new System.Windows.Forms.Button();
			m_closeArchives.Text = "Close Archives";
			m_closeArchives.AutoSize = true;
			m_closeArchives.Click += OnCloseArchives_;
			Controls.Add(m_closeArchives);

			m_nvrGroupBox = new System.Windows.Forms.GroupBox();
			m_nvrGroupBox.Text = "NVRs and Devices";
			Controls.Add(m_nvrGroupBox);

			m_maxRxLabel = new Label();
			m_maxRxLabel.Text = "Max Data Rate:";
			m_maxRxLabel.AutoSize = true;
			Controls.Add(m_maxRxLabel);

			m_maxRxDataRate = new System.Windows.Forms.NumericUpDown();
			m_maxRxDataRate.Minimum = 0;
			m_maxRxDataRate.Maximum = 125000000;
			m_maxRxDataRate.Value = m_controlCenter.SystemLimitedRxDataRate;
			m_maxRxDataRate.ValueChanged += OnChangeMaxRx_;
			Controls.Add(m_maxRxDataRate);

			m_decompBias = new ImageQualitySelector();
			m_decompBias.Value = m_controlCenter.DecompressionBias;
			m_decompBias.ValueChanged += OnDecompBiasChange_;
			Controls.Add(m_decompBias);

			m_decompBiasLabel = new System.Windows.Forms.Label();
			m_decompBiasLabel.Text = "Image Quality:";
			m_decompBiasLabel.AutoSize = true;
			Controls.Add(m_decompBiasLabel);

			m_deinterlacing = new System.Windows.Forms.CheckBox();
			m_deinterlacing.Text = "Deinterlace Images";
			m_deinterlacing.Checked = m_controlCenter.Deinterlacing;
			m_deinterlacing.CheckedChanged += OnDeinterlacingChange_;
			m_deinterlacing.AutoSize = true;
			Controls.Add(m_deinterlacing);

			m_volLabel = new Label();
			m_volLabel.Text = "Volume:";
			m_volLabel.AutoSize = true;
			Controls.Add(m_volLabel);

			m_monochrome = new System.Windows.Forms.CheckBox();
			m_monochrome.Text = "Monochrome Display";
			m_monochrome.Checked = m_controlCenter.Monochrome;
			m_monochrome.CheckedChanged += OnMonochromeChange_;
			m_monochrome.AutoSize = true;
			Controls.Add(m_monochrome);

			m_volumeTrackBar = new System.Windows.Forms.TrackBar();
			m_volumeTrackBar.Minimum = 0;
			m_volumeTrackBar.Maximum = 100;
			m_volumeTrackBar.SmallChange = 1;
			m_volumeTrackBar.LargeChange = 1;
			m_volumeTrackBar.Value = 0;
			m_volumeTrackBar.AutoSize = false;
			m_volumeTrackBar.Height = 25;
			m_volumeTrackBar.Scroll += new System.EventHandler(VolumeTrackerScroll_);
			Controls.Add(m_volumeTrackBar);

			m_settingsGroupBox = new System.Windows.Forms.GroupBox();
			m_settingsGroupBox.Text = "Global Settings";
			Controls.Add(m_settingsGroupBox);

			m_livePlaybackCombo = new System.Windows.Forms.ComboBox();
			m_livePlaybackCombo.Width = 120;
			m_livePlaybackCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			m_livePlaybackCombo.Items.Add("Live");
			m_livePlaybackCombo.Items.Add("Playback");
			m_livePlaybackCombo.SelectedIndex = 0;
			m_livePlaybackCombo.SelectionChangeCommitted += OnLiveRecordedChange_;
			Controls.Add(m_livePlaybackCombo);

			m_playButton = new System.Windows.Forms.Button();
			m_playButton.Text = "Play";
			m_playButton.Click += new System.EventHandler(OnPlay_);
			Controls.Add(m_playButton);

			m_pauseButton = new System.Windows.Forms.Button();
			m_pauseButton.Text = "Pause";
			m_pauseButton.Click += new System.EventHandler(OnPause_);
			Controls.Add(m_pauseButton);

			m_stepFwdButton = new System.Windows.Forms.Button();
			m_stepFwdButton.Text = "Step Fwd";
			m_stepFwdButton.Click += new System.EventHandler(OnFAdv_);
			Controls.Add(m_stepFwdButton);

			m_stepBackButton = new System.Windows.Forms.Button();
			m_stepBackButton.Text = "Step Back";
			m_stepBackButton.Click += new System.EventHandler(OnFBack_);
			Controls.Add(m_stepBackButton);

			m_boundTimesButton = new System.Windows.Forms.Button();
			m_boundTimesButton.Text = "Bound ip1";
			m_boundTimesButton.Click += new System.EventHandler(OnBoundTimesButton_);
			Controls.Add(m_boundTimesButton);

			List<AvigilonDotNet.Overlay> ignoredOverlays = new List<Overlay>();
			ignoredOverlays.Add(AvigilonDotNet.Overlay.None);
			m_overlaysSelector = new EnumListSelector<AvigilonDotNet.Overlay>(ignoredOverlays);
			m_overlaysSelector.Size = new System.Drawing.Size(120, 70);
			m_overlaysSelector.ValueChanged += OverlaysChecked_;
			Controls.Add(m_overlaysSelector);

			m_boundTimes = new System.Windows.Forms.CheckBox();
			m_boundTimes.Text = "Bound Time";
			m_boundTimes.AutoSize = true;
			m_boundTimes.CheckedChanged += new System.EventHandler(BoundTimesChanged_);
			Controls.Add(m_boundTimes);

			m_startTimeLabel = new System.Windows.Forms.Label();
			m_startTimeLabel.Text = "Start:";
			m_startTimeLabel.AutoSize = true;
			Controls.Add(m_startTimeLabel);

			m_startTimePicker = new System.Windows.Forms.DateTimePicker();
			m_startTimePicker.Format = DateTimePickerFormat.Custom;
			m_startTimePicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_startTimePicker.Width = 145;
			m_startTimePicker.Enabled = m_boundTimes.Checked;
			Controls.Add(m_startTimePicker);

			m_currentTimeLabel = new System.Windows.Forms.Label();
			m_currentTimeLabel.Text = "Current:";
			m_currentTimeLabel.AutoSize = true;
			Controls.Add(m_currentTimeLabel);

			m_currentTimePicker = new System.Windows.Forms.DateTimePicker();
			m_currentTimePicker.Format = DateTimePickerFormat.Custom;
			m_currentTimePicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_currentTimePicker.Width = 145;
			Controls.Add(m_currentTimePicker);

			m_endTimeLabel = new System.Windows.Forms.Label();
			m_endTimeLabel.Text = "End:";
			m_endTimeLabel.AutoSize = true;
			Controls.Add(m_endTimeLabel);

			m_endTimePicker = new System.Windows.Forms.DateTimePicker();
			m_endTimePicker.Format = DateTimePickerFormat.Custom;
			m_endTimePicker.CustomFormat = "MMM dd yyyy HH:mm:ss";
			m_endTimePicker.Width = 145;
			m_endTimePicker.Enabled = m_boundTimes.Checked;
			Controls.Add(m_endTimePicker);

			m_setTime = new System.Windows.Forms.Button();
			m_setTime.Text = "Set Time";
			m_setTime.Click += new System.EventHandler(SetTimeClick_);
			Controls.Add(m_setTime);

			m_speedTrackBar = new System.Windows.Forms.TrackBar();
			m_speedTrackBar.Minimum = 0;
			m_speedTrackBar.Maximum = 11;
			m_speedTrackBar.SmallChange = 1;
			m_speedTrackBar.LargeChange = 1;
			m_speedTrackBar.Value = 8;
			m_speedTrackBar.AutoSize = false;
			m_speedTrackBar.Height = 25;
			m_speedTrackBar.Scroll += new System.EventHandler(SpeedTrackerScroll_);
			Controls.Add(m_speedTrackBar);

			m_speedReadout = new System.Windows.Forms.Label();
			m_speedReadout.AutoSize = true;
			Controls.Add(m_speedReadout);

			m_playbackGroupBox = new System.Windows.Forms.GroupBox();
			m_playbackGroupBox.Text = "Playback";
			Controls.Add(m_playbackGroupBox);

			DefineStreamGroup();
			SetVolume_();
		}

		void OnImagePanelClick_(object sender, EventArgs e)
		{
			AvigilonDotNet.ImagePanel ip = (AvigilonDotNet.ImagePanel)sender;
			if (ip != null &&
				ip.StreamWindow != null)
			{
				m_streamGroup.FocusedStream = ip.StreamWindow;
			}
		}

		/// <summary>
		/// Positions the controls on the form
		/// </summary>
		private void LayoutControls_()
		{
			System.Drawing.Rectangle clientRect = this.ClientRectangle;

			int imgPanelWidth = (clientRect.Width - 20 - 250) / NUM_IMAGE_PANELS_SIDE;
			int imgPanelHeight = (clientRect.Height - 145) / NUM_IMAGE_PANELS_SIDE;

			int row = 0;
			int col = 0;
			for (int ix = 0; ix < NUM_IMAGE_PANELS; ++ix)
			{
				row = ix / NUM_IMAGE_PANELS_SIDE;
				col = ix % NUM_IMAGE_PANELS_SIDE;

				m_displayPanels[ix].imagePanel.Location = new System.Drawing.Point(260 + (col * imgPanelWidth), 10 + (row * imgPanelHeight));
				m_displayPanels[ix].imagePanel.Size = new System.Drawing.Size(imgPanelWidth, imgPanelHeight);
			}

			const int globalIqHeight = 210;

			m_settingsGroupBox.Location = new System.Drawing.Point(5, clientRect.Bottom + 10 - globalIqHeight);
			m_settingsGroupBox.Size = new System.Drawing.Size(250, clientRect.Bottom - 5 - m_settingsGroupBox.Top);

			m_maxRxLabel.Location = new System.Drawing.Point(10, clientRect.Bottom + 25 - globalIqHeight);
			m_maxRxDataRate.Location = new System.Drawing.Point(100, clientRect.Bottom + 25 - globalIqHeight);
			m_decompBiasLabel.Location = new System.Drawing.Point(10, clientRect.Bottom + 55 - globalIqHeight);
			m_decompBias.Location = new System.Drawing.Point(100, clientRect.Bottom + 55 - globalIqHeight);
			m_deinterlacing.Location = new System.Drawing.Point(100, clientRect.Bottom + 135 - globalIqHeight);
			m_monochrome.Location = new System.Drawing.Point(100, clientRect.Bottom + 155 - globalIqHeight);
			m_volLabel.Location = new System.Drawing.Point(10, clientRect.Bottom + 175 - globalIqHeight);
			m_volumeTrackBar.Location = new System.Drawing.Point(100, clientRect.Bottom + 175 - globalIqHeight);

			m_addNvr.Location = new System.Drawing.Point(5, m_settingsGroupBox.Top - 5 - m_addNvr.Height);
			m_addNvr.Width = m_settingsGroupBox.Width / 2 - 5;
			m_continueQuery.Location = new System.Drawing.Point(m_addNvr.Right + 5, m_settingsGroupBox.Top - 5 - m_continueQuery.Height);
			m_continueQuery.Width = m_settingsGroupBox.Width / 2 - 5;
			m_loadArchive.Location = new System.Drawing.Point(5, m_addNvr.Top - 5 - m_loadArchive.Height);
			m_loadArchive.Width = m_settingsGroupBox.Width / 2 - 5;
			m_closeArchives.Location = new System.Drawing.Point(m_loadArchive.Right + 5, m_addNvr.Top - 5 - m_loadArchive.Height);
			m_closeArchives.Width = m_settingsGroupBox.Width / 2 - 5;

			m_nvrGroupBox.Location = new System.Drawing.Point(5, 5);
			m_nvrGroupBox.Width = m_settingsGroupBox.Width;
			m_nvrGroupBox.Height = m_loadArchive.Top - 5 - 5;
			m_avigilonTreeView.Location = new System.Drawing.Point(10, 20);
			m_avigilonTreeView.Width = m_nvrGroupBox.Width - 10;
			m_avigilonTreeView.Height = m_nvrGroupBox.Height - 20; //15 top, 5 bottom

			m_playbackGroupBox.Location = new System.Drawing.Point(260, clientRect.Height - 130);
			m_playbackGroupBox.Size = new System.Drawing.Size(clientRect.Width - 275, 125);

			m_livePlaybackCombo.Location = new System.Drawing.Point(270, clientRect.Height - 110);
			m_playButton.Location = new System.Drawing.Point(400, clientRect.Height - 110);
			m_pauseButton.Location = new System.Drawing.Point(480, clientRect.Height - 110);
			m_stepFwdButton.Location = new System.Drawing.Point(560, clientRect.Height - 110);
			m_stepBackButton.Location = new System.Drawing.Point(640, clientRect.Height - 110);
			m_boundTimesButton.Location = new System.Drawing.Point(720, clientRect.Height - 110);

			m_overlaysSelector.Location = new System.Drawing.Point(270, clientRect.Height - 80);

			m_startTimeLabel.Location = new System.Drawing.Point(400, clientRect.Height - 80);
			m_startTimePicker.Location = new System.Drawing.Point(400, clientRect.Height - 65);
			m_currentTimeLabel.Location = new System.Drawing.Point(550, clientRect.Height - 80);
			m_currentTimePicker.Location = new System.Drawing.Point(550, clientRect.Height - 65);
			m_endTimeLabel.Location = new System.Drawing.Point(700, clientRect.Height - 80);
			m_endTimePicker.Location = new System.Drawing.Point(700, clientRect.Height - 65);

			m_boundTimes.Location = new System.Drawing.Point(400, clientRect.Height - 40);
			m_setTime.Location = new System.Drawing.Point(500, clientRect.Height - 40);

			m_speedTrackBar.Location = new System.Drawing.Point(580, clientRect.Height - 40);
			m_speedReadout.Location = new System.Drawing.Point(720, clientRect.Height - 40);
		}


		private AvigilonDotNet.IAvigilonControlCenter m_controlCenter;
		private bool m_disposeAcc;
		private AvigilonDotNet.IStreamGroup m_streamGroup;


		private const int NUM_IMAGE_PANELS_SIDE = 2; // actual number is ^2
		private const int NUM_IMAGE_PANELS = NUM_IMAGE_PANELS_SIDE * NUM_IMAGE_PANELS_SIDE;
		private int m_imagePanelCount = 0;
		private class DisplayPanel
		{
			public DisplayPanel()
			{
				imagePanel = new ImagePanelAndToolbar();
				imagePanel.ImagePanelClose += OnImagePanelClose_;
			}

			public void Close()
			{
				OnImagePanelClose_();
			}

			public ImagePanelAndToolbar imagePanel;
			public AvigilonDotNet.IStreamWindow stream = null;
			public AvigilonDotNet.IEntity streamEntity = null;

			void OnImagePanelClose_()
			{
				// Close the image panel                
				streamEntity = null;                
				imagePanel.Stream = null;

				if (stream != null)
				{
					stream.Dispose();
					stream = null;           
				}                              
			}
		}
		private DisplayPanel[] m_displayPanels;

		private System.Windows.Forms.GroupBox m_nvrGroupBox;
		private TreeViewAvigilon m_avigilonTreeView; // avigilon tree view
		private System.Windows.Forms.Button m_addNvr;
		private System.Windows.Forms.Button m_continueQuery;
		private System.Windows.Forms.Button m_loadArchive;
		private System.Windows.Forms.Button m_closeArchives;
		private System.Windows.Forms.GroupBox m_settingsGroupBox;
		private System.Windows.Forms.NumericUpDown m_maxRxDataRate;
		private System.Windows.Forms.Label m_maxRxLabel;
		private System.Windows.Forms.Label m_decompBiasLabel;
		private ImageQualitySelector m_decompBias;
		private System.Windows.Forms.CheckBox m_deinterlacing;
		private System.Windows.Forms.CheckBox m_monochrome;
		private System.Windows.Forms.Label m_volLabel;
		private System.Windows.Forms.TrackBar m_volumeTrackBar;

		private System.Windows.Forms.ComboBox m_livePlaybackCombo;
		private System.Windows.Forms.Button m_playButton;
		private System.Windows.Forms.Button m_pauseButton;
		private System.Windows.Forms.Button m_stepFwdButton;
		private System.Windows.Forms.Button m_stepBackButton;
		private System.Windows.Forms.Button m_boundTimesButton;

		private EnumListSelector<AvigilonDotNet.Overlay> m_overlaysSelector;

		private System.Windows.Forms.Label m_startTimeLabel;
		private System.Windows.Forms.DateTimePicker m_startTimePicker;
		private System.Windows.Forms.Label m_currentTimeLabel;
		private System.Windows.Forms.DateTimePicker m_currentTimePicker;
		private System.Windows.Forms.Label m_endTimeLabel;
		private System.Windows.Forms.DateTimePicker m_endTimePicker;

		private System.Windows.Forms.CheckBox m_boundTimes;
		private System.Windows.Forms.Button m_setTime;

		private System.Windows.Forms.TrackBar m_speedTrackBar;
		private System.Windows.Forms.Label m_speedReadout;

		private System.Windows.Forms.GroupBox m_playbackGroupBox;
	}
}
