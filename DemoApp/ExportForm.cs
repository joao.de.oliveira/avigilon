/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Text;

namespace DemoApp
{
	/// <summary>
	/// Displays the current progress of an export operation
	/// </summary>
	class ExportForm
		: System.Windows.Forms.Form
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="export">The export operation to monitor</param>
		public ExportForm(AvigilonDotNet.IExport export)
		{
			m_export = export;
			m_export.ProgressChanged += new AvigilonDotNet.ExportProgressHandler(ExportChange_);

			Size = new System.Drawing.Size(460, 150);
			Text = "Exporting ";
			StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;

			m_progress = new System.Windows.Forms.ProgressBar();
			m_progress.Location = new System.Drawing.Point(10, 10);
			m_progress.Width = 430;
			m_progress.Minimum = 0;
			m_progress.Maximum = 100;
			Controls.Add(m_progress);

			System.Windows.Forms.Label stateLabel = new System.Windows.Forms.Label();
			stateLabel.Location = new System.Drawing.Point(10, 40);
			stateLabel.Text = "State:";
			stateLabel.AutoSize = true;
			Controls.Add(stateLabel);

			m_state = new System.Windows.Forms.Label();
			m_state.Location = new System.Drawing.Point(50, 40);
			m_state.AutoSize = true;
			Controls.Add(m_state);

			System.Windows.Forms.Label countLabel = new System.Windows.Forms.Label();
			countLabel.Location = new System.Drawing.Point(120, 40);
			countLabel.Text = "Count:";
			countLabel.AutoSize = true;
			Controls.Add(countLabel);

			m_count = new System.Windows.Forms.Label();
			m_count.Location = new System.Drawing.Point(160, 40);
			m_count.AutoSize = true;
			Controls.Add(m_count);

			System.Windows.Forms.Label bytesLabel = new System.Windows.Forms.Label();
			bytesLabel.Location = new System.Drawing.Point(280, 40);
			bytesLabel.Text = "Bytes:";
			bytesLabel.AutoSize = true;
			Controls.Add(bytesLabel);

			m_bytes = new System.Windows.Forms.Label();
			m_bytes.Location = new System.Drawing.Point(320, 40);
			m_bytes.AutoSize = true;
			Controls.Add(m_bytes);

			m_startButton = new System.Windows.Forms.Button();
			m_startButton.Location = new System.Drawing.Point(10, 80);
			m_startButton.Text = "Start";
			m_startButton.AutoSize = true;
			m_startButton.Click += new EventHandler(StartButtonClick_);
			Controls.Add(m_startButton);

			m_stopButton = new System.Windows.Forms.Button();
			m_stopButton.Location = new System.Drawing.Point(100, 80);
			m_stopButton.Text = "Stop";
			m_stopButton.AutoSize = true;
			m_stopButton.Click += new EventHandler(StopButtonClick_);
			Controls.Add(m_stopButton);

			m_pauseButton = new System.Windows.Forms.Button();
			m_pauseButton.Location = new System.Drawing.Point(190, 80);
			m_pauseButton.Text = "Pause";
			m_pauseButton.AutoSize = true;
			m_pauseButton.Click += new EventHandler(PauseButtonClick_);
			Controls.Add(m_pauseButton);

			m_resumeButton = new System.Windows.Forms.Button();
			m_resumeButton.Location = new System.Drawing.Point(280, 80);
			m_resumeButton.Text = "Resume";
			m_resumeButton.AutoSize = true;
			m_resumeButton.Click += new EventHandler(ResumeButtonClick_);
			Controls.Add(m_resumeButton);

			m_exitButton = new System.Windows.Forms.Button();
			m_exitButton.Location = new System.Drawing.Point(370, 80);
			m_exitButton.Text = "Exit";
			m_exitButton.AutoSize = true;
			m_exitButton.Click += new EventHandler(ExitButtonClick_);
			Controls.Add(m_exitButton);
		}

		/// <summary>
		/// Export change event handler. This is called whenever the state of the export changes.
		/// </summary>
		/// <param name="state">Current state of the export</param>
		/// <param name="progress">Current progress of the export</param>
		void ExportChange_(AvigilonDotNet.ExportState state,
			AvigilonDotNet.ExportProgress progress)
		{
			m_state.Text = state.ToString();
			m_count.Text = progress.count.ToString();
			m_bytes.Text = progress.written.ToString();

			m_progress.Value = (System.Int32)(100.0 * progress.percent);
		}

		/// <summary>
		/// Start button clicked event handler
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="e"></param>
		void StartButtonClick_(
			System.Object obj,
			System.EventArgs e)
		{
			if(AvigilonDotNet.ErrorHelper.IsError(m_export.Start()))
			{
				System.Windows.Forms.MessageBox.Show("Illegal transition");
			}
		}

		/// <summary>
		/// Stop button clicked event handler
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="e"></param>
		void StopButtonClick_(
			System.Object obj,
			System.EventArgs e)
		{
			if(AvigilonDotNet.ErrorHelper.IsError(m_export.Stop()))
			{
				System.Windows.Forms.MessageBox.Show("Illegal transition");
			}
		}

		/// <summary>
		/// Pause button clicked event handler
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="e"></param>
		void PauseButtonClick_(
			System.Object obj,
			System.EventArgs e)
		{
			if(AvigilonDotNet.ErrorHelper.IsError(m_export.Pause()))
			{
				System.Windows.Forms.MessageBox.Show("Illegal transition");
			}
		}

		/// <summary>
		/// Resume button clicked event handler
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="e"></param>
		void ResumeButtonClick_(
			System.Object obj,
			System.EventArgs e)
		{
			if (AvigilonDotNet.ErrorHelper.IsError(m_export.Resume()))
			{
				System.Windows.Forms.MessageBox.Show("Illegal transition");
			}
		}

		/// <summary>
		/// Exit button clicked event handler
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="e"></param>
		void ExitButtonClick_(
			System.Object obj,
			System.EventArgs e)
		{
			Close();
		}

		/// <summary>
		/// Called when the dialog is to be closed
		/// </summary>
		/// <param name="e"></param>
		protected override void OnFormClosed(System.Windows.Forms.FormClosedEventArgs e)
		{
			m_export.Stop();
			base.OnFormClosed(e);
		}


		private AvigilonDotNet.IExport m_export;

		private System.Windows.Forms.ProgressBar m_progress;
		private System.Windows.Forms.Label m_state;
		private System.Windows.Forms.Label m_count;
		private System.Windows.Forms.Label m_bytes;

		private System.Windows.Forms.Button m_startButton;
		private System.Windows.Forms.Button m_stopButton;
		private System.Windows.Forms.Button m_pauseButton;
		private System.Windows.Forms.Button m_resumeButton;

		private System.Windows.Forms.Button m_exitButton;
	}
}
