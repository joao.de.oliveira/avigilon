/*--------------------------------------------------------------------------------------
 * Copyright 2009 Avigilon Corporation.
 *
 * This code is part of the Avigilon Control Center SDK sample application
 * collection.  This code is supplied as is, Avigilon Corporation assumes no
 * liability for any damages which may occur from use of this sample code.
 * Permission is hereby granted to use, copy, modify, and distribute this code,
 * in binary or source form, for any purpose, without restriction.
 *------------------------------------------------------------------------------------*/

namespace DemoApp
{
	public class LoginForm
		: System.Windows.Forms.Form
	{
		public LoginForm()
		{
			// Initialize the form
			m_usernameLabel = new System.Windows.Forms.Label();
			m_usernameLabel.Text = "User Name:";
			m_usernameLabel.AutoSize = true;
			Controls.Add(m_usernameLabel);

			m_usernameTextBox = new System.Windows.Forms.TextBox();
			m_usernameTextBox.Width = 150;
			m_usernameTextBox.Text = "administrator";
			Controls.Add(m_usernameTextBox);

			m_passwordLabel = new System.Windows.Forms.Label();
			m_passwordLabel.Text = "Password:";
			m_passwordLabel.AutoSize = true;
			Controls.Add(m_passwordLabel);

			m_passwordTextBox = new System.Windows.Forms.TextBox();
			m_passwordTextBox.Width = 150;
			m_passwordTextBox.UseSystemPasswordChar = true;
			Controls.Add(m_passwordTextBox);

			m_okButton = new System.Windows.Forms.Button();
			m_okButton.Text = "OK";
			m_okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			Controls.Add(m_okButton);

			m_cancelButton = new System.Windows.Forms.Button();
			m_cancelButton.Text = "Cancel";
			m_cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			Controls.Add(m_cancelButton);

			ClientSize = new System.Drawing.Size(250, 90);

			m_usernameLabel.Location = new System.Drawing.Point(5, 7);
			m_usernameTextBox.Location = new System.Drawing.Point(80, 5);
			m_passwordLabel.Location = new System.Drawing.Point(5, 30);
			m_passwordTextBox.Location = new System.Drawing.Point(80, 28);

			m_cancelButton.Location = new System.Drawing.Point(168, 60);
			m_okButton.Location = new System.Drawing.Point(90, 60);

			Text = "Log In to Site";
			FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			AcceptButton = m_okButton;
			CancelButton = m_cancelButton;
		}

		public string UserName
		{
			get { return m_usernameTextBox.Text; }
		}

		public string Password
		{
			get { return m_passwordTextBox.Text; }
		}

		private System.Windows.Forms.Label m_usernameLabel;
		private System.Windows.Forms.TextBox m_usernameTextBox;
		private System.Windows.Forms.Label m_passwordLabel;
		private System.Windows.Forms.TextBox m_passwordTextBox;
		private System.Windows.Forms.Button m_okButton;
		private System.Windows.Forms.Button m_cancelButton;

	}
}
