using AvigilonPOC.ORM.Entities;
using AvigilonPOC.ORM.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

[Route("api/[controller]")]
[ApiController]
public class CameraController : ControllerBase
{
    private readonly ICameraRepository _cameraRepository;

    public CameraController(ICameraRepository cameraRepository)
    {
        _cameraRepository = cameraRepository;
    }

    // GET: api/Camera/5
    [HttpGet("{id}")]
    public async Task<ActionResult<Camera>> GetCamera(int id)
    {
        var entity = await _cameraRepository.GetById(id);

        if (entity == null)
        {
            return NotFound();
        }

        return entity;
    }

    // GET: api/Camera/deviceId=501920kxzmck
    [HttpGet("deviceId={deviceId}")]
    public async Task<ActionResult<Camera?>> GetCameraByDeviceId(string deviceId)
    {
        var entity = await _cameraRepository.GetAll().Result.FirstOrDefaultAsync(x => x.DeviceId == deviceId);

        if (entity == null)
        {
            return NotFound();
        }

        return entity;
    }

    // GET: api/Camera/serialNumber=501920kxzmck
    [HttpGet("serialNumber={serialNumber}")]
    public async Task<ActionResult<Camera?>> GetCameraBySerialNumber(string serialNumber)
    {
        var entity = await _cameraRepository.GetAll().Result.FirstOrDefaultAsync(x => x.SerialNumber == serialNumber);

        if (entity == null)
        {
            return NotFound();
        }

        return entity;
    }

    // GET: api/Camera/logicalId=12456
    [HttpGet("logicalId={logicalId}")]
    public async Task<ActionResult<Camera?>> GetCameraByLogicalId(uint logicalId)
    {
        var entity = await _cameraRepository.GetAll().Result.FirstOrDefaultAsync(x => x.LogicalId == logicalId);

        if (entity == null)
        {
            return NotFound();
        }

        return entity;
    }

    // GET: api/Camera
    [HttpGet]
    public async Task<ActionResult<IEnumerable<Camera>>> GetCameras()
    {
        return await _cameraRepository.GetAll().Result.ToListAsync();
    }

    // POST: api/Camera
    [HttpPost]
    public async Task<ActionResult<Camera>> PostCamera(Camera entity)
    {
        await _cameraRepository.Add(entity);
        await _cameraRepository.Save();

        return CreatedAtAction(nameof(GetCamera), new { id = entity.Id }, entity);
    }

    // PUT: api/Camera/5
    [HttpPut("{id}")]
    public async Task<IActionResult> PutCamera(int id, Camera entity)
    {
        if (id != entity.Id)
        {
            return BadRequest();
        }

        try
        {
            await _cameraRepository.Update(entity);
            await _cameraRepository.Save();

        }
        catch (DbUpdateConcurrencyException)
        {
            if (!CameraExists(id))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }

        return NoContent();
    }

    // DELETE: api/Camera/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteCamera(int id)
    {
        var entity = await _cameraRepository.GetById(id);
        if (entity == null)
        {
            return NotFound();
        }

        await _cameraRepository.Delete(entity);
        await _cameraRepository.Save();


        return NoContent();
    }

    private bool CameraExists(int id)
    {
        return _cameraRepository.GetAll().Result.Any(e => e.Id == id);
    }
}
