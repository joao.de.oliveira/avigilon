using AvigilonPOC.ORM.Entities;
using AvigilonPOC.ORM.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

[Route("api/[controller]")]
[ApiController]
public class NVREventController : ControllerBase
{
    private readonly INVREventRepository _nvreventRepository;

    public NVREventController(INVREventRepository nvreventRepository)
    {
        _nvreventRepository = nvreventRepository;
    }

    // GET: api/NVREvent/dateStart=12/01/2024T20:46:16&dateEnd=12/02/2024T20:46:16
    [HttpGet("dateStart={dateStart}&dateEnd={dateEnd}")]
    public async Task<ActionResult<List<NVREvent>>> GetNVREventByTimeStamp(DateTime dateStart, DateTime dateEnd)
    {
        var entityList = await _nvreventRepository.GetAll().Result.Where(x => x.TimeStamp >= dateStart && x.TimeStamp <= dateEnd).ToListAsync();

        if (entityList == null || !entityList.Any())
        {
            return NotFound();
        }

        return entityList;
    }

    // POST: api/NVREvent
    [HttpPost]
    public async Task<ActionResult<List<NVREvent>>> PostNVREvent(List<NVREvent> listEntity)
    {
        await _nvreventRepository.Add(listEntity);
        await _nvreventRepository.Save();

        return CreatedAtAction(nameof(PostNVREvent), listEntity);
    }

    // GET: api/NVREvent/5
    [HttpGet("{id}")]
    public async Task<ActionResult<NVREvent>> GetNVREvent(int id)
    {
        var entity = await _nvreventRepository.GetById(id);

        if (entity == null)
        {
            return NotFound();
        }

        return entity;
    }
}
