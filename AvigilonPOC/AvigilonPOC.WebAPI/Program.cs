using AvigilonPOC.ORM.Contexts;
using AvigilonPOC.ORM.Repositories;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container. 

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<AvigilonPOCDbContext>(
    options => options.UseSqlServer(builder.Configuration.GetConnectionString("AvigilonPOCSqlServer"))    
);

builder.Services.AddScoped<ICameraRepository, CameraRepository>();
builder.Services.AddScoped<INVREventRepository, NVREventRepository>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
