﻿using AvigilonDotNet;
using AvigilonPOC.WinService.DTO;
using AvigilonPOC.WinService.Utilities.FluentRequest;
using System.Net.Http;

namespace AvigilonPOC.WinService.Services
{
    public class CameraServiceAPI
    {

        private const string URL_API = "http://localhost:5294";

        public ResponseDTO<CameraDTO> ObterCameraPeloDeviceId(string deviceId) 
        {
            return new FluentRequest()
                .WithMethod(HttpMethod.Get)
                .WithUrl($"{URL_API}/api/Camera/deviceId={deviceId}")
                .Send<CameraDTO>();
        }

        public CameraDTO AdicionarCamera(IEntityCamera entity) 
        {
            var cameraRequestDTO = new CameraDTO()
            {
                DeviceId = entity.DeviceId,
                Description = entity.Description,
                HasRecordedData = entity.HasRecordedData,
                DisplayName = entity.DisplayName,
                Enabled = entity.Enabled,
                IsH264Supported = entity.IsH264Supported,
                IsManualRecord = entity.IsManualRecord,
                IsMotionDetected = entity.IsMotionDetected,
                Location = entity.Location,
                LogicalId = entity.LogicalId,
                Type = entity.Type.ToString(),
                Manufacturer = entity.ParentDevice?.Manufacturer,
                Model = entity.ParentDevice?.Model,
                Name = entity.ParentDevice?.Name,
                ReportingDataStoreUuid = entity.ParentDevice?.ReportingDataStoreUuid,
                ReportingNvrUuid = entity.ParentDevice?.ReportingNvrUuid,
                StandbyMode = entity.ParentDevice?.StandbyMode ?? false,
                Connected = entity.ParentDevice?.Connected ?? false,
                SerialNumber = entity.ParentDevice?.SerialNumber
            };

            var response = new FluentRequest()
                .WithMethod(HttpMethod.Post)
                .WithUrl($"{URL_API}/api/Camera/")
                .WithData(cameraRequestDTO)
                .Send<CameraDTO>();

            return response.Data;
        }

        public void AtualizarCamera(CameraDTO cameraDTO, IEntityCamera entity)
        {

            var cameraRequestDTO = new CameraDTO()
            {
                Id = cameraDTO.Id,
                DeviceId = entity.DeviceId,
                Description = entity.Description,
                HasRecordedData = entity.HasRecordedData,
                DisplayName = entity.DisplayName,
                Enabled = entity.Enabled,
                IsH264Supported = entity.IsH264Supported,
                IsManualRecord = entity.IsManualRecord,
                IsMotionDetected = entity.IsMotionDetected,
                Location = entity.Location,
                LogicalId = entity.LogicalId,
                Type = entity.Type.ToString(),
                Manufacturer = entity.ParentDevice.Manufacturer,
                Model = entity.ParentDevice.Model,
                Name = entity.ParentDevice.Name,
                ReportingDataStoreUuid = entity.ParentDevice.ReportingDataStoreUuid,
                ReportingNvrUuid = entity.ParentDevice.ReportingNvrUuid,
                StandbyMode = entity.ParentDevice.StandbyMode,
                Connected = entity.ParentDevice.Connected,
                SerialNumber = entity.ParentDevice.SerialNumber
            };

            var response = new FluentRequest()
                .WithMethod(HttpMethod.Put)
                .WithUrl($"{URL_API}/api/Camera/{cameraDTO.Id}")
                .WithData(cameraRequestDTO)
                .Send<object>();

            return;
        }

    }
}
