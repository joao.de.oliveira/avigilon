﻿using AvigilonDotNet;
using AvigilonPOC.WinService.DTO;
using AvigilonPOC.WinService.Utilities.FluentRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace AvigilonPOC.WinService.Services
{
    public class NVREventServiceAPI
    {

        private const string URL_API = "http://localhost:5294";

        public ResponseDTO<List<NVREventDTO>> ObterNVREventPeloTimeStamp(DateTime starttime, DateTime endtime) 
        {
            return new FluentRequest()
                .WithMethod(HttpMethod.Get)
                .WithUrl($"{URL_API}/api/NVREvent/dateStart={starttime:yyyy-MM-dd}&dateEnd={endtime:yyyy-MM-dd}")
                .Send<List<NVREventDTO>>();
        }

        public List<NVREventDTO> AdicionarNVREvent(List<NVREventDTO> entityList) 
        {

            var response = new FluentRequest()
                .WithMethod(HttpMethod.Post)
                .WithUrl($"{URL_API}/api/NVREvent/")
                .WithData(entityList)
                .Send<List<NVREventDTO>>();

            return response.Data;
        }

    }
}
