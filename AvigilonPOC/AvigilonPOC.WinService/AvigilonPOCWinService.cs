﻿using AvigilonDotNet;
using AvigilonPOC.WinService.DTO;
using AvigilonPOC.WinService.Services;
using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using System.Timers;

namespace AvigilonPOC.WinService
{
    public partial class AvigilonPOCWinService : ServiceBase
    {
        public AvigilonPOCWinService()
        {
            InitializeComponent();
        }

        private AvigilonDotNet.IAvigilonControlCenter _controlCenter;

        protected override void OnStart(string[] args)
        {

            System.Timers.Timer timer = new System.Timers.Timer
            {
                Interval = 15000
            };

            timer.Elapsed += new ElapsedEventHandler(this.ExecutarLogicaPrincipal);
            timer.Start();

        }

        public void ExecutarLogicaPrincipal(object sender, ElapsedEventArgs e)
        {
            // TODO: Obter informações sensíveis de um arquivo de configurações 
            // TODO: Descriptogravar a senha ao obter do arquivo de configurações
            int PORTA = 38880;
            string ENDERECO_IP = "131.255.82.106";
            string USERNAME = "redmaxx";
            string PASSWORD = "Tecnoradio@2024";

            if (!System.Net.IPAddress.TryParse(ENDERECO_IP, out var ipAddress))
            {
                throw new Exception("IP Address is invalid");
            }

            AvigilonDotNet.AvigilonSdk sdk = new AvigilonDotNet.AvigilonSdk();
            AvigilonDotNet.SdkInitParams initParams = new AvigilonDotNet.SdkInitParams(
                AvigilonDotNet.AvigilonSdk.MajorVersion,
                AvigilonDotNet.AvigilonSdk.MinorVersion)
            {
                AutoDiscoverNvrs = true,
                ServiceMode = true
            };

            this._controlCenter = sdk.CreateInstance(initParams);

            AvigilonDotNet.AvgError controlCenterResult = _controlCenter.AddNvr(ENDERECO_IP, PORTA);
            
            if (AvigilonDotNet.ErrorHelper.IsError(controlCenterResult))
            {
                throw new Exception($"Um erro aconteceu enquanto estava adicionando um NVR [{controlCenterResult}]");
            }

            EventLog.WriteEntry($"Control Center conectou com IP e Porta [{ENDERECO_IP}:{PORTA}] {controlCenterResult}", EventLogEntryType.Information);

            Thread.Sleep(15000);

            var cameraServiceAPI = new CameraServiceAPI();
            var nvrEventServiceAPI = new NVREventServiceAPI();
            
            var nvr = this._controlCenter.Nvrs.First();

            AvigilonDotNet.LoginResult loginResult = nvr.Login(USERNAME, PASSWORD);

            if (loginResult != AvigilonDotNet.LoginResult.Successful)
            {
                throw new Exception($"Um erro ocorreu ao fazer Login no NVR: {loginResult}");
            }

            EventLog.WriteEntry($"NVR [{nvr.Name}] conectou com Usuário [{USERNAME}]", EventLogEntryType.Information);

            if (nvr.Devices.Count == 0)
            {
                EventLog.WriteEntry($"NVR [{nvr.Name}] não possui Devices", EventLogEntryType.Error);
            }

            EventLog.WriteEntry($"NVR [{nvr.Name}] possui [{nvr.Devices.Count}] Devices", EventLogEntryType.Information);


            foreach (var entity in nvr.Devices.SelectMany(x => x.Entities))
            {

                var entityCamera = (IEntityCamera)entity;

                if (entityCamera == null) {
                    continue;
                }

                var responseDTO = cameraServiceAPI.ObterCameraPeloDeviceId(entityCamera.DeviceId);

                if (responseDTO.Data == null)
                {

                    cameraServiceAPI.AdicionarCamera(entityCamera);

                    EventLog.WriteEntry($"EntityCamera DeviceId [{entityCamera.DeviceId}] adicionado na API", EventLogEntryType.Information);

                    continue;
                }
                else
                {
                    cameraServiceAPI.AtualizarCamera(responseDTO.Data, entityCamera);

                    EventLog.WriteEntry($"EntityCamera DeviceId [{entityCamera.DeviceId}] atualizado na API", EventLogEntryType.Information);

                    continue;
                }

            }
             
            var query = nvr.QueryEventsTimeStamp(
                new DateTime(2023,01,01,00,00,00),
                DateTime.Now,
                EventLinkMode.Single | EventLinkMode.Opened | EventLinkMode.Closed | EventLinkMode.Undefined,
                true
            );

            query.ResultReceived += QueryResultHandlerDelegate_;

            var execution = query.Execute();    

            if (ErrorHelper.IsError(execution))
            {
                EventLog.WriteEntry($"Query Failed [{execution}]", EventLogEntryType.Error);
            }

            nvr?.Logout();
            sdk.Shutdown();
            sdk.Dispose();
            _controlCenter.Dispose();

        }


        protected override void OnStop()
        {
            
        }

        private void QueryResultHandlerDelegate_(
            IQueryResult result,
            QueryState queryState)
        {
            // Send conversion to thread pool so we don't block up the main thread
            System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(ConvertResult_), result);
        }

        void ConvertResult_(object obj)
        {
            IQueryResult result = obj as IQueryResult;
            System.Collections.Generic.List<NVREventDTO> lviList = new System.Collections.Generic.List<NVREventDTO>();
            if (result != null)
            {
                switch (result.Type)
                {
                    case QueryResultType.Events:
                        {
                            IQueryResultEvents queryResultsEvents = (IQueryResultEvents)result;
                            System.Collections.Generic.List<IQueryResultEvents.Result> eventResults = queryResultsEvents.Results;
                            foreach (IQueryResultEvents.Result eventResult in eventResults)
                            {
                                // Columns:
                                // Time, Type, Category, linkMode, Name, Message

                                if (eventResult.originalEvent != null)
                                {
                                    NVREventDTO newItem = new NVREventDTO()
                                    {
                                        Category = eventResult.originalEvent.Category.ToString(),
                                        Name = eventResult.originalEvent.Name,
                                        Message = eventResult.originalEvent.Message,
                                        TimeStamp = eventResult.originalEvent.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss"),
                                        Type = eventResult.originalEvent.Type.ToString(),
                                        LinkMode = eventResult.originalEvent.LinkMode.ToString(),
                                        NvrUuid = eventResult.originalEvent.NvrUuid.ToString(),
                                        LinkedEventType = eventResult.originalEvent.LinkedEventType.ToString()
                                    };
                                    lviList.Add(newItem);
                                }

                                if (eventResult.linkedEvent != null)
                                {
                                    NVREventDTO newItem = new NVREventDTO()
                                    {
                                        Category = eventResult.linkedEvent.Category.ToString(),
                                        Name = eventResult.linkedEvent.Name,
                                        Message = eventResult.linkedEvent.Message,
                                        TimeStamp = eventResult.linkedEvent.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss"),
                                        Type = eventResult.linkedEvent.Type.ToString(),
                                        LinkMode = eventResult.linkedEvent.LinkMode.ToString(),
                                        NvrUuid = eventResult.linkedEvent.NvrUuid.ToString(),
                                        LinkedEventType = eventResult.linkedEvent.LinkedEventType.ToString()
                                    };
                                    lviList.Add(newItem);
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            EventLog.WriteEntry($"NRVEvents [{lviList.Count()}]", EventLogEntryType.Information);

            if(lviList.Count > 0)
            {
                new NVREventServiceAPI().AdicionarNVREvent(lviList);
            }


        }


    }
}
