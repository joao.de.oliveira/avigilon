﻿using System;

namespace AvigilonPOC.WinService.DTO
{
    public class NVREventDTO
    {
        public int Id { get; set; }
        public string LinkedEventType { get; set; }
        public string Message { get; set; }
        public string Name { get; set; }
        public string TimeStamp { get; set; }
        public string LinkMode { get; set; }
        public string Category { get; set; }
        public string NvrUuid { get; set; }
        public string Type { get; set; }
    }
}
