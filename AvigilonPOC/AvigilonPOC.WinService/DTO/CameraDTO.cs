﻿namespace AvigilonPOC.WinService.DTO
{
    public class CameraDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string DeviceId { get; set; }
        public string DisplayName { get; set; }
        public bool Enabled { get; set; }
        public bool HasRecordedData { get; set; }
        public bool IsH264Supported { get; set; }
        public bool IsManualRecord { get; set; }
        public bool IsMotionDetected { get; set; }
        public string Location { get; set; }
        public uint LogicalId { get; set; }
        public string Name { get; set; }
        public string ReportingDataStoreUuid { get; set; }
        public string ReportingNvrUuid { get; set; }
        public string Type { get; set; }
        public bool Connected { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public bool StandbyMode { get; set; }
    }
}
