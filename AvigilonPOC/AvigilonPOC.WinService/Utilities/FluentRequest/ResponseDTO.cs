﻿namespace AvigilonPOC.WinService.Utilities.FluentRequest
{
    public class ResponseDTO<T>
    {
        public int StatusCode { get; set; }
        public T Data { get; set; }
        public bool Error { get; set; }
        public string Message { get; set; }
    }

}