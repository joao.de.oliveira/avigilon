﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace AvigilonPOC.WinService.Utilities.FluentRequest
{
    public interface IFluentRequest
    {
        ResponseDTO<TStruct> Send<TStruct>();
        IFluentRequest OnErrorResponse(Action<HttpStatusCode, string> errorResponseHandler);
        IFluentRequest OnException(Action<Exception> errorHandler);
        IFluentRequest WithAuthenticationHeader(AuthenticationHeaderValue authenticationHeaderValue);
        IFluentRequest WithStream(byte[] data, string contentType = null);
        IFluentRequest WithData(object data, Encoding encoding = null, string contentType = null, JsonSerializerSettings jsonSerializerSettings = null);
        IFluentRequest WithHeader(string key, string value);
        IFluentRequest WithHeaders(Dictionary<string, string> headers);
        IFluentRequest WithHttpClientHandler(HttpClientHandler handler);
        IFluentRequest WithMethod(HttpMethod method);
        IFluentRequest WithTimeout(uint seconds);
        IFluentRequest WithUrl(string url);
    }

}