﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Xml;
using Newtonsoft.Json;

namespace AvigilonPOC.WinService.Utilities.FluentRequest
{

    public class FluentRequest : IFluentRequest
    {

        private HttpMethod method;
        private string url;
        private HttpContent content;
        private Dictionary<string, string> headers;
        private Action<Exception> errorHandler;
        private Action<HttpStatusCode, string> errorResponseHandler;
        private HttpClientHandler httpClientHandler;
        private AuthenticationHeaderValue authenticationHeaderValue;
        private TimeSpan? timeout;
        private byte[] dataBytes;
        private string contentType;
        private const int DEFAULT_TIMEOUT_SECONDS = 30;

        public FluentRequest()
        {
            headers = new Dictionary<string, string>();
        }

        public IFluentRequest WithMethod(HttpMethod method)
        {
            this.method = method;
            return this;
        }

        public IFluentRequest WithUrl(string url)
        {
            this.url = url;
            return this;
        }

        public IFluentRequest WithData(object data, Encoding encoding = null, string contentType = null, JsonSerializerSettings jsonSerializerSettings = null)
        {
            if (data != null)
            {
                if (data is FormUrlEncodedContent)
                {
                    content = data as FormUrlEncodedContent;
                }
                else if (IsJson(data, out var jsonData, jsonSerializerSettings))
                {
                    content = new StringContent(jsonData, encoding ?? Encoding.UTF8, contentType ?? "application/json");
                }
                else if (IsXml(data, out var xmlData))
                {
                    content = new StringContent(xmlData, encoding ?? Encoding.UTF8, contentType ?? "application/xml");
                }
                else
                {
                    content = new StringContent(JsonConvert.SerializeObject(data), encoding ?? Encoding.UTF8, contentType ?? "application/json");
                }
            }
            return this;
        }

        private static bool IsJson(object data, out string jsonData, JsonSerializerSettings jsonSerializerSettings = null)
        {
            try
            {
                var jsonObject = jsonSerializerSettings == null ? JsonConvert.SerializeObject(data) : JsonConvert.SerializeObject(data, jsonSerializerSettings);
                var obj = JsonConvert.DeserializeObject(jsonObject);
                jsonData = jsonObject;
                return true;
            }
            catch (JsonException)
            {
                jsonData = null;
                return false;
            }
        }

        private static bool IsXml(object data, out string xmlData)
        {
            try
            {
                var xmlObject = data.ToString();
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlObject);
                xmlData = xmlObject;
                return true;
            }
            catch (XmlException)
            {
                xmlData = null;
                return false;
            }
        }

        public IFluentRequest WithStream(byte[] data, string contentType)
        {
            this.dataBytes = data;
            this.contentType = contentType;
            return this;
        }

        public IFluentRequest WithHeader(string key, string value)
        {
            headers[key] = value;
            return this;
        }

        public IFluentRequest WithHeaders(Dictionary<string, string> headers)
        {
            foreach (var header in headers)
            {
                this.headers[header.Key] = header.Value;
            }
            return this;
        }

        public IFluentRequest WithTimeout(uint seconds)
        {
            this.timeout = TimeSpan.FromSeconds(seconds);
            return this;
        }

        public IFluentRequest WithHttpClientHandler(HttpClientHandler handler)
        {
            httpClientHandler = handler;
            return this;
        }

        public IFluentRequest WithAuthenticationHeader(AuthenticationHeaderValue authenticationHeaderValue)
        {
            this.authenticationHeaderValue = authenticationHeaderValue;
            return this;
        }

        public IFluentRequest OnException(Action<Exception> errorHandler)
        {
            this.errorHandler = errorHandler;
            return this;
        }

        public IFluentRequest OnErrorResponse(Action<HttpStatusCode, string> errorResponseHandler)
        {
            this.errorResponseHandler = errorResponseHandler;
            return this;
        }

        public ResponseDTO<T> Send<T>()
        {
            try
            {

                using (var client = NewHttpClient())
                {

                    HttpRequestMessage request = NewHttpRequestMessage(client);

                    using (var cancellationTokenSource = new System.Threading.CancellationTokenSource())
                    {

                        cancellationTokenSource.CancelAfter(client.Timeout);

                        using (var response = client.SendAsync(request, cancellationTokenSource.Token).Result)
                        {

                            var responseContent = response.Content.ReadAsStringAsync().Result;

                            if (!response.IsSuccessStatusCode)
                            {

                                errorResponseHandler?.Invoke(response.StatusCode, responseContent);

                                return new ResponseDTO<T>
                                {
                                    StatusCode = (int)response.StatusCode,
                                    Data = default(T),
                                    Error = true
                                };

                            }

                            T responseData;

                            if (IsXml(responseContent, out var xmlData))
                            {
                                responseData = (T)(object)xmlData;
                            }
                            else
                            {
                                responseData = JsonConvert.DeserializeObject<T>(responseContent);
                            }

                            return new ResponseDTO<T>
                            {
                                StatusCode = (int)response.StatusCode,
                                Data = responseData
                            };

                        }
                    }
                }
            }
            catch (Exception ex) when (ex.InnerException is TimeoutException)
            {
                var timeoutException = (TimeoutException)ex.InnerException;
                errorHandler?.Invoke(timeoutException);

                return new ResponseDTO<T>
                {
                    StatusCode = 0,
                    Data = default(T),
                    Error = true,
                    Message = $"Tempo limite de {timeout.Value.TotalSeconds} esgotado: {timeoutException.Message}"
                };
            }
            catch (Exception ex)
            {

                errorHandler?.Invoke(ex);

                return new ResponseDTO<T>
                {
                    StatusCode = 0,
                    Data = default(T),
                    Error = true,
                    Message = ex.Message.ToString()
                };

            }
        }

        private HttpRequestMessage NewHttpRequestMessage(HttpClient client)
        {

            var request = new HttpRequestMessage(method, url);

            AddRequestContent(request);
            AddClientAuthenticationHeaderValue(client);
            AddRequestHeaders(request);
            AddClientTimeout(client);
            LogRequestToDebug(request);

            return request;

        }

        private HttpClient NewHttpClient() =>
            httpClientHandler != null ? new HttpClient(httpClientHandler, false) : new HttpClient();

        private void AddClientTimeout(HttpClient client) =>
            client.Timeout = timeout ?? TimeSpan.FromSeconds(DEFAULT_TIMEOUT_SECONDS);

        private void LogRequestToDebug(HttpRequestMessage request)
        {

            string httpRequest = $"{this.method} {this.url}";

            if (request.Content != null)
            {
                string contentString = request.Content.ReadAsStringAsync().Result;
                httpRequest += Environment.NewLine + contentString;
            }

            System.Diagnostics.Debug.WriteLine($"> HTTP Request: {httpRequest}");

        }

        private void AddRequestHeaders(HttpRequestMessage request)
        {
            foreach (var header in headers)
            {
                request.Headers.Add(header.Key, header.Value);
            }
        }

        private void AddClientAuthenticationHeaderValue(HttpClient client) =>
            client.DefaultRequestHeaders.Authorization = authenticationHeaderValue ?? client.DefaultRequestHeaders.Authorization;

        private void AddRequestContent(HttpRequestMessage request)
        {
            if (dataBytes != null)
            {
                request.Content = new ByteArrayContent(dataBytes);
                request.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType);
                dataBytes = null;
            }
            else
            {
                request.Content = content ?? request.Content;
            }

            if (!HasContent(request) || method == HttpMethod.Get)
            {
                content = null;
                request.Content = null;
            }
        }

        private bool HasContent(HttpRequestMessage request)
        {
            // Verifica se o HttpRequestMessage é nulo ou se o Content é nulo ou vazio.
            return request != null && request.Content != null && request.Content.Headers.ContentLength != 0;
        }

    }

}