﻿namespace AvigilonPOC.ORM.Entities
{
    public class NVREvent
    {
        public int Id { get; set; }
        public string LinkedEventType { get; set; }
        public string Message { get; set; }
        public string Name { get; set; }
        public DateTime TimeStamp { get; set; }
        public string LinkMode { get; set; }
        public string Category { get; set; }
        public string NvrUuid { get; set; }
        public string Type { get; set; }
    }
}
