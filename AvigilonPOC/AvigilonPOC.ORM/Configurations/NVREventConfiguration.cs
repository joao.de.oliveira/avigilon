﻿using AvigilonPOC.ORM.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AvigilonPOC.ORM.Configurations
{
    internal class NVREventConfiguration : IEntityTypeConfiguration<NVREvent>
    {
        public void Configure(EntityTypeBuilder<NVREvent> builder)
        {
            builder.HasKey(x => x.Id);
            // TODO: Criar limites para as strings pelo menos
        }
    }
}
