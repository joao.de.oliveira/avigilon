﻿using AvigilonPOC.ORM.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AvigilonPOC.ORM.Configurations
{
    internal class CameraConfiguration : IEntityTypeConfiguration<Camera>
    {
        public void Configure(EntityTypeBuilder<Camera> builder)
        {
            builder.HasKey(x => x.Id);

            // TODO: Criar limites para as strings pelo menos
        }
    }
}
