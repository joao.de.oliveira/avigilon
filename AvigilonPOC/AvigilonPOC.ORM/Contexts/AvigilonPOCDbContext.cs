﻿using AvigilonPOC.ORM.Configurations;
using AvigilonPOC.ORM.Entities;
using Microsoft.EntityFrameworkCore;

namespace AvigilonPOC.ORM.Contexts
{
    public class AvigilonPOCDbContext : DbContext
    {
        public DbSet<Camera> Cameras { get; set; }
        public DbSet<NVREvent> NVREvents { get; set; }

        public AvigilonPOCDbContext(DbContextOptions<AvigilonPOCDbContext> options) :base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CameraConfiguration());
            modelBuilder.ApplyConfiguration(new NVREventConfiguration());
        }
    }
}
