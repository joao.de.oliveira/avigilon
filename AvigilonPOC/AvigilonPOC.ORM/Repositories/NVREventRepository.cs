﻿using AvigilonPOC.ORM.Contexts;
using AvigilonPOC.ORM.Entities;
using Microsoft.EntityFrameworkCore;

namespace AvigilonPOC.ORM.Repositories
{
    public class NVREventRepository : INVREventRepository
    {

        private readonly AvigilonPOCDbContext _dbContext;

        public NVREventRepository(AvigilonPOCDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Add(NVREvent entity)
        {
            await _dbContext.Set<NVREvent>().AddAsync(entity);

        }

        public async Task Add(List<NVREvent> entity)
        {
            await _dbContext.Set<NVREvent>().AddRangeAsync(entity);

        }

        public async Task Delete(NVREvent entity)
        {
            _dbContext.Set<NVREvent>().Remove(entity);

        }

        public async Task Delete(List<NVREvent> listOfEntity)
        {
            _dbContext.Set<NVREvent>().RemoveRange(listOfEntity);

        }

        public async Task<IQueryable<NVREvent>> GetAll() =>
             await Task.FromResult(_dbContext.Set<NVREvent>());

        public async Task<NVREvent?> GetById(int id) =>
            await _dbContext.Set<NVREvent>().FindAsync(id);

        public async Task Update(NVREvent entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;

        }

        public async Task Update(List<NVREvent> listOfEntity)
        {
            listOfEntity.ForEach(entity => _dbContext.Entry(entity).State = EntityState.Modified);

        }

        public async Task<bool> Save() =>
            await _dbContext.SaveChangesAsync() > 0;

    }
}
