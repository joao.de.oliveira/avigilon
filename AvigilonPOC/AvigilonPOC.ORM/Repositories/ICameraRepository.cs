﻿using AvigilonPOC.ORM.Entities;

namespace AvigilonPOC.ORM.Repositories
{
    public interface ICameraRepository
    {
        Task Add(List<Camera> entity);
        Task Add(Camera entity);
        Task Delete(List<Camera> listOfEntity);
        Task Delete(Camera entity);
        Task<IQueryable<Camera>> GetAll();
        Task<Camera?> GetById(int id);
        Task Update(List<Camera> listOfEntity);
        Task Update(Camera entity);
        Task<bool> Save();
    }

}
