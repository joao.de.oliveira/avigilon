﻿using AvigilonPOC.ORM.Contexts;
using AvigilonPOC.ORM.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvigilonPOC.ORM.Repositories
{
    public class CameraRepository : ICameraRepository
    {

        private readonly AvigilonPOCDbContext _dbContext;

        public CameraRepository(AvigilonPOCDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Add(Camera entity)
        {
            await _dbContext.Set<Camera>().AddAsync(entity);

        }

        public async Task Add(List<Camera> entity)
        {
            await _dbContext.Set<Camera>().AddRangeAsync(entity);

        }

        public async Task Delete(Camera entity)
        {
            _dbContext.Set<Camera>().Remove(entity);

        }

        public async Task Delete(List<Camera> listOfEntity)
        {
            _dbContext.Set<Camera>().RemoveRange(listOfEntity);

        }

        public async Task<IQueryable<Camera>> GetAll() =>
             await Task.FromResult(_dbContext.Set<Camera>());

        public async Task<Camera?> GetById(int id) =>
            await _dbContext.Set<Camera>().FindAsync(id);

        public async Task Update(Camera entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;

        }

        public async Task Update(List<Camera> listOfEntity)
        {
            listOfEntity.ForEach(entity => _dbContext.Entry(entity).State = EntityState.Modified);

        }

        public async Task<bool> Save() =>
            await _dbContext.SaveChangesAsync() > 0;

    }
}
