﻿using AvigilonPOC.ORM.Entities;

namespace AvigilonPOC.ORM.Repositories
{
    public interface INVREventRepository
    {
        Task Add(List<NVREvent> entity);
        Task Add(NVREvent entity);
        Task Delete(List<NVREvent> listOfEntity);
        Task Delete(NVREvent entity);
        Task<IQueryable<NVREvent>> GetAll();
        Task<NVREvent?> GetById(int id);
        Task Update(List<NVREvent> listOfEntity);
        Task Update(NVREvent entity);
        Task<bool> Save();
    }

}
